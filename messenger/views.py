from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
from django.utils import timezone

from home.views import base_auth


@login_required(login_url='base-user:login')
def index(request):
    profile = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if profile.usertype == 1:
        pass
    elif profile.usertype == 2:
        pass

    # else:
    #     raise Http404
    # return HttpResponse(customers[:20].count())
    # context['employee_count'] = employees.count()
    # context['customer_count'] = customers.count()
    # context['last_customers'] = customers.all()[:20]
    # context['last_employees'] = employees.all()[:20]
    # context['tours'] = Tour.objects.filter(active=True)
    return render(request, 'messenger/messenger.html', context=context)
