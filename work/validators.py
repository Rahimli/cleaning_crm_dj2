from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def card_expire_day_validate(value):
    if len(str(value)) != 2:
        raise ValidationError(
            _('%(value)s length is not equal 2'),
            params={'value': value},
        )
    if value > 12 and value < 1:
        raise ValidationError(
            _('%(value)s is incorrect'),
            params={'value': value},
        )

def integer_2_length_validate(value):
    if len(str(value)) != 2:
        raise ValidationError(
            _('%(value)s length is not equal 2'),
            params={'value': value},
        )

def integer_3_length_validate(value):
    if len(str(value)) != 3:
        raise ValidationError(
            _('%(value)s length is not equal 3'),
            params={'value': value},
        )

def integer_4_length_validate(value):
    if len(str(value)) != 4:
        raise ValidationError(
            _('%(value)s length is not equal 4'),
            params={'value': value},
        )

def integer_8_length_validate(value):
    if len(str(value)) != 8:
        raise ValidationError(
            _('%(value)s length is not equal 8'),
            params={'value': value},
        )

def integer_16_length_validate(value):
    if len(str(value)) != 16:
        raise ValidationError(
            _('%(value)s length is not equal 16'),
            params={'value': value},
        )