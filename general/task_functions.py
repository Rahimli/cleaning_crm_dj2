from django.conf import settings
from django.db.models import Q
import copy


def distance_2_point(location1_lat, location1_lon, location2_lat, location2_lon):
    import googlemaps
    from datetime import datetime
    # GEOPOSITION_GOOGLE_MAPS_API_KEY = settings.GEOPOSITION_GOOGLE_MAPS_API_KEY
    gmaps = googlemaps.Client(key=settings.GEOPOSITION_GOOGLE_MAPS_API_KEY)
    # gmaps = googlemaps.Client(key='AIzaSyAhAUDoUJWxE294kOzQuvI5u-hOMfCA24A')

    # Geocoding and address
    geocode_distance = gmaps.distance_matrix((location1_lat, location1_lon), (location2_lat, location2_lon),
                                             mode="driving")
    geocode_distance_es = geocode_distance['rows'][0]['elements'][0]
    return [int(round(geocode_distance_es['duration']['value'] / 60)), geocode_distance_es['distance']['value']]




def exc_list_loc(lst, wc, w):
    try:
        if w > wc:
            w = w % wc
        part = [lst[i::wc] for i in range(0, wc)]
        w = w - 1
        for x in part[w]:
            lst.remove(x)
        return lst
    except:
        return []





def calculate_minute_1times(loc_list, new_loc_id, main_loc_id, all_minute):
    from work.models import CustomerDistance
    # loc_destinations = CustomerDistance.objects.exclude(Q(customer1_id__in=exc_list) | Q(customer2_id__in=exc_list))
    i = 0
    # result_val=0
    minute = 0
    loc_list.insert(0, main_loc_id)
    loc_list.append(new_loc_id)
    loc_destinations = CustomerDistance.objects
    for loc_list_item in loc_list:
        # print("loc_list_item = {}".format(loc_list_item))
        if i > 0:
            # print("---------------------------------------------------------------------------------------------------------")
            if loc_destinations:
                pass
                # print("loc_destinations.count()".format(loc_destinations.count()))
            loc_destinations_item = loc_destinations.filter(
                Q(customer1=loc_list[i], customer2=loc_list[i - 1]) | Q(customer1=loc_list[i - 1],
                                                                        customer2=loc_list[i])).first()
            # main_customer = None
            # print(loc_destinations_item)
            if loc_destinations_item:
                if loc_list[i] == loc_destinations_item.customer1.id:
                    main_customer = loc_destinations_item.customer1
                else:
                    main_customer = loc_destinations_item.customer2
                minute += loc_destinations_item.minute + main_customer.minute
                # print('loc_destinations_item.minute + main_customer.minute = {}'.format(
                #     loc_destinations_item.minute + main_customer.minute))
            else:
                new_customer_distance = CustomerDistance()
                new_customer_distance.customer1_id = loc_list[i - 1]
                new_customer_distance.customer2_id = loc_list[i]
                distance_2_point_o = distance_2_point(
                    new_customer_distance.customer1.position.latitude,
                    new_customer_distance.customer1.position.longitude,
                    new_customer_distance.customer2.position.latitude,
                    new_customer_distance.customer2.position.longitude
                )
                new_customer_distance.minute = distance_2_point_o[0]
                new_customer_distance.distance = distance_2_point_o[1]
                new_customer_distance.save()
                minute += new_customer_distance.customer2.minute + new_customer_distance.minute

                # print("loc_list[i]={}  ---- loc_list[i-1]={}".format(loc_list[i], loc_list[i - 1]))
            # print('minute = {}'.format(minute))

        i += 1

    if all_minute >= minute:
        result_val = all_minute - minute
    else:
        result_val = -1
    return [result_val, len(loc_list) - 1]



def result1p1(week_day,exc_list, minute):
    from work.models import Customer,CustomerDistance
    from django.forms.models import model_to_dict
    # from work.new_main import calculate_minute_1times as nm_calculate_minute_1times

    # loc_destinations = CustomerDistance.objects.exclude(Q(customer1_id__in=exc_list) | Q(customer2_id__in=exc_list)).filter(Q(customer1_id=current_loc_id) | Q(customer2_id=current_loc_id)).order_by('minute')
    result_list = []
    result_exc_list = []

        # .order_by('-minute', '-customer1__minute','-customer2__minute')
    # dest_list = []
    # for loc_destination_item in loc_destinations:
    #     dest_list.append(obj_to_dict_dest(loc_destination_item))
    loc_destinations = CustomerDistance.objects.exclude(
        Q(customer1_id__in=exc_list) | Q(customer2_id__in=exc_list)).exclude(
        Q(customer1__work_times=3) | Q(customer2__work_times=3)).order_by('minute')
    main_customer = Customer.objects.filter(our_company=True).first()
    customers = Customer.objects.exclude(id__in=exc_list).exclude(our_company=True)
    while_bool = True
    before_vals = []
    customers_11_bool = False
    # res_val = []
    while_bool = True
    return_while = False
    for order_index in range(9, 0, -1):
        print('return for')
        # print(while_bool)
        print("<order_index> = <{}>".format(order_index))
        print("<while_bool> = <{}>".format(while_bool))
        print("<return_while> = <{}>".format(return_while))
        while_bool = True
        return_while = False
        print('return for')
        while while_bool:
            print('head yoxdu return_while = {}'.format(return_while))
            # if while_bool is False:
            #     # print('return_while = {}'.format(return_while))
            #     break
            customers_9_item_i = 0
            customers_8_item_i = 0
            customers_7_item_i = 0
            return_while = False
            res_val = [
                       main_customer.id,
                    ]
            # print("while_bool  res_val= {}".format(res_val))
            first_dests = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=main_customer.id) | Q(customer2_id=main_customer.id)).order_by('minute')

            if first_dests.count() == 0:
                # # print('var first_dests= {}'.format(first_dests.count()))
                break
            else:
                pass
            print('foot yoxdu first_dests = {}'.format(first_dests.count()))
            for customers_1_item in first_dests:
                if return_while:
                    # print('return_while = {}'.format(return_while))
                    break
                # for_break = False
                # if for_break:
                #     continue
                # before_vals.append(customers_1_item.id)
                if customers_1_item.customer1_id == main_customer.id:
                    customers_1_item_GO = customers_1_item.customer2
                    customers_1_item_ord = 2
                else:
                    customers_1_item_GO = customers_1_item.customer1
                    customers_1_item_ord = 1
                # res_val += [customers_1_item_GO.id]
                minute_sum = 0
                # print("customers_1_item.minute , customers_1_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_1_item.minute , customers_1_item_GO.get_customer_day_minute(week_day)))
                if order_index > 1 and minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) <= minute:
                    print("@@@@@@@@@@@ if 1")
                    minute_sum += customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day)
                    res_val += [
                        customers_1_item_GO.id,
                    ]
                    # pass
                elif order_index > 1 and minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) > minute:
                    print("@@@@@@@@@@@ elif 1")
                    continue
                else:
                    print("@@@@@@@@@@@ else 1")
                    print("@@@@@@@@@@@ {} - {} - {}".format(order_index , minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) , minute))
                    # print("order_index else = {}".format(order_index))
                    if minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) <= minute:
                        # print("@@@@@@@@@@@ if 1")
                        minute_sum += customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day)
                        res_val += [
                            customers_1_item_GO.id,
                        ]
                        # print('+++++++++++++++++++++++++++++++++++++++minute_sum <= minute++++++++++++++++++++++++++++++++++++++++')
                        # print('+++++++++++++++++++++++++++++++++++++++ res_val = {} ++++++++++++++++++++++++++++++++++++++++'.format(res_val))
                        res_val_i = 0
                        for res_val_item in res_val:
                            res_val_i += 1
                            if res_val_i > 0:
                                result_exc_list.append(copy.deepcopy(res_val_item))
                        result_list.append(copy.deepcopy(res_val + [[minute_sum]]))

                        return_while = True
                        # print('+++++++++++++++++++++++++++++++++++++++ return_while = {} ++++++++++++++++++++++++++++++++++++++++'.format(return_while))
                        break
                    else:
                        if order_index == 1 and first_dests.last().id == customers_1_item.id:
                            return_while = True
                            while_bool = False
                            break
                        else:
                            continue
                print("customers_1_item  res_val= {}, = minute_sum= {}".format(res_val,minute_sum))
                # print("customers_1_item  minute_sum= {}".format(minute_sum))
                dests_2 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_1_item_GO.id) | Q(customer2_id=customers_1_item_GO.id)).order_by('minute')
                if dests_2.count() == 0:
                    while_bool = False
                    return_while = True
                    print('var dests_2= {}'.format(dests_2.count()))
                    break
                else:
                    print('yoxdu dests_2 = {}'.format(dests_2.count()))
                    pass
                res_val_1 = copy.deepcopy(res_val)
                minute_sum_1 = copy.deepcopy(minute_sum)
                for customers_2_item in dests_2:
                    res_val = copy.deepcopy(res_val_1)
                    minute_sum = copy.deepcopy(minute_sum_1)
                    if return_while:
                        break

                    if customers_2_item.customer1_id == customers_1_item_GO.id:
                        customers_2_item_GO = customers_2_item.customer2
                    else:
                        customers_2_item_GO = customers_2_item.customer1
                    # res_val += [customers_2_item_GO.id]
                    # print("customers_2_item  res_val= {}".format(res_val))
                    # before_vals.append(customers_2_item.id)
                    # minute_sum += customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day)
                    # print("customers_2_item  minute_sum= {}".format(minute_sum))
                    # print("customers_2_item.minute , customers_2_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_2_item.minute , customers_2_item_GO.get_customer_day_minute(week_day)))
                    if order_index > 2 and minute_sum + customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) <= minute:
                        minute_sum += customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day)
                        res_val += [
                            customers_2_item_GO.id,
                        ]
                        # pass
                    elif order_index > 2 and minute_sum + customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) > minute:
                        continue
                    else:
                        print("order_index else = {}".format(order_index))
                        if minute_sum + customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) <= minute:
                            minute_sum += customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day)
                            res_val += [
                                customers_2_item_GO.id,
                            ]
                            res_val_i = 0
                            for res_val_item in res_val:
                                if res_val_i > 0:
                                    result_exc_list.append(copy.deepcopy(res_val_item))
                                res_val_i += 1
                            result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                            return_while = True
                            break
                        else:
                            if order_index == 2 and first_dests.last().id == customers_1_item.id:
                                return_while = True
                                while_bool = False
                                break
                            else:
                                continue
                    print("customers_2_item  res_val= {}, = minute_sum= {}".format(res_val,minute_sum))
                    dests_3 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_2_item_GO.id) | Q(customer2_id=customers_2_item_GO.id)).order_by('minute')
                    if dests_3.count() == 0:
                        while_bool = False
                        return_while = True
                        print('var dests_3= {}'.format(dests_3.count()))
                        break
                    else:
                        print('yoxdu dests_3= {}'.format(dests_3.count()))
                        pass
                    res_val_2 = copy.deepcopy(res_val)
                    minute_sum_2 = copy.deepcopy(minute_sum)
                    for customers_3_item in dests_3:
                        res_val = copy.deepcopy(res_val_2)
                        minute_sum = copy.deepcopy(minute_sum_2)
                        if return_while:
                            break
                        # before_vals.append(customers_3_item.id)
                        if customers_3_item.customer1_id == customers_2_item_GO.id:
                            customers_3_item_GO = customers_3_item.customer2
                        else:
                            customers_3_item_GO = customers_3_item.customer1
                        # res_val += [customers_3_item_GO.id]
                        # # print("customers_3_item  res_val= {}".format(res_val))
                        # minute_sum += customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day)
                        if order_index > 3 and minute_sum + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) <= minute:
                            minute_sum += customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day)
                            res_val += [
                                customers_3_item_GO.id,
                            ]
                            # pass
                        elif order_index > 3 and minute_sum + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) > minute:
                            continue
                        else:
                            # print("order_index else = {}".format(order_index))
                            if minute_sum + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) <= minute:
                                minute_sum += customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day)
                                res_val += [
                                    customers_3_item_GO.id,
                                ]
                                res_val_i = 0
                                for res_val_item in res_val:
                                    if res_val_i > 0:
                                        result_exc_list.append(copy.deepcopy(res_val_item))
                                    res_val_i += 1
                                result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                return_while = True
                                break
                            else:
                                if order_index == 3 and first_dests.last().id == customers_1_item.id:
                                    return_while = True
                                    while_bool = False
                                    break
                                else:
                                    continue
                        # print("customers_3_item  minute_sum= {}".format(minute_sum))
                        # print("customers_3_item.minute , customers_3_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_3_item.minute , customers_3_item_GO.get_customer_day_minute(week_day)))
                        dests_4 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_3_item_GO.id) | Q(customer2_id=customers_3_item_GO.id)).order_by('minute')
                        if dests_4.count() == 0:
                            while_bool = False
                            return_while = True
                            print('var dests_4= {}'.format(dests_4.count()))
                            break
                        else:
                            print('yoxdu dests_4= {}'.format(dests_4.count()))
                            pass
                        res_val_3 = copy.deepcopy(res_val)
                        minute_sum_3 = copy.deepcopy(minute_sum)
                        print("customers_3_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                        for customers_4_item in dests_4:
                            res_val = copy.deepcopy(res_val_3)
                            minute_sum = copy.deepcopy(minute_sum_3)
                            if return_while:
                                break
                            # before_vals.append(customers_4_item.id)
                            if customers_4_item.customer1_id == customers_3_item_GO.id:
                                customers_4_item_GO = customers_4_item.customer2
                            else:
                                customers_4_item_GO = customers_4_item.customer1
                            # res_val += [customers_4_item_GO.id]
                            # # print("customers_4_item  res_val= {}".format(res_val))
                            # minute_sum += customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day)
                            if order_index > 4 and minute_sum + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) <= minute:
                                minute_sum += customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day)
                                res_val += [
                                    customers_4_item_GO.id,
                                ]
                                # pass
                            elif order_index > 4 and minute_sum + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) > minute:
                                continue
                            else:
                                # print("order_index else = {}".format(order_index))
                                if minute_sum + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) <= minute:
                                    minute_sum += customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day)
                                    res_val += [
                                        customers_4_item_GO.id,
                                    ]
                                    res_val_i = 0
                                    for res_val_item in res_val:
                                        if res_val_i > 0:
                                            result_exc_list.append(copy.deepcopy(res_val_item))
                                        res_val_i += 1
                                    result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                    return_while = True
                                    break
                                else:
                                    if order_index == 4 and first_dests.last().id == customers_1_item.id:
                                        return_while = True
                                        while_bool = False
                                        break
                                    else:
                                        continue
                            # print("customers_4_item  minute_sum= {}".format(minute_sum))
                            # print("customers_4_item.minute , customers_4_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_4_item.minute , customers_4_item_GO.get_customer_day_minute(week_day)))
                            print("customers_4_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                            dests_5 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_4_item_GO.id) | Q(customer2_id=customers_4_item_GO.id)).order_by('minute')
                            if dests_5.count() == 0:
                                while_bool = False
                                return_while = True
                                print('var dests_5= {}'.format(dests_5.count()))
                                break
                            else:
                                pass
                                print('yoxdu dests_5= {}'.format(dests_5.count()))
                            res_val_4 = copy.deepcopy(res_val)
                            minute_sum_4 = copy.deepcopy(minute_sum)
                            for customers_5_item in dests_5:
                                res_val = copy.deepcopy(res_val_4)
                                minute_sum = copy.deepcopy(minute_sum_4)
                                if return_while:
                                    break
                                if customers_5_item.customer1_id == customers_4_item_GO.id:
                                    customers_5_item_GO = customers_5_item.customer2
                                else:
                                    customers_5_item_GO = customers_5_item.customer1
                                # res_val += [customers_5_item_GO.id]
                                print("customers_5_item  res_val= {}".format(res_val))
                                # before_vals.append(customers_5_item.id)
                                if order_index > 5 and minute_sum + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) <= minute:
                                    minute_sum += customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day)
                                    res_val += [
                                        customers_5_item_GO.id,
                                    ]
                                    # pass
                                elif order_index > 5 and minute_sum + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) > minute:
                                    continue
                                else:
                                    print("order_index else = {}".format(order_index))
                                    if minute_sum + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) <= minute:
                                        minute_sum += customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day)
                                        res_val += [
                                            customers_5_item_GO.id,
                                        ]
                                        res_val_i = 0
                                        for res_val_item in res_val:
                                            if res_val_i > 0:
                                                result_exc_list.append(copy.deepcopy(res_val_item))
                                            res_val_i += 1
                                        result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                        return_while = True
                                        break
                                    else:
                                        if order_index == 5 and first_dests.last().id == customers_1_item.id:
                                            return_while = True
                                            while_bool = False
                                            break
                                        else:
                                            continue
                                # print("customers_5_item  minute_sum= {}".format(minute_sum))
                                # print("customers_5_item.minute , customers_5_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_5_item.minute , customers_5_item_GO.get_customer_day_minute(week_day)))
                                dests_6 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_5_item_GO.id) | Q(customer2_id=customers_5_item_GO.id)).order_by('minute')
                                if dests_6.count() == 0:
                                    while_bool = False
                                    return_while = True
                                    print('var dests_6= {}'.format(dests_6.count()))
                                    break
                                else:
                                    pass
                                    print('yoxdu dests_6= {}'.format(dests_6.count()))
                                res_val_5 = copy.deepcopy(res_val)
                                minute_sum_5 = copy.deepcopy(minute_sum)
                                for customers_6_item in dests_6:
                                    # while_bool = False
                                    # return_while = True
                                    # break
                                    res_val = copy.deepcopy(res_val_5)
                                    minute_sum = copy.deepcopy(minute_sum_5)
                                    if return_while:
                                        break
                                    if customers_6_item.customer1_id == customers_5_item_GO.id:
                                        customers_6_item_GO = customers_6_item.customer2
                                    else:
                                        customers_6_item_GO = customers_6_item.customer1
                                    # res_val += [customers_6_item_GO.id]
                                    # print("customers_6_item  res_val= {}".format(res_val))
                                    # # before_vals.append(customers_6_item.id)
                                    # # minute_sum += customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day)
                                    # print("customers_6_item  minute_sum = {}".format(minute_sum))
                                    # print("customers_6_item.minute , customers_6_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_6_item.minute , customers_6_item_GO.get_customer_day_minute(week_day)))
                                    if order_index > 6 and minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) <= minute:
                                        minute_sum += customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day)
                                        res_val += [
                                            customers_6_item_GO.id,
                                        ]
                                        # pass
                                    elif order_index > 6 and minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) > minute:
                                        print("order_index > 6 and minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) > minute")
                                        customers_8_item_i += (copy.deepcopy(dests_6.count()) - 1) * (
                                        copy.deepcopy(dests_6.count()) - 2)
                                        if customers_8_item_i == dests_6.count() * (dests_6.count() - 1) * (dests_6.count() - 2) or order_index == 8 and first_dests.last().id == customers_1_item.id:
                                            print("customers_8_item_i == dests_6.count() * (dests_6.count() - 1) * (dests_6.count() - 2) or order_index == 8 and first_dests.last().id == customers_1_item.id")
                                            return_while = True
                                            while_bool = False
                                            break
                                        continue
                                    else:
                                        print("6 else")
                                        print("order_index else = {}".format(order_index))
                                        if minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) <= minute:
                                            print("minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) <= minute")
                                            minute_sum += customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day)
                                            res_val += [
                                                customers_6_item_GO.id,
                                            ]
                                            res_val_i = 0
                                            for res_val_item in res_val:
                                                if res_val_i > 0:
                                                    result_exc_list.append(copy.deepcopy(res_val_item))
                                                res_val_i += 1
                                            result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                            return_while = True
                                            break
                                        else:
                                            if order_index == 6 and first_dests.last().id == customers_1_item.id:
                                                return_while = True
                                                while_bool = False
                                                break
                                            else:
                                                continue
                                    dests_7 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_6_item_GO.id) | Q(customer2_id=customers_6_item_GO.id)).order_by('minute')
                                    if dests_7.count() == 0:
                                        while_bool = False
                                        return_while = True
                                        break
                                    res_val_6 = copy.deepcopy(res_val)
                                    minute_sum_6 = copy.deepcopy(minute_sum)
                                    # print("customers_6_item  minute_sum = {}".format(minute_sum))
                                    # print("customers_6_item  res_val = {}".format(res_val))
                                    print("customers_6_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                                    for customers_7_item in dests_7:
                                        # while_bool = False
                                        # return_while = True
                                        # break
                                        res_val = copy.deepcopy(res_val_6)
                                        minute_sum = copy.deepcopy(minute_sum_6)
                                        if return_while:
                                            break
                                        if customers_7_item.customer1_id == customers_6_item_GO.id:
                                            customers_7_item_GO = customers_7_item.customer2
                                        else:
                                            customers_7_item_GO = customers_7_item.customer1
                                        # res_val += [
                                        #             customers_7_item_GO.id,
                                        #         ]
                                        # print("customers_7_item  res_val= {}".format(res_val))
                                        # before_vals.append(customers_7_item.id)
                                        # minute_sum += customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day)
                                        if order_index > 7 and minute_sum + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) <= minute:
                                            minute_sum += customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day)
                                            res_val += [
                                                customers_7_item_GO.id,
                                            ]
                                            # pass
                                        elif order_index > 7 and minute_sum + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) > minute:
                                            customers_8_item_i += copy.deepcopy(dests_7.count()) - 1
                                            customers_9_item_i += (copy.deepcopy(dests_7.count())-1)*(copy.deepcopy(dests_7.count())-2)
                                            if customers_9_item_i == dests_7.count() * (dests_7.count() - 1) * (
                                                dests_7.count() - 2) or order_index == 9 and first_dests.last().id == customers_1_item.id:
                                                return_while = True
                                                while_bool = False
                                                break
                                            if customers_8_item_i == dests_6.count() * (dests_6.count() - 1) * (
                                                dests_6.count() - 2) or order_index == 8 and first_dests.last().id == customers_1_item.id:
                                                return_while = True
                                                while_bool = False
                                                break
                                            continue
                                        else:
                                            print("order_index else = {}".format(order_index))

                                            if minute_sum + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) <= minute:
                                                minute_sum += customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day)
                                                res_val += [
                                                    customers_7_item_GO.id,
                                                ]
                                                res_val_i = 0
                                                for res_val_item in res_val:
                                                    if res_val_i > 0:
                                                        result_exc_list.append(copy.deepcopy(res_val_item))
                                                    res_val_i += 1
                                                result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                                return_while = True
                                                break
                                            else:
                                                if order_index == 7 and first_dests.last().id == customers_1_item.id:
                                                    return_while = True
                                                    while_bool = False
                                                    break
                                                else:
                                                    continue
                                        # print("customers_7_item update  res_val= {}".format(res_val))
                                        # print("customers_7_item  minute_sum= {}".format(minute_sum))
                                        # print("customers_7_item.minute , customers_7_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_7_item.minute , customers_7_item_GO.get_customer_day_minute(week_day)))
                                        dests_8 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_7_item_GO.id) | Q(customer2_id=customers_7_item_GO.id)).order_by('minute')
                                        if dests_8.count() == 0:
                                            while_bool = False
                                            return_while = True
                                            break
                                        res_val_7 = copy.deepcopy(res_val)
                                        minute_sum_7 = copy.deepcopy(minute_sum)
                                        print("customers_7_item  res_val= {}, = minute_sum= {}".format(res_val,
                                                                                                       minute_sum))
                                        for customers_8_item in dests_8:
                                            customers_8_item_i += 1
                                            res_val = copy.deepcopy(res_val_7)
                                            minute_sum = copy.deepcopy(minute_sum_7)
                                            if return_while:
                                                break
                                            if customers_8_item.customer1_id == customers_7_item_GO.id:
                                                customers_8_item_GO = customers_8_item.customer2
                                            else:
                                                customers_8_item_GO = customers_8_item.customer1
                                            # before_vals.append(customers_8_item.id)

                                            if order_index > 8 and minute_sum + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) <= minute:
                                                minute_sum += customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day)
                                                res_val += [
                                                            customers_8_item_GO.id,
                                                        ]
                                            elif order_index > 8 and minute_sum + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) > minute:
                                                customers_9_item_i += copy.deepcopy(dests_8.count())-1
                                                if customers_9_item_i == dests_7.count() * (dests_7.count() - 1) * (
                                                    dests_7.count() - 2) and order_index == 9 or order_index == 9 and first_dests.last().id == customers_1_item.id:
                                                    return_while = True
                                                    while_bool = False
                                                    break
                                                continue
                                            else:
                                                print("order_index else = {}".format(order_index))
                                                if minute_sum + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) <= minute:
                                                    minute_sum += customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day)
                                                    res_val += [
                                                                customers_8_item_GO.id,
                                                            ]
                                                    res_val_i = 0
                                                    for res_val_item in res_val:
                                                        if res_val_i > 0:
                                                            result_exc_list.append(copy.deepcopy(res_val_item))
                                                        res_val_i += 1
                                                    result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                                    return_while = True
                                                    break
                                                else:
                                                    if order_index == 8 and first_dests.last().id == customers_1_item.id:
                                                        return_while = True
                                                        while_bool = False
                                                        break
                                                    else:
                                                        continue
                                            # print("customers_8_item  minute_sum= {}".format(minute_sum))
                                            # print("customers_8_item  res_val= {}".format(res_val))
                                            # print("customers_8_item.minute , customers_8_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_8_item.minute , customers_8_item_GO.get_customer_day_minute(week_day)))
                                            print("customers_8_item  res_val= {}, = minute_sum= {}".format(res_val,minute_sum))
                                            dests_9 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_8_item_GO.id) | Q(customer2_id=customers_8_item_GO.id))
                                            if dests_9.count() == 0:
                                                while_bool = False
                                                return_while = True
                                                break
                                            res_val_8 = copy.deepcopy(res_val)
                                            minute_sum_8 = copy.deepcopy(minute_sum)
                                            for customers_9_item in dests_9:
                                                customers_9_item_i += 1
                                                res_val = copy.deepcopy(res_val_8)
                                                minute_sum = copy.deepcopy(minute_sum_8)
                                                if return_while:
                                                    break
                                                if customers_9_item.customer1_id == customers_8_item_GO.id:
                                                    customers_9_item_GO = customers_9_item.customer2
                                                else:
                                                    customers_9_item_GO = customers_9_item.customer1
                                                customers_9_bool = True
                                                # before_vals.append(customers_9_item.id)
                                                # print("customers_9_item  res_val= {}".format(res_val))
                                                # calculate_minute_val =  calculate_minute(res_val,minute_all=minute,dest_list=dest_list)
                                                # return calculate_minute_val
                                                # minute_sum = customers_1_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) + customers_9_item.minute + customers_9_item_GO.get_customer_day_minute(week_day)
                                                if minute_sum + customers_9_item.minute + customers_9_item_GO.get_customer_day_minute(week_day) <= minute:
                                                    minute_sum += customers_9_item.minute + customers_9_item_GO.get_customer_day_minute(week_day)
                                                    res_val += [
                                                                customers_9_item_GO.id
                                                            ]
                                                    res_val_i = 0
                                                    for res_val_item in res_val:
                                                        if res_val_i > 0:
                                                            result_exc_list.append(copy.deepcopy(res_val_item))
                                                        res_val_i += 1
                                                    result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                                    return_while = True
                                                    print("customers_9_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                                                    # print("customers_9_item  minute_sum= {}".format(minute_sum))
                                                    # print("customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)))
                                                    break
                                                else:
                                                    if customers_9_item_i == dests_7.count() * (dests_7.count()-1) * (dests_7.count()-2) or order_index == 9 and first_dests.last().id == customers_1_item.id:
                                                        return_while = True
                                                        while_bool = False
                                                        break
                                                    else:
                                                        continue
                                                # print("customers_9_item  minute_sum= {}".format(minute_sum))
                                                # print("customers_9_item  result_exc_list= {}".format(result_exc_list))
                                                # print("customers_9_item  result_list= {}".format(result_list))
                                                # print("customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)))

    return result_list



def sub_result1p1(week_day,custom_start_id,exc_list, minute):
    from work.models import Customer,CustomerDistance
    from django.forms.models import model_to_dict
    # from work.new_main import calculate_minute_1times as nm_calculate_minute_1times

    # loc_destinations = CustomerDistance.objects.exclude(Q(customer1_id__in=exc_list) | Q(customer2_id__in=exc_list)).filter(Q(customer1_id=current_loc_id) | Q(customer2_id=current_loc_id)).order_by('minute')
    result_list = []
    result_exc_list = []

        # .order_by('-minute', '-customer1__minute','-customer2__minute')
    # dest_list = []
    # for loc_destination_item in loc_destinations:
    #     dest_list.append(obj_to_dict_dest(loc_destination_item))
    loc_destinations = CustomerDistance.objects.exclude(
        Q(customer1_id__in=exc_list) | Q(customer2_id__in=exc_list)).exclude(
        Q(customer1__work_times=3) | Q(customer2__work_times=3)).order_by('minute')
    if custom_start_id == 0:
        main_customer = Customer.objects.filter(our_company=True).first()
    else:
        main_customer = Customer.objects.filter(id=custom_start_id).first()
    # customers = Customer.objects.exclude(id__in=exc_list).exclude(our_company=True)
    while_bool = True
    for_bool = True
    before_vals = []
    customers_11_bool = False
    # res_val = []
    while_bool = True
    return_while = False
    for order_index in range(9, 0, -1):
        if for_bool is False:
            # print('return_while = {}'.format(return_while))
            break
        print('return for')
        # print(while_bool)
        print("<order_index> = <{}>".format(order_index))
        print("<while_bool> = <{}>".format(while_bool))
        print("<return_while> = <{}>".format(return_while))
        while_bool = True
        return_while = False
        print('return for')
        while while_bool:
            print('head yoxdu return_while = {}'.format(return_while))
            # if while_bool is False:
            #     # print('return_while = {}'.format(return_while))
            #     break
            customers_9_item_i = 0
            customers_8_item_i = 0
            customers_7_item_i = 0
            return_while = False
            res_val = [
                       main_customer.id,
                    ]
            # print("while_bool  res_val= {}".format(res_val))
            first_dests = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=main_customer.id) | Q(customer2_id=main_customer.id)).order_by('minute')

            if first_dests.count() == 0:
                # # print('var first_dests= {}'.format(first_dests.count()))
                break
            else:
                pass
            print('foot yoxdu first_dests = {}'.format(first_dests.count()))
            for customers_1_item in first_dests:
                if return_while:
                    # print('return_while = {}'.format(return_while))
                    break
                # for_break = False
                # if for_break:
                #     continue
                # before_vals.append(customers_1_item.id)
                if customers_1_item.customer1_id == main_customer.id:
                    customers_1_item_GO = customers_1_item.customer2
                    customers_1_item_ord = 2
                else:
                    customers_1_item_GO = customers_1_item.customer1
                    customers_1_item_ord = 1
                # res_val += [customers_1_item_GO.id]
                minute_sum = 0
                # print("customers_1_item.minute , customers_1_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_1_item.minute , customers_1_item_GO.get_customer_day_minute(week_day)))
                if order_index > 1 and minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) <= minute:
                    print("@@@@@@@@@@@ if 1")
                    minute_sum += customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day)
                    res_val += [
                        customers_1_item_GO.id,
                    ]
                    # pass
                elif order_index > 1 and minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) > minute:
                    print("@@@@@@@@@@@ elif 1")
                    continue
                else:
                    print("@@@@@@@@@@@ else 1")
                    print("@@@@@@@@@@@ {} - {} - {}".format(order_index , minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) , minute))
                    # print("order_index else = {}".format(order_index))
                    if minute_sum + customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day) <= minute:
                        # print("@@@@@@@@@@@ if 1")
                        minute_sum += customers_1_item.minute + customers_1_item_GO.get_customer_day_minute(week_day)
                        res_val += [
                            customers_1_item_GO.id,
                        ]
                        # print('+++++++++++++++++++++++++++++++++++++++minute_sum <= minute++++++++++++++++++++++++++++++++++++++++')
                        # print('+++++++++++++++++++++++++++++++++++++++ res_val = {} ++++++++++++++++++++++++++++++++++++++++'.format(res_val))
                        res_val_i = 0
                        for res_val_item in res_val:
                            res_val_i += 1
                            if res_val_i > 0:
                                result_exc_list.append(copy.deepcopy(res_val_item))
                        result_list.append(copy.deepcopy(res_val + [[minute_sum]]))

                        return_while = True
                        while_bool = False
                        for_bool = False
                        # print('+++++++++++++++++++++++++++++++++++++++ return_while = {} ++++++++++++++++++++++++++++++++++++++++'.format(return_while))
                        break
                    else:
                        if order_index == 1 and first_dests.last().id == customers_1_item.id:
                            return_while = True
                            while_bool = False
                            break
                        else:
                            continue
                print("customers_1_item  res_val= {}, = minute_sum= {}".format(res_val,minute_sum))
                # print("customers_1_item  minute_sum= {}".format(minute_sum))
                dests_2 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_1_item_GO.id) | Q(customer2_id=customers_1_item_GO.id)).order_by('minute')
                if dests_2.count() == 0:
                    while_bool = False
                    return_while = True
                    print('var dests_2= {}'.format(dests_2.count()))
                    break
                else:
                    print('yoxdu dests_2 = {}'.format(dests_2.count()))
                    pass
                res_val_1 = copy.deepcopy(res_val)
                minute_sum_1 = copy.deepcopy(minute_sum)
                for customers_2_item in dests_2:
                    res_val = copy.deepcopy(res_val_1)
                    minute_sum = copy.deepcopy(minute_sum_1)
                    if return_while:
                        break

                    if customers_2_item.customer1_id == customers_1_item_GO.id:
                        customers_2_item_GO = customers_2_item.customer2
                    else:
                        customers_2_item_GO = customers_2_item.customer1
                    # res_val += [customers_2_item_GO.id]
                    # print("customers_2_item  res_val= {}".format(res_val))
                    # before_vals.append(customers_2_item.id)
                    # minute_sum += customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day)
                    # print("customers_2_item  minute_sum= {}".format(minute_sum))
                    # print("customers_2_item.minute , customers_2_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_2_item.minute , customers_2_item_GO.get_customer_day_minute(week_day)))
                    if order_index > 2 and minute_sum + customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) <= minute:
                        minute_sum += customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day)
                        res_val += [
                            customers_2_item_GO.id,
                        ]
                        # pass
                    elif order_index > 2 and minute_sum + customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) > minute:
                        continue
                    else:
                        print("order_index else = {}".format(order_index))
                        if minute_sum + customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day) <= minute:
                            minute_sum += customers_2_item.minute + customers_2_item_GO.get_customer_day_minute(week_day)
                            res_val += [
                                customers_2_item_GO.id,
                            ]
                            res_val_i = 0
                            for res_val_item in res_val:
                                if res_val_i > 0:
                                    result_exc_list.append(copy.deepcopy(res_val_item))
                                res_val_i += 1
                            result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                            return_while = True
                            while_bool = False
                            for_bool = False
                            break
                        else:
                            if order_index == 2 and first_dests.last().id == customers_1_item.id:
                                return_while = True
                                while_bool = False
                                break
                            else:
                                continue
                    print("customers_2_item  res_val= {}, = minute_sum= {}".format(res_val,minute_sum))
                    dests_3 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_2_item_GO.id) | Q(customer2_id=customers_2_item_GO.id)).order_by('minute')
                    if dests_3.count() == 0:
                        while_bool = False
                        return_while = True
                        print('var dests_3= {}'.format(dests_3.count()))
                        break
                    else:
                        print('yoxdu dests_3= {}'.format(dests_3.count()))
                        pass
                    res_val_2 = copy.deepcopy(res_val)
                    minute_sum_2 = copy.deepcopy(minute_sum)
                    for customers_3_item in dests_3:
                        res_val = copy.deepcopy(res_val_2)
                        minute_sum = copy.deepcopy(minute_sum_2)
                        if return_while:
                            break
                        # before_vals.append(customers_3_item.id)
                        if customers_3_item.customer1_id == customers_2_item_GO.id:
                            customers_3_item_GO = customers_3_item.customer2
                        else:
                            customers_3_item_GO = customers_3_item.customer1
                        # res_val += [customers_3_item_GO.id]
                        # # print("customers_3_item  res_val= {}".format(res_val))
                        # minute_sum += customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day)
                        if order_index > 3 and minute_sum + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) <= minute:
                            minute_sum += customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day)
                            res_val += [
                                customers_3_item_GO.id,
                            ]
                            # pass
                        elif order_index > 3 and minute_sum + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) > minute:
                            continue
                        else:
                            # print("order_index else = {}".format(order_index))
                            if minute_sum + customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day) <= minute:
                                minute_sum += customers_3_item.minute + customers_3_item_GO.get_customer_day_minute(week_day)
                                res_val += [
                                    customers_3_item_GO.id,
                                ]
                                res_val_i = 0
                                for res_val_item in res_val:
                                    if res_val_i > 0:
                                        result_exc_list.append(copy.deepcopy(res_val_item))
                                    res_val_i += 1
                                result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                return_while = True
                                while_bool = False
                                for_bool = False
                                break
                            else:
                                if order_index == 3 and first_dests.last().id == customers_1_item.id:
                                    return_while = True
                                    while_bool = False
                                    break
                                else:
                                    continue
                        # print("customers_3_item  minute_sum= {}".format(minute_sum))
                        # print("customers_3_item.minute , customers_3_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_3_item.minute , customers_3_item_GO.get_customer_day_minute(week_day)))
                        dests_4 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_3_item_GO.id) | Q(customer2_id=customers_3_item_GO.id)).order_by('minute')
                        if dests_4.count() == 0:
                            while_bool = False
                            return_while = True
                            print('var dests_4= {}'.format(dests_4.count()))
                            break
                        else:
                            print('yoxdu dests_4= {}'.format(dests_4.count()))
                            pass
                        res_val_3 = copy.deepcopy(res_val)
                        minute_sum_3 = copy.deepcopy(minute_sum)
                        print("customers_3_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                        for customers_4_item in dests_4:
                            res_val = copy.deepcopy(res_val_3)
                            minute_sum = copy.deepcopy(minute_sum_3)
                            if return_while:
                                break
                            # before_vals.append(customers_4_item.id)
                            if customers_4_item.customer1_id == customers_3_item_GO.id:
                                customers_4_item_GO = customers_4_item.customer2
                            else:
                                customers_4_item_GO = customers_4_item.customer1
                            # res_val += [customers_4_item_GO.id]
                            # # print("customers_4_item  res_val= {}".format(res_val))
                            # minute_sum += customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day)
                            if order_index > 4 and minute_sum + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) <= minute:
                                minute_sum += customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day)
                                res_val += [
                                    customers_4_item_GO.id,
                                ]
                                # pass
                            elif order_index > 4 and minute_sum + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) > minute:
                                continue
                            else:
                                # print("order_index else = {}".format(order_index))
                                if minute_sum + customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day) <= minute:
                                    minute_sum += customers_4_item.minute + customers_4_item_GO.get_customer_day_minute(week_day)
                                    res_val += [
                                        customers_4_item_GO.id,
                                    ]
                                    res_val_i = 0
                                    for res_val_item in res_val:
                                        if res_val_i > 0:
                                            result_exc_list.append(copy.deepcopy(res_val_item))
                                        res_val_i += 1
                                    result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                    return_while = True
                                    while_bool = False
                                    for_bool = False
                                    break
                                else:
                                    if order_index == 4 and first_dests.last().id == customers_1_item.id:
                                        return_while = True
                                        while_bool = False
                                        break
                                    else:
                                        continue
                            # print("customers_4_item  minute_sum= {}".format(minute_sum))
                            # print("customers_4_item.minute , customers_4_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_4_item.minute , customers_4_item_GO.get_customer_day_minute(week_day)))
                            print("customers_4_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                            dests_5 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_4_item_GO.id) | Q(customer2_id=customers_4_item_GO.id)).order_by('minute')
                            if dests_5.count() == 0:
                                while_bool = False
                                return_while = True
                                print('var dests_5= {}'.format(dests_5.count()))
                                break
                            else:
                                pass
                                print('yoxdu dests_5= {}'.format(dests_5.count()))
                            res_val_4 = copy.deepcopy(res_val)
                            minute_sum_4 = copy.deepcopy(minute_sum)
                            for customers_5_item in dests_5:
                                res_val = copy.deepcopy(res_val_4)
                                minute_sum = copy.deepcopy(minute_sum_4)
                                if return_while:
                                    break
                                if customers_5_item.customer1_id == customers_4_item_GO.id:
                                    customers_5_item_GO = customers_5_item.customer2
                                else:
                                    customers_5_item_GO = customers_5_item.customer1
                                # res_val += [customers_5_item_GO.id]
                                print("customers_5_item  res_val= {}".format(res_val))
                                # before_vals.append(customers_5_item.id)
                                if order_index > 5 and minute_sum + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) <= minute:
                                    minute_sum += customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day)
                                    res_val += [
                                        customers_5_item_GO.id,
                                    ]
                                    # pass
                                elif order_index > 5 and minute_sum + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) > minute:
                                    continue
                                else:
                                    print("order_index else = {}".format(order_index))
                                    if minute_sum + customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day) <= minute:
                                        minute_sum += customers_5_item.minute + customers_5_item_GO.get_customer_day_minute(week_day)
                                        res_val += [
                                            customers_5_item_GO.id,
                                        ]
                                        res_val_i = 0
                                        for res_val_item in res_val:
                                            if res_val_i > 0:
                                                result_exc_list.append(copy.deepcopy(res_val_item))
                                            res_val_i += 1
                                        result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                        return_while = True
                                        while_bool = False
                                        for_bool = False
                                        break
                                    else:
                                        if order_index == 5 and first_dests.last().id == customers_1_item.id:
                                            return_while = True
                                            while_bool = False
                                            break
                                        else:
                                            continue
                                # print("customers_5_item  minute_sum= {}".format(minute_sum))
                                # print("customers_5_item.minute , customers_5_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_5_item.minute , customers_5_item_GO.get_customer_day_minute(week_day)))
                                dests_6 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_5_item_GO.id) | Q(customer2_id=customers_5_item_GO.id)).order_by('minute')
                                if dests_6.count() == 0:
                                    while_bool = False
                                    return_while = True
                                    print('var dests_6= {}'.format(dests_6.count()))
                                    break
                                else:
                                    pass
                                    print('yoxdu dests_6= {}'.format(dests_6.count()))
                                res_val_5 = copy.deepcopy(res_val)
                                minute_sum_5 = copy.deepcopy(minute_sum)
                                for customers_6_item in dests_6:
                                    # while_bool = False
                                    # return_while = True
                                    # break
                                    res_val = copy.deepcopy(res_val_5)
                                    minute_sum = copy.deepcopy(minute_sum_5)
                                    if return_while:
                                        break
                                    if customers_6_item.customer1_id == customers_5_item_GO.id:
                                        customers_6_item_GO = customers_6_item.customer2
                                    else:
                                        customers_6_item_GO = customers_6_item.customer1
                                    # res_val += [customers_6_item_GO.id]
                                    # print("customers_6_item  res_val= {}".format(res_val))
                                    # # before_vals.append(customers_6_item.id)
                                    # # minute_sum += customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day)
                                    # print("customers_6_item  minute_sum = {}".format(minute_sum))
                                    # print("customers_6_item.minute , customers_6_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_6_item.minute , customers_6_item_GO.get_customer_day_minute(week_day)))
                                    if order_index > 6 and minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) <= minute:
                                        minute_sum += customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day)
                                        res_val += [
                                            customers_6_item_GO.id,
                                        ]
                                        # pass
                                    elif order_index > 6 and minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) > minute:
                                        print("order_index > 6 and minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) > minute")
                                        customers_8_item_i += (copy.deepcopy(dests_6.count()) - 1) * (
                                        copy.deepcopy(dests_6.count()) - 2)
                                        if customers_8_item_i == dests_6.count() * (dests_6.count() - 1) * (dests_6.count() - 2) or order_index == 8 and first_dests.last().id == customers_1_item.id:
                                            print("customers_8_item_i == dests_6.count() * (dests_6.count() - 1) * (dests_6.count() - 2) or order_index == 8 and first_dests.last().id == customers_1_item.id")
                                            return_while = True
                                            while_bool = False
                                            break
                                        continue
                                    else:
                                        print("6 else")
                                        print("order_index else = {}".format(order_index))
                                        if minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) <= minute:
                                            print("minute_sum + customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day) <= minute")
                                            minute_sum += customers_6_item.minute + customers_6_item_GO.get_customer_day_minute(week_day)
                                            res_val += [
                                                customers_6_item_GO.id,
                                            ]
                                            res_val_i = 0
                                            for res_val_item in res_val:
                                                if res_val_i > 0:
                                                    result_exc_list.append(copy.deepcopy(res_val_item))
                                                res_val_i += 1
                                            result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                            return_while = True
                                            while_bool = False
                                            for_bool = False
                                            break
                                        else:
                                            if order_index == 6 and first_dests.last().id == customers_1_item.id:
                                                return_while = True
                                                while_bool = False
                                                break
                                            else:
                                                continue
                                    dests_7 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_6_item_GO.id) | Q(customer2_id=customers_6_item_GO.id)).order_by('minute')
                                    if dests_7.count() == 0:
                                        while_bool = False
                                        return_while = True
                                        break
                                    res_val_6 = copy.deepcopy(res_val)
                                    minute_sum_6 = copy.deepcopy(minute_sum)
                                    # print("customers_6_item  minute_sum = {}".format(minute_sum))
                                    # print("customers_6_item  res_val = {}".format(res_val))
                                    print("customers_6_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                                    for customers_7_item in dests_7:
                                        # while_bool = False
                                        # return_while = True
                                        # break
                                        res_val = copy.deepcopy(res_val_6)
                                        minute_sum = copy.deepcopy(minute_sum_6)
                                        if return_while:
                                            break
                                        if customers_7_item.customer1_id == customers_6_item_GO.id:
                                            customers_7_item_GO = customers_7_item.customer2
                                        else:
                                            customers_7_item_GO = customers_7_item.customer1
                                        # res_val += [
                                        #             customers_7_item_GO.id,
                                        #         ]
                                        # print("customers_7_item  res_val= {}".format(res_val))
                                        # before_vals.append(customers_7_item.id)
                                        # minute_sum += customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day)
                                        if order_index > 7 and minute_sum + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) <= minute:
                                            minute_sum += customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day)
                                            res_val += [
                                                customers_7_item_GO.id,
                                            ]
                                            # pass
                                        elif order_index > 7 and minute_sum + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) > minute:
                                            customers_8_item_i += copy.deepcopy(dests_7.count()) - 1
                                            customers_9_item_i += (copy.deepcopy(dests_7.count())-1)*(copy.deepcopy(dests_7.count())-2)
                                            if customers_9_item_i == dests_7.count() * (dests_7.count() - 1) * (
                                                dests_7.count() - 2) or order_index == 9 and first_dests.last().id == customers_1_item.id:
                                                return_while = True
                                                while_bool = False
                                                break
                                            if customers_8_item_i == dests_6.count() * (dests_6.count() - 1) * (
                                                dests_6.count() - 2) or order_index == 8 and first_dests.last().id == customers_1_item.id:
                                                return_while = True
                                                while_bool = False
                                                break
                                            continue
                                        else:
                                            print("order_index else = {}".format(order_index))

                                            if minute_sum + customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day) <= minute:
                                                minute_sum += customers_7_item.minute + customers_7_item_GO.get_customer_day_minute(week_day)
                                                res_val += [
                                                    customers_7_item_GO.id,
                                                ]
                                                res_val_i = 0
                                                for res_val_item in res_val:
                                                    if res_val_i > 0:
                                                        result_exc_list.append(copy.deepcopy(res_val_item))
                                                    res_val_i += 1
                                                result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                                return_while = True
                                                while_bool = False
                                                for_bool = False
                                                break
                                            else:
                                                if order_index == 7 and first_dests.last().id == customers_1_item.id:
                                                    return_while = True
                                                    while_bool = False
                                                    break
                                                else:
                                                    continue
                                        # print("customers_7_item update  res_val= {}".format(res_val))
                                        # print("customers_7_item  minute_sum= {}".format(minute_sum))
                                        # print("customers_7_item.minute , customers_7_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_7_item.minute , customers_7_item_GO.get_customer_day_minute(week_day)))
                                        dests_8 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_7_item_GO.id) | Q(customer2_id=customers_7_item_GO.id)).order_by('minute')
                                        if dests_8.count() == 0:
                                            while_bool = False
                                            return_while = True
                                            break
                                        res_val_7 = copy.deepcopy(res_val)
                                        minute_sum_7 = copy.deepcopy(minute_sum)
                                        print("customers_7_item  res_val= {}, = minute_sum= {}".format(res_val,
                                                                                                       minute_sum))
                                        for customers_8_item in dests_8:
                                            customers_8_item_i += 1
                                            res_val = copy.deepcopy(res_val_7)
                                            minute_sum = copy.deepcopy(minute_sum_7)
                                            if return_while:
                                                break
                                            if customers_8_item.customer1_id == customers_7_item_GO.id:
                                                customers_8_item_GO = customers_8_item.customer2
                                            else:
                                                customers_8_item_GO = customers_8_item.customer1
                                            # before_vals.append(customers_8_item.id)

                                            if order_index > 8 and minute_sum + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) <= minute:
                                                minute_sum += customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day)
                                                res_val += [
                                                            customers_8_item_GO.id,
                                                        ]
                                            elif order_index > 8 and minute_sum + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) > minute:
                                                customers_9_item_i += copy.deepcopy(dests_8.count())-1
                                                if customers_9_item_i == dests_7.count() * (dests_7.count() - 1) * (
                                                    dests_7.count() - 2) and order_index == 9 or order_index == 9 and first_dests.last().id == customers_1_item.id:
                                                    return_while = True
                                                    while_bool = False
                                                    break
                                                continue
                                            else:
                                                print("order_index else = {}".format(order_index))
                                                if minute_sum + customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day) <= minute:
                                                    minute_sum += customers_8_item.minute + customers_8_item_GO.get_customer_day_minute(week_day)
                                                    res_val += [
                                                                customers_8_item_GO.id,
                                                            ]
                                                    res_val_i = 0
                                                    for res_val_item in res_val:
                                                        if res_val_i > 0:
                                                            result_exc_list.append(copy.deepcopy(res_val_item))
                                                        res_val_i += 1
                                                    result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                                    return_while = True
                                                    while_bool = False
                                                    for_bool = False
                                                    break
                                                else:
                                                    if order_index == 8 and first_dests.last().id == customers_1_item.id:
                                                        return_while = True
                                                        while_bool = False
                                                        break
                                                    else:
                                                        continue
                                            # print("customers_8_item  minute_sum= {}".format(minute_sum))
                                            # print("customers_8_item  res_val= {}".format(res_val))
                                            # print("customers_8_item.minute , customers_8_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_8_item.minute , customers_8_item_GO.get_customer_day_minute(week_day)))
                                            print("customers_8_item  res_val= {}, = minute_sum= {}".format(res_val,minute_sum))
                                            dests_9 = loc_destinations.exclude(Q(customer1_id__in=result_exc_list) | Q(customer2_id__in=result_exc_list)).exclude(Q(customer1_id__in=res_val[:-1]) | Q(customer2_id__in=res_val[:-1])).filter(Q(customer1_id=customers_8_item_GO.id) | Q(customer2_id=customers_8_item_GO.id))
                                            if dests_9.count() == 0:
                                                while_bool = False
                                                return_while = True
                                                break
                                            res_val_8 = copy.deepcopy(res_val)
                                            minute_sum_8 = copy.deepcopy(minute_sum)
                                            for customers_9_item in dests_9:
                                                customers_9_item_i += 1
                                                res_val = copy.deepcopy(res_val_8)
                                                minute_sum = copy.deepcopy(minute_sum_8)
                                                if return_while:
                                                    break
                                                if customers_9_item.customer1_id == customers_8_item_GO.id:
                                                    customers_9_item_GO = customers_9_item.customer2
                                                else:
                                                    customers_9_item_GO = customers_9_item.customer1
                                                customers_9_bool = True
                                                if minute_sum + customers_9_item.minute + customers_9_item_GO.get_customer_day_minute(week_day) <= minute:
                                                    minute_sum += customers_9_item.minute + customers_9_item_GO.get_customer_day_minute(week_day)
                                                    res_val += [
                                                                customers_9_item_GO.id
                                                            ]
                                                    res_val_i = 0
                                                    for res_val_item in res_val:
                                                        if res_val_i > 0:
                                                            result_exc_list.append(copy.deepcopy(res_val_item))
                                                        res_val_i += 1
                                                    result_list.append(copy.deepcopy(res_val + [[minute_sum]]))
                                                    return_while = True
                                                    while_bool = False
                                                    for_bool = False
                                                    print("customers_9_item  res_val= {}, = minute_sum= {}".format(res_val, minute_sum))
                                                    # print("customers_9_item  minute_sum= {}".format(minute_sum))
                                                    # print("customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)))
                                                    break
                                                else:
                                                    if customers_9_item_i == dests_7.count() * (dests_7.count()-1) * (dests_7.count()-2) or order_index == 9 and first_dests.last().id == customers_1_item.id:
                                                        return_while = True
                                                        while_bool = False
                                                        break
                                                    else:
                                                        continue
                                                # print("customers_9_item  minute_sum= {}".format(minute_sum))
                                                # print("customers_9_item  result_exc_list= {}".format(result_exc_list))
                                                # print("customers_9_item  result_list= {}".format(result_list))
                                                # print("customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)= {}  ,   {}".format(customers_9_item.minute , customers_9_item_GO.get_customer_day_minute(week_day)))

    return result_list

