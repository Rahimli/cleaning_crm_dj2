from django.db import models

# Create your models here.
from django.db.models import signals

from general.functions import get_nearest_week_day_date



class PlanLog(models.Model):
    complated = models.BooleanField(default=False)
    rejcected = models.BooleanField(default=False)
    confirm_date = models.DateField(blank=True,null=True)
    planned_date = models.DateField(blank=True,null=True)
    update_date = models.DateTimeField(auto_now=True,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.date.strftime("%d %b %Y %H:%M:%S")

def planlog_post_save(sender, instance,created, signal, *args, **kwargs):
    if instance.confirm_date and instance.planned_date is None:
        date_str = "{}-{}-{}".format(instance.confirm_date.year,instance.confirm_date.month,instance.confirm_date.day)
        print("date_str={}".format(date_str))   
        instance.planned_date = get_nearest_week_day_date(date_str,1)
        # instance.planned_date = date_str
        print(instance.planned_date)
        instance.save()
signals.post_save.connect(planlog_post_save, sender=PlanLog)
#




class PlanSettings(models.Model):
    title = models.CharField(max_length=255)
    main_plan = models.BooleanField(default=False)
    sub_plan = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title



class WorkSettings(models.Model):
    title = models.CharField(max_length=255)
    time_of_floor = models.DecimalField(max_digits=19, decimal_places=2)
    time_of_bathroom = models.DecimalField(max_digits=19, decimal_places=2)
    time_of_kitchen = models.DecimalField(max_digits=19, decimal_places=2)
    active = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title



#
# class Team(models.Model):
#     status = models.BooleanField(default=True)
#     first_name = models.CharField(max_length=255)
#     last_name = models.CharField(max_length=255)
#     address = models.TextField(blank=True,null=True)
#     email = models.EmailField(max_length=255,unique=True)
#     password = models.CharField(max_length=255)
#     date = models.DateTimeField(auto_now_add=True)
#     def __str__(self):
#         return self.first_name
#
# class WorkSettings(models.Model):
#     title = models.CharField(max_length=255)
#     main_plan = models.BooleanField(default=False)
#     sub_plan = models.BooleanField(default=False)
#     date = models.DateTimeField(auto_now_add=True)
#
# class Customer(models.Model):
#     our_company = models.BooleanField(default=False)
#     status = models.BooleanField(default=True)
#     name = models.CharField(max_length=255)
#     minute = models.PositiveIntegerField()
#     work_times = models.PositiveIntegerField(default=1)
#     image = models.ImageField(upload_to=path_and_rename,blank=True,null=True,max_length=500)
#     address = models.TextField()
#     position = GeopositionField()
#     date = models.DateTimeField(auto_now_add=True)
#
# class TeamWorkDay(models.Model):
#     team = models.ForeignKey("Team")
#     day = models.ForeignKey("WorkDay")
#     minute = gen_field.IntegerRangeField(min_value=0, max_value=1440, default=0, choices=Hours_CHOICES)
#
#     class Meta:
#         unique_together = (("team", "day"),)
#
#
#
# class CustomerDistance(models.Model):
#     customer1 = models.ForeignKey('Customer',related_name='+')
#     customer2 = models.ForeignKey('Customer',related_name='+')
#     main_company = models.BooleanField(default=False)
#     minute = models.IntegerField()
#     distance = models.DecimalField(max_digits=19,decimal_places=2)
#     def __str__(self):
#         return "{} - {} - {} - {}".format(self.customer1.name,self.customer2.name,self.distance,self.minute)
#
#
#
#
#
#
# class PlanTeamWork(models.Model):
#     team = models.ForeignKey("Team")
#     week = gen_field.IntegerRangeField(min_value=1, max_value=4)
#     day = models.ForeignKey("WorkDay")
#     minute = gen_field.IntegerRangeField(min_value=0, max_value=1440,default=0)
#     difference_minute = gen_field.IntegerRangeField(min_value=0, max_value=1440,default=0)
#     is_empty = models.BooleanField(default=True)
#     class Meta:
#         unique_together = (("team", "week", "day"),)
#     def get_customers(self):
#         return CustomerOrder.objects.filter(plan_team_work=self).order_by('order_index')
#     def get_last_customer(self):
#         return CustomerOrder.objects.filter(plan_team_work=self).order_by('order_index').last()
#     def get_customer(self,l_id):
#         return CustomerOrder.objects.filter(plan_team_work=self,customer_id=l_id).first()
#     def __str__(self):
#         return "{} - week={}   -  day={}".format(self.team,self.week,self.day.day)
#
#
#
# class CustomerOrder(models.Model):
#     plan_team_work = models.ForeignKey(PlanTeamWork)
#     order_index = gen_field.IntegerRangeField(min_value=1)
#     customer = models.ForeignKey('Customer')
#     main_process = models.BooleanField(default=False)
#     def __str__(self):
#         return "{} ----- {}. {}".format(self.plan_team_work,self.order_index,self.customer.name)
#
#
#
# class PlanLog(models.Model):
#     complated = models.BooleanField(default=False)
#     rejcected = models.BooleanField(default=False)
#     update_date = models.DateTimeField(auto_now=True,blank=True,null=True)
#     date = models.DateTimeField(auto_now_add=True)
#     def __str__(self):
#         return self.date.strftime("%d %b %Y %H:%M:%S")
#
#
# class DistanceErrorLog(models.Model):
#     customer1 = models.ForeignKey('Customer',related_name='+')
#     customer2 = models.ForeignKey('Customer',related_name='+')
#     # update_date = models.DateTimeField(auto_now=True,blank=True,null=True)
#     date_time = models.DateTimeField(auto_now_add=True)
#     date = models.DateField(auto_now_add=True)
#     def __str__(self):
#         return "{} - {} {} ".format(self.customer1.name,self.customer2.name,self.date.strftime("%d %b %Y %H:%M:%S"))
#
