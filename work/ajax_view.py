from django.contrib.auth.decorators import login_required
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.utils import timezone

from general.functions import check_permission_for_admins
from home.views import base_auth
from work.models import Snippet, SNIPPET_EMPLOYEE_CHOICE, SNIPPET_CUSTOMER_CHOICE


@login_required(login_url='base-user:login')
def calculate_task(request):
    user = request.user
    if check_permission_for_admins(request,'task-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        pass




@login_required(login_url='base-user:login')
def get_snippet_ajax(request,f_slug):
    user = request.user
    # if check_permission_for_admins(request,'task-edit') is False:
    #     raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if request.method == 'POST' and request.is_ajax():
        _html = ''
        data_list = ()
        if f_slug == 'option-list':
            snippet_user_type = int(request.POST.get('snippet_user_type', ''))
            if snippet_user_type == 1:
                data_list = SNIPPET_EMPLOYEE_CHOICE
            elif snippet_user_type == 2:
                data_list = SNIPPET_CUSTOMER_CHOICE
            for data_list_item in data_list:
                _html = "{}{}".format(_html, '<option value="{}">{}</option>'.format(data_list_item[0],data_list_item[1]))
        elif f_slug == 'drag-list':
            type = int(request.POST.get('user_type', ''))
            snippets = Snippet.objects.filter(snippet_user_type=type)
            for snippet_item in snippets:
                _html = "{}{}".format(_html,"#{} ,  ".format(snippet_item.snippet))
        data = {
            'result':_html
        }
        return JsonResponse(data)
    else:
        raise Http404
