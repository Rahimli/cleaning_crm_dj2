"""cleaning_crm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.conf import settings
from django.urls import path, include,re_path
from django.conf.urls.i18n import i18n_patterns
from django.views import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.utils import translation
from django.views.static import serve
from django.contrib.auth.decorators import login_required


from core import set_lang as core_views
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token


urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('administration/', admin.site.urls,name="admin"),

    # path(r'api-auth/', include('rest_framework.urls')),
    path('auth/', include('djoser.urls')),
    path('auth/', include('djoser.urls.authtoken')),

    path('api/v1/', include("api.urls"), name="api"),
    path('api/v1/chat/', include('chat.uris')),
    path('api/v1/customer/', include('api_customer.urls',namespace="customer-api")),
    path('api/v1/employee/', include('api_employee.urls',namespace="employee-api")),
    path('api/v1/administration/', include('api_administration.urls',namespace="administration-api")),
    # path(r'api-token-auth/', obtain_jwt_token),
    # path(r'api-token-refresh/', refresh_jwt_token),
    # path(r'api-token-verify/', verify_jwt_token),

    # path(r'tinymce/', include('tinymce.urls')),
     path(r'summernote/', include('django_summernote.urls')),
    # path(r'ckeditor/', include('ckeditor_uploader.urls')),
        path(r'media/(?P<path>.*)', static.serve, {'document_root': settings.MEDIA_ROOT }, name='media')
]
translation.activate(settings.LANGUAGE_CODE)

urlpatterns += i18n_patterns(
    # path(r'test/$', index, name="index"),
    path('set-language/',core_views.set_language,  name='set_language'),
    # path(r'', include("timeopt.core.urls", namespace="core")),
    # path('api/', include('chat.uris')),
    path('', include('home.urls', namespace='home')),
    # path('', include("home.urls",namespace='home')),
    path('', include("work.urls", namespace="work")),
    path('auth-user/', include("base_user.urls", namespace="base-user")),
    path('messenger/', include("messenger.urls", namespace="messenger")),
    path('general/', include("general.urls", namespace="general")),

    # path(r'', include("tour.urls", namespace="tour")),
    # path(r'', include("userprofile.urls", namespace="userprofile")),
    # path(r'account/', include("base_user.urls", namespace="account")),
)

urlpatterns += staticfiles_urlpatterns()




@login_required
def protected_serve(request, path, document_root=None, show_indexes=False):
    return serve(request, path, document_root, show_indexes)


if settings.DEBUG is False:   #if DEBUG is True it will be served automatically
    urlpatterns += [
        path(r'static/(?P<path>.*)$', static.serve, {'document_root': settings.STATIC_ROOT }, name='static'),
        path(r'media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT }, name='media')
    ]
urlpatterns += staticfiles_urlpatterns()
