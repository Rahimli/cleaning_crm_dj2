from django.utils.translation import ugettext_lazy as _


rule_models_choices = (
    ("customer-list",_('Customer List')),
    ("customer-excel-upload",_('Customer Excel Upload')),
    ("customer-all-payments-list",_('Customer Payment List')),
    ("customer-create",_('Customer Create')),
    ("customer-edit",_('Customer Edit')),
    ("customer-delete",_('Customer Delete')),

    ("employee-list",_('Employee List')),
    ("employee-create",_('Employee Create')),
    ("employee-edit",_('Employee Edit')),
    ("employee-delete",_('Employee Delete')),

    ("subcontractor-list", _('Subcontractor List')),
    ("subcontractor-create",_('Subcontractor Create')),
    ("subcontractor-edit",_('Subcontractor Edit')),
    ("subcontractor-delete",_('Subcontractor Delete')),

    ("team-list", _('Team List')),
    ("team-create",_('Team Create')),
    ("team-edit",_('Team Edit')),
    ("team-delete",_('Team Delete')),

    ("complaint-list", _('Complaint List')),
    ("complaint-create",_('Complaint Create')),
    ("complaint-edit",_('Complaint Edit')),
    ("complaint-delete",_('Complaint Delete')),

    ("reminder-list", _('Reminder List')),
    ("reminder-create",_('Reminder Create')),
    ("reminder-edit",_('Reminder Edit')),
    ("reminder-delete",_('Reminder Delete')),

    ("task-list", _('Task List')),
    ("task-create",_('Task Create')),
    ("task-edit",_('Task Edit')),
    ("task-delete",_('Task Delete')),

    ("service-list", _('Service List')),
    ("service-create",_('Service Create')),
    ("service-edit",_('Service Edit')),
    ("service-delete",_('Service Delete')),

    ("cleaning-program-list", _('Cleaning Program List')),
    ("cleaning-program-create",_('Cleaning Program Create')),
    ("cleaning-program-edit",_('Cleaning Program Edit')),
    ("cleaning-program-delete",_('Cleaning Program Delete')),

    ("contract-template-list", _('Contract Template List')),
    ("contract-template-create",_('Contract Template Create')),
    ("contract-template-edit",_('Contract Template Edit')),
    ("contract-template-delete",_('Contract Template Delete')),

    ("snippet-list", _('Snippet List')),
    ("snippet-create",_('Snippet Create')),
    ("snippet-edit",_('Snippet Edit')),
    ("snippet-delete",_('Snippet Delete')),

    ("economy",_('Economy')),
    ("work-plans",_('Work plans')),
    ("calendar-show",_('Calendar show')),
    ("salary",_('Salary')),
    ("settings",_('Settings')),
)


DayCHOICES = (
    (1, _("Monday")),
    (2, _("Tuesday")),
    (3, _("Wednesday")),
    (4, _("Thursday")),
    (5, _("Friday")),
    (6, _("Saturday")),
    (7, _("Sunday")),
)

Hours_CHOICES = (
    ('', _("Choose Hours")),
    (60, _("1 Hours")),
    (120, _("2 Hours")),
    (180, _("3 Hours")),
    (240, _("4 Hours")),
    (300, _("5 Hours")),
    (360, _("6 Hours")),
    (420, _("7 Hours")),
    (480, _("8 Hours")),
)

SNIPPET_CONRACT_USERTYPE_CHOICE = (
    (1,_("Employee")),
    (2,_("Customer")),
)

SNIPPET_EMPLOYEE_CHOICE = (
    ('employee_type',_('Employee type')),
    ('subcontractor',_('Subcontractor')),
    ('full_name',_('Full name')),
    ('cpr_number',_('Cpr number')),
    ('birth_date',_('Birthdate')),
    ('phonenumber',_('Phone number')),
    ('street_name',_('Street_name')),
    ('zip_code',_('Zip code')),
    ('city',_('city')),
    ('start_date',_('Start date')),
    ('employment_type',_('Employment type')),
    ('amount_of_hours',_('Amount of hours')),
    ('hourly_rate',_('Hourly rate')),
    ('social_security_card',_('Social security card')),
    ('picture',_('picture')),
)

SNIPPET_CUSTOMER_CHOICE = (
    ('customer_type',_('Customer type')),
    ('full_name',_('Full name')),
    ('company_name',_('company name')),
    ('contact_person',_('contact_person')),
    ('cvr_number',_('Cvr number')),
    ('phonenumber',_('Phone number')),
    ('street_name',_('Street_name')),
    ('zip_code',_('Zip code')),
    ('city',_('city')),
    ('start_date',_('Start date')),
    ('square_meters',_('Square meters')),
    ('amount_of_bathrooms',_('Amount of bathrooms')),
    ('amount_of_kitchens',_('Amount of kitchens')),
    ('amount_of_floors',_('Amount of floors')),
    ('work_times',_('Work times')),
    ('address',_('Address')),
)



SNIPPET_EMPLOYEE_CUSTOMER_CHOICE = (
    ('employee_type',_('Employee type')),
    ('subcontractor',_('Subcontractor')),
    ('cpr_number',_('Cpr number')),
    ('birth_date',_('Birthdate')),

    ('employment_type',_('Employment type')),
    ('amount_of_hours',_('Amount of hours')),
    ('hourly_rate',_('Hourly rate')),
    ('social_security_card',_('Social security card')),
    ('picture',_('picture')),

    ('customer_type',_('customer type')),
    ('full_name',_('Full name')),
    ('company_name',_('company name')),
    ('contact_person',_('contact_person')),
    ('cvr_number',_('Cvr number')),
    ('phonenumber',_('Phone number')),
    ('street_name',_('Street_name')),
    ('zip_code',_('Zip code')),
    ('city',_('city')),
    ('start_date',_('Start date')),
    ('square_meters',_('Square meters')),
    ('amount_of_bathrooms',_('Amount of bathrooms')),
    ('amount_of_kitchens',_('Amount of kitchens')),
    ('amount_of_floors',_('Amount of floors')),
    ('work_times',_('Work times')),
    ('address',_('Address')),
)











WorkTimesCHOICES = (
    ('', _("Choose Week")),
    (1, _("Every Week")),
    (2, _("2 Times a Week")),
    (4, _("4 Times a Week")),
)