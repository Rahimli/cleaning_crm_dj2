from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from rest_framework.generics import ListCreateAPIView

from .views import *
from django.urls import path


app_name = 'api'
urlpatterns = [
    path('', ApiRoot.as_view(),name='api-root'),
    # path('employees/list/', EmployeeListApiView.as_view(),name='employee-list'),
    # path('employees/add/', EmployeeCreateApiView.as_view(),name='employee-add'),
    # path('subcontractors/list/', SubcontractorListApiView.as_view(),name='subcontractor-list'),
    # path('subcontractors/add/', SubcontractorCreateApiView.as_view(),name='subcontractor-add'),
    # path('subcontractors/view/(?P<pk>[0-9]+)/', SubcontractorRetrieveView.as_view(),name='subcontractor-view'),
    # path('subcontractors/edit/(?P<pk>\d+)/$', SubcontractorUpdateAPIView.as_view(),name='subcontractor-edit'),
	# path('logout/$', log_out, name='logout'),

]
