import os

from work.common import SNIPPET_EMPLOYEE_CHOICE, DayCHOICES
from work.models import week_days_choices


import datetime


def get_28_days_dates(date_str):
    datetime.datetime.now()
    date_time_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    week_day = date_time_obj.isoweekday()
    date_list = []
    for i in range(1,29):
        print(i)
        diff = week_day - i
        date_list.append(date_time_obj - datetime.timedelta(days=diff))
    return date_list





def get_week(day):

    if day == 0:
      return_val = 1
    elif day % 7 == 0:
        return_val = int(day/7)
    else:
        return_val = int(day/7) + 1
    return return_val

def get_day_from_days(day):

    if day % 7 == 0:
        return_val = 7
    else:
        return_val = int(day % 7)
    return return_val

def get_day_name(day):
    return_val = ''
    for dc in DayCHOICES:
        if dc[0] == day:
            return_val = dc[1]
    return str(return_val)


def get_day(date_str):
    from datetime import datetime, date

    d = datetime.now() - datetime.strptime(date_str, '%b %d %Y')
    return d.days + 1



def get_date_type_custom(task_num):
    return_val = ''
    for week_days_choices_item in week_days_choices:
        if week_days_choices_item[0] == task_num:
            return_val = week_days_choices_item[1]
            break
    return return_val






def customer_calculate_task(customer_obj,cleaning_obj):
    return_val = []

    return return_val

def employee_contract_converter(contract_html,emp_obj):
    contract_html = str(contract_html)
    print("emp_obj.employee_type={}".format(emp_obj.employee_type))
    for choice_item in SNIPPET_EMPLOYEE_CHOICE:
        if choice_item[0] == 'employee_type':
            contract_html = contract_html.replace('#employee_type',str(emp_obj.employee_type))
        if choice_item[0] == 'subcontractor':
            try:
                contract_html = contract_html.replace('#subcontractor',str(emp_obj.subcontractor.company_name))
            except:
                pass
        if choice_item[0] == 'full_name':
            try:
                contract_html = contract_html.replace('#full_name',str(emp_obj.full_name))
            except:
                pass

        if choice_item[0] == 'cpr_number':
            try:
                contract_html = contract_html.replace('#cpr_number',str(emp_obj.cpr_number))
            except:
                pass
        if choice_item[0] == 'birth_date':
            try:
                contract_html = contract_html.replace('#birth_date',str(emp_obj.birth_date))
            except:
                pass
        if choice_item[0] == 'phonenumber':
            try:
                contract_html = contract_html.replace('#phonenumber',str(emp_obj.phonenumber))
            except:
                pass
        if choice_item[0] == 'street_name':
            try:
                contract_html = contract_html.replace('#street_name',str(emp_obj.street_name))
            except:
                pass
        if choice_item[0] == 'zip_code':
            try:
                contract_html = contract_html.replace('#zip_code',str(emp_obj.zip_code))
            except:
                pass
        if choice_item[0] == 'city':
            try:
                contract_html = contract_html.replace('#city',str(emp_obj.city))
            except:
                pass
        if choice_item[0] == 'start_date':
            try:
                contract_html = contract_html.replace('#start_date',str(emp_obj.start_date))
            except:
                pass
        if choice_item[0] == 'employment_type':
            try:
                contract_html = contract_html.replace('#employment_type',str(emp_obj.employment_type))
            except:
                pass
        if choice_item[0] == 'amount_of_hours':
            try:
                contract_html = contract_html.replace('#amount_of_hours',str(emp_obj.amount_of_hours))
            except:
                pass
        if choice_item[0] == 'employment_type':
            try:
                contract_html = contract_html.replace('#hourly_rate',str(emp_obj.hourly_rate))
            except:
                pass
        if choice_item[0] == 'social_security_card':
            try:
                contract_html = contract_html.replace('#social_security_card',str(emp_obj.social_security_card))
            except:
                pass
        if choice_item[0] == 'picture':
            try:
                contract_html = contract_html.replace('#social_security_card',str(emp_obj.picture))
            except:
                pass
    return contract_html





def read_customer_from_excel(url):
    from copy import deepcopy
    import xlrd
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    file_location = "{}{}".format(BASE_DIR,url)
    work_book = xlrd.open_workbook(file_location)
    sheet = work_book.sheet_by_index(0)
    nrows = sheet.nrows
    ncols = sheet.ncols
    all_list = []
    for nrows_item in range(nrows):
        result = ""
        if nrows_item > 0:
            email = ""
            customer_type = 0
            full_name = ""
            company_name = ""
            contact_person = ""
            cvr_number = 0
            phonenumber = 0
            street_name = ""
            zip_code = 0
            city = ""
            start_date = ""
            square_meters = 0
            amount_of_bathrooms = 0
            amount_of_kitchens = 0
            amount_of_floors = 0
            address = ""
            latitude = 0
            longitude = 0
            # ncols_i = 0
            for ncols_item in range(ncols):
                if ncols_item == 0:
                    email = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 1:
                    customer_type = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 2:
                    full_name = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 3:
                    company_name = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 5:
                    contact_person = sheet.cell_value(nrows_item,ncols_item)
                # result = "{} - {}.{} {}".format(result,nrows_item,ncols_item,sheet.cell_value(nrows_item,ncols_item))
                if ncols_item == 11:
                    cvr_number = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    phonenumber = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    street_name = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    zip_code = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    city = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    start_date = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    square_meters = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    amount_of_bathrooms = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    amount_of_kitchens = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    amount_of_floors = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 11:
                    address = sheet.cell_value(nrows_item,ncols_item)


                if ncols_item == 13:
                    latitude = sheet.cell_value(nrows_item,ncols_item)
                if ncols_item == 14:
                    longitude = sheet.cell_value(nrows_item,ncols_item)



                result = "{} - {}.{} {}".format(result,nrows_item,ncols_item,sheet.cell_value(nrows_item,ncols_item))
            all_list.append([
                    deepcopy(email),
                    int(deepcopy(customer_type)),
                    deepcopy(full_name),
                    deepcopy(company_name),
                    deepcopy(contact_person),
                    int(cvr_number),
                    int(phonenumber),
                    deepcopy(street_name),
                    int(zip_code),
                    deepcopy(city),
                    deepcopy(start_date),
                    float(deepcopy(square_meters)),
                    int(deepcopy(amount_of_bathrooms)),
                    int(deepcopy(amount_of_kitchens)),
                    int(deepcopy(amount_of_floors)),
                    deepcopy(address),
                    deepcopy(float(latitude)),
                    deepcopy(float(longitude)),
            ])
    print("---------------------------------------------------------------")
    print(all_list)
    print(len(all_list))
    print("---------------------------------------------------------------")
    return all_list

