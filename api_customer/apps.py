from django.apps import AppConfig


class ApiCustomerConfig(AppConfig):
    name = 'api_customer'
