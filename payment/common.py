from django.utils.translation import ugettext as _




PAYMENT_TYPE = (
    ('',_('Customer Type')),
    ('daily',_('Daily')),
    ('weekly',_('Weekly')),
    ('monthly',_('Montly')),
    ('general',_('General')),
)