import copy

from celery import shared_task

from work.functions import read_customer_from_excel
from work.models import ConfirmedContract, Task


def get_mimtype(file_path):
    import mimetypes
    # magic.from_file("testdata/test.pdf")
    # 'PDF document, version 1.2'
    # >> > magic.from_buffer(open("testdata/test.pdf").read(1024))
    'PDF document, version 1.2'
    return_val = ''
    try:
        file_type = mimetypes.guess_type(file_path)
        return_val = file_type[0]
    except:
        pass
    print("return_val={}".format(return_val))
    return return_val



@shared_task
def add_location_from_excel_list(url):
    from work.models import Customer
    from geoposition import Geoposition
    import time
    all_list = read_customer_from_excel(url)
    # print(all_list)
    all_customer = Customer.objects.filter()
    all_list_item_i = 0
    all_list_item_general_i = all_customer.count()
    if len(all_list):
        for all_list_item in all_list:
            all_list_item_general_i += 1
            if all_customer.filter(name=all_list_item[0],address=all_list_item[1]).count()==0:
                # print("all_customer.filter(name=all_list_item[0],address=all_list_item[1])")
                all_list_item_i+=1
                slip_time = 0.13 * all_list_item_general_i
                # if all_list_item_i > 100 and all_list_item_i <= 200 :
                #     time.sleep(1.5)
                # elif all_list_item_i > 200 and all_list_item_i <= 300 :
                #     time.sleep(2)
                # elif all_list_item_i > 300 and all_list_item_i <= 400 :
                #     time.sleep(2.5)
                # elif all_list_item_i > 400 and all_list_item_i <= 500 :
                #     time.sleep(3)
                # elif all_list_item_i > 500:
                #     time.sleep(3.5)
                if all_list_item_i < 16:
                    Customer.objects.create(
                                            name=all_list_item[0],
                                            address=all_list_item[1],
                                            status = True,
                                            our_company = False,
                                            minute = all_list_item[3],
                                            work_times = all_list_item[2],
                                            position =  Geoposition(float(all_list_item[4]), float(all_list_item[5]))
                                            )
                    time.sleep(slip_time)
                # else:
                #     break

    return '{} customer created with success!'.format(all_list_item_i)


@shared_task
def montly_payment_customer():
    import stripe
    from work.models import Customer, Task, PlanTeamWorkDate
    from django.conf import settings
    stripe.api_key = settings.STRIPE_SECRET
    customers = Customer.objects.exclude(plan_start_date=None)
    for customer_item in customers:
        plan_team_work_dates = PlanTeamWorkDate.objects.filter().first()

    charge = stripe.Charge.create(
        amount=1500,  # $15.00 this time
        currency='usd',
        customer='cus_Di6orVqQIHec0q',  # Previously stored, then retrieved
    )
    return True

@shared_task
def send_html_mail(email,subject,html):
    from django.conf import settings
    from django.core.mail import send_mail
    send_mail(
        subject=subject,
        html_message=html,
        message='',
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[email],
        fail_silently=False
    )


    return True



@shared_task
def send_atachment_mail(email,subject,html,attach_path):
    import os
    from django.conf import settings
    from django.core.mail import send_mail, EmailMessage
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    mail = EmailMessage(subject=subject,body=html,to=[email],from_email=settings.EMAIL_HOST_USER)
    mail.attach_file(os.path.join(BASE_DIR)+ attach_path,get_mimtype(attach_path))
    mail.content_subtype = html
    mail.send()
    return True



@shared_task
def send_contract_html_mail(confirmed_contract_id,contract_html_generated,email,subject,html):
    import os
    from work.models import ConfirmedContract
    from django.conf import settings
    from django.core.mail import EmailMessage
    import pdfkit
    from uuid import uuid4
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    print('geldik')
    pdffile_name = 'contract-template-{}-{}.pdf'.format(uuid4().hex, uuid4().hex)
    upload_to = 'media/files/contract-generate-files/{}'.format(pdffile_name)
    print('winter is comming')
    pdfkit.from_string(contract_html_generated, os.path.join(BASE_DIR) + "{}{}".format('/', upload_to))
    print('winter was came')
    mail = EmailMessage(subject,html,settings.EMAIL_HOST_USER,[email])
    # msg = EmailMessage(subject, html_content, from_email, [to])
    mail.content_subtype = 'html'
    try:
        confirmed_contract_obj = ConfirmedContract.objects.get(uid_key=confirmed_contract_id)
        confirmed_contract_obj.contract_url = "{}{}".format('/',upload_to)
        confirmed_contract_obj.save()
    except:
        pass
    file_full_path = os.path.join(BASE_DIR) +"{}{}".format('/',upload_to)
    mail.attach_file(file_full_path,get_mimtype( "{}{}".format('/',upload_to) ))
    print('sending...')
    mail.send()
    print('sent')
    return True





