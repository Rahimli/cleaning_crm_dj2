
@shared_task
def create_customer_distance_customers(loc_id):
    from django.db.models import Q
    from .models import Customer
    from .models import CustomerDistance , PlanTeamWork , CustomerOrder, DistanceErrorLog
    from general.task_functions import distance_2_point as nm_distance_2_point
    customers = Customer.objects.filter().exclude(id=loc_id)
    # the list that will hold the bulk insert
    bulk_customer_distances = []
    main_customer = Customer.objects.filter(our_company=True).first()

    # # loop that list and make those game objects
    # CustomerDistance.objects.filter().delete()
    # # print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    # # print(loc_id)
    customer1 = Customer.objects.get(status=True,id=loc_id)
    # # print(customer1.name)
    # # print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    # if customer1.id > 0:
    for customer2 in customers:
        # # print("CustomerDistance.objects.filter(Q(customer1=customer1,customer2=customer2) | Q(customer1=customer2,customer2=customer1))={}".format(CustomerDistance.objects.filter(Q(customer1=customer1,customer2=customer2) | Q(customer1=customer2,customer2=customer1)).count()))
        if CustomerDistance.objects.filter(Q(customer1=customer1,customer2=customer2) | Q(customer1=customer2,customer2=customer1)).count() == 0:
        #     pass
        # else:
        #     # print("yeah")
            new_customer_distance = CustomerDistance()
            if customer2.our_company or customer1.our_company:
                new_customer_distance.main_company=True
            if customer2.our_company:
                new_customer_distance.customer1 = customer2
                new_customer_distance.customer2 = customer1
            else:
                new_customer_distance.customer1 = customer1
                new_customer_distance.customer2 = customer2
            # distance_2_point_o = distance_2_point(new_customer_distance.customer1,new_customer_distance.customer2)

            # new_customer_distance.minute = distance_2_point_o[0]
            # new_customer_distance.distance = distance_2_point_o[1]
            #
            try:
                distance_2_point_o = nm_distance_2_point(
                    new_customer_distance.customer1.position.latitude,new_customer_distance.customer1.position.longitude,
                    new_customer_distance.customer2.position.latitude,new_customer_distance.customer2.position.longitude
                )
                # # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')
                # # print(distance_2_point_o[0])
                # # print(distance_2_point_o[1])
                # # print("customer1 = ({},{})".format(new_customer_distance.customer1.position.latitude,new_customer_distance.customer1.position.longitude))
                # # print("customer2 = ({},{})".format(new_customer_distance.customer2.latitude,new_customer_distance.customer2.longitude))
                # # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')

                new_customer_distance.minute = distance_2_point_o[0]
                new_customer_distance.distance = distance_2_point_o[1]

                # add game to the bulk list
                new_customer_distance.save()
                bulk_customer_distances.append(copy.deepcopy(new_customer_distance))
            except:
                DistanceErrorLog.objects.create(customer1=customer1,customer2=customer2)

    # now with a list of game objects that want to be created, run bulk_create on the chosen model
    # CustomerDistance.objects.bulk_create(bulk_customer_distances)

    # print('******************************************************')
    # print('{} distances created with success! '.format(len(bulk_customer_distances)))
    # print('******************************************************')
    # week_list = []


    from general.task_functions import calculate_minute_1times as nm_calculate_minute_1times
    # print('##############################################################################################################')
    if customer1.work_times == 1:
        week_list = [1,2,3,4]
        for week_list_item in week_list:
            plan_tim_works_weeks = PlanTeamWork.objects.filter(week=week_list_item)
            plan_tim_works_weeks_i = 0
            opt_tim_work_id = 0
            opt_tim_work_loc_count = 0
            opt_tim_work_min_minute = -1
            opt_tim_work_list = []
            for plan_tim_works_weeks_item in plan_tim_works_weeks:
                plan_tim_works_weeks_item_customers = plan_tim_works_weeks_item.get_customers()
                plan_tim_works_weeks_item_customers_list = []
                for plan_tim_works_weeks_item_customers_item in plan_tim_works_weeks_item_customers:
                    plan_tim_works_weeks_item_customers_list.append(plan_tim_works_weeks_item_customers_item.customer_id)
                if len(plan_tim_works_weeks_item_customers_list):
                    minute_r = nm_calculate_minute_1times(plan_tim_works_weeks_item_customers_list,loc_id,main_customer.id,plan_tim_works_weeks_item.minute)
                    if minute_r[0] >= 0:
                        plan_tim_works_weeks_i += 1
                        if plan_tim_works_weeks_i == 1:
                            opt_tim_work_id = plan_tim_works_weeks_item.id
                            opt_tim_work_min_minute = minute_r[0]
                            opt_tim_work_loc_count = minute_r[1]
                        else:
                            if opt_tim_work_min_minute < minute_r[0] and opt_tim_work_loc_count < minute_r[1]:
                                opt_tim_work_min_minute = minute_r[0]
                                opt_tim_work_id = plan_tim_works_weeks_item.id
                                opt_tim_work_loc_count = minute_r[1]
            if opt_tim_work_id != 0 and opt_tim_work_min_minute >= 0:
                plan_tim_works_weeks_f = plan_tim_works_weeks.filter(id=opt_tim_work_id).first()
                last_loc = plan_tim_works_weeks_f.get_last_customer()
                if last_loc:
                    last_loc_order = last_loc.order_index + 1
                else:
                    last_loc_order = 0

                CustomerOrder.objects.create(plan_team_work=plan_tim_works_weeks_f,customer=customer1,order_index=last_loc_order,main_process=False)
    elif customer1.work_times == 2:
        work_list = [[1,2],[3,4]]
        for week_list in work_list:
            week_list_plan_tim_works_weeks_i = 0
            week_list_opt_tim_work_id = 0
            week_list_opt_tim_work_min_minute = -1
            week_list_opt_tim_work_list = []
            week_list_week_item = 1
            for week_list_item in week_list:
                week_list_plan_tim_works_weeks_i += 1
                plan_tim_works_weeks = PlanTeamWork.objects.filter(week=week_list_item)
                plan_tim_works_weeks_i = 0
                opt_tim_work_id = 0
                opt_tim_work_loc_count = 0
                opt_tim_work_min_minute = -1
                opt_tim_work_list = []
                for plan_tim_works_weeks_item in plan_tim_works_weeks:
                    plan_tim_works_weeks_item_customers = plan_tim_works_weeks_item.get_customers()
                    plan_tim_works_weeks_item_customers_list = []
                    for plan_tim_works_weeks_item_customers_item in plan_tim_works_weeks_item_customers:
                        plan_tim_works_weeks_item_customers_list.append(
                            plan_tim_works_weeks_item_customers_item.customer_id)
                    if len(plan_tim_works_weeks_item_customers_list):
                        minute_r = nm_calculate_minute_1times(plan_tim_works_weeks_item_customers_list, loc_id,
                                                              main_customer.id, plan_tim_works_weeks_item.minute)
                        if minute_r[0] >= 0:
                            plan_tim_works_weeks_i += 1
                            if plan_tim_works_weeks_i == 1:
                                opt_tim_work_id = plan_tim_works_weeks_item.id
                                opt_tim_work_min_minute = minute_r[0]
                                opt_tim_work_loc_count = minute_r[1]
                            else:
                                if opt_tim_work_min_minute < minute_r[0] and opt_tim_work_loc_count < minute_r[1]:
                                    opt_tim_work_min_minute = minute_r[0]
                                    opt_tim_work_id = plan_tim_works_weeks_item.id
                                    opt_tim_work_loc_count = minute_r[1]

                if opt_tim_work_id != 0 and opt_tim_work_min_minute >= 0:
                    if week_list_plan_tim_works_weeks_i == 1:
                        week_list_opt_tim_work_id = opt_tim_work_id
                        week_list_opt_tim_work_min_minute = opt_tim_work_min_minute
                        week_list_week_item = week_list_item
                    else:
                        if week_list_opt_tim_work_min_minute > opt_tim_work_min_minute:
                            week_list_opt_tim_work_min_minute = opt_tim_work_min_minute
                            week_list_opt_tim_work_id = opt_tim_work_id
                            week_list_week_item = week_list_item
            if week_list_opt_tim_work_id != 0 and week_list_opt_tim_work_min_minute >= 0:
                plan_tim_works_weeks_f = PlanTeamWork.objects.filter(week=week_list_week_item).filter(
                    id=week_list_opt_tim_work_id).first()
                try:
                    last_loc = plan_tim_works_weeks_f.get_last_customer()
                    if last_loc:
                        last_loc_order = last_loc.order_index + 1
                    else:
                        last_loc_order = 0
                    CustomerOrder.objects.create(plan_team_work=plan_tim_works_weeks_f,
                                                 customer=customer1,
                                                 order_index=last_loc_order,
                                                 main_process=False)
                except:
                    pass
    elif customer1.work_times == 4:
        week_list = [1,2,3,4]
        week_list_plan_tim_works_weeks_i = 0
        week_list_opt_tim_work_id = 0
        week_list_opt_tim_work_min_minute = -1
        week_list_opt_tim_work_list = []
        week_list_week_item = 1
        for week_list_item in week_list:
            week_list_plan_tim_works_weeks_i += 1
            plan_tim_works_weeks = PlanTeamWork.objects.filter(week=week_list_item)
            plan_tim_works_weeks_i = 0
            opt_tim_work_id = 0
            opt_tim_work_loc_count = 0
            opt_tim_work_min_minute = -1
            opt_tim_work_list = []
            for plan_tim_works_weeks_item in plan_tim_works_weeks:
                plan_tim_works_weeks_item_customers = plan_tim_works_weeks_item.get_customers()
                plan_tim_works_weeks_item_customers_list = []
                for plan_tim_works_weeks_item_customers_item in plan_tim_works_weeks_item_customers:
                    plan_tim_works_weeks_item_customers_list.append(
                        plan_tim_works_weeks_item_customers_item.customer_id)
                if len(plan_tim_works_weeks_item_customers_list):
                    minute_r = nm_calculate_minute_1times(plan_tim_works_weeks_item_customers_list, loc_id,
                                                          main_customer.id, plan_tim_works_weeks_item.minute)
                    if minute_r[0] >= 0:
                        plan_tim_works_weeks_i += 1
                        if plan_tim_works_weeks_i == 1:
                            opt_tim_work_id = plan_tim_works_weeks_item.id
                            opt_tim_work_min_minute = minute_r[0]
                            opt_tim_work_loc_count = minute_r[1]
                        else:
                            if opt_tim_work_min_minute < minute_r[0] and opt_tim_work_loc_count < minute_r[1]:
                                opt_tim_work_min_minute = minute_r[0]
                                opt_tim_work_id = plan_tim_works_weeks_item.id
                                opt_tim_work_loc_count = minute_r[1]
            if opt_tim_work_id != 0 and opt_tim_work_min_minute >= 0:
                if week_list_plan_tim_works_weeks_i == 1:
                    week_list_opt_tim_work_id = opt_tim_work_id
                    week_list_opt_tim_work_min_minute = opt_tim_work_min_minute
                    week_list_week_item = week_list_item
                else:
                    if week_list_opt_tim_work_min_minute > opt_tim_work_min_minute:
                        week_list_opt_tim_work_min_minute = opt_tim_work_min_minute
                        week_list_opt_tim_work_id = opt_tim_work_id
                        week_list_week_item = week_list_item
        if week_list_opt_tim_work_id != 0:
            plan_tim_works_weeks_f = PlanTeamWork.objects.filter(week=week_list_week_item).filter(id=week_list_opt_tim_work_id).first()
            try:
                last_loc = plan_tim_works_weeks_f.get_last_customer()
                if last_loc:
                    last_loc_order = last_loc.order_index + 1
                else:
                    last_loc_order = 0
                CustomerOrder.objects.create(plan_team_work=plan_tim_works_weeks_f,customer=customer1,order_index=last_loc_order,main_process=False)
            except:
                pass

    # print('##############################################################################################################')
    return True

