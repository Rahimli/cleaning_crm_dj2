import copy

from celery import shared_task


@shared_task
def create_customer_distance_customers(loc_id):
    from django.db.models import Q
    from work.models import Customer
    from work.models import CustomerDistance , PlanTeamWork , CustomerOrder, DistanceErrorLog
    from .task_functions import distance_2_point
    customers = Customer.objects.filter().exclude(id=loc_id)
    # the list that will hold the bulk insert
    bulk_customer_distances = []
    customer1 = Customer.objects.get(status=True,id=loc_id)
    for customer2 in customers:
        if CustomerDistance.objects.filter(Q(customer1=customer1,customer2=customer2) | Q(customer1=customer2,customer2=customer1)).count() == 0:
            new_customer_distance = CustomerDistance()
            if customer2.our_company or customer1.our_company:
                new_customer_distance.main_company=True
            if customer2.our_company:
                new_customer_distance.customer1 = customer2
                new_customer_distance.customer2 = customer1
            else:
                new_customer_distance.customer1 = customer1
                new_customer_distance.customer2 = customer2

            try:
                distance_2_point_o = distance_2_point(
                    new_customer_distance.customer1.position.latitude,new_customer_distance.customer1.position.longitude,
                    new_customer_distance.customer2.position.latitude,new_customer_distance.customer2.position.longitude
                )
                new_customer_distance.minute = distance_2_point_o[0]
                new_customer_distance.distance = distance_2_point_o[1]

                # add game to the bulk list
                new_customer_distance.save()
                bulk_customer_distances.append(copy.deepcopy(new_customer_distance))
            except:
                DistanceErrorLog.objects.create(customer1=customer1,customer2=customer2)

    print('******************************************************')
    print('{} distances created with success! '.format(len(bulk_customer_distances)))
    print('******************************************************')


    return True





# from content.models import DayCHOICES
@shared_task
def main_result_prepare_plan_new_plan():
    from work.models import TaskCustomDay
    from general.task_functions import result1p1 as nm_result1p1
    from general.models import PlanLog
    from work.models import Customer, PlanTeamWork, Team, WorkDay, CustomerOrder
    from general.task_functions import exc_list_loc
    from work.common import Hours_CHOICES
    # from .new_main import ordered_customers as nm_ordered_customers
    customers = Customer.objects.all().order_by('id')
    week_2_team_work_list = []
    week_4_team_work_list = []
    week_no_standart_team_work_list = []
    for customers_item in customers:
        if customers_item.work_times == 1 and customers_item.standart_task is False:
            week_no_standart_team_work_list.append(customers_item.id)
        if customers_item.work_times == 2:
            week_2_team_work_list.append(customers_item.id)
        if customers_item.work_times == 4:
            week_4_team_work_list.append(customers_item.id)
    week_list = [1,2,3,4]
    day_list = [1,2,3,4,5,6,7]
    # print("week_4_team_work_list={}".format(week_4_team_work_list))
    # print("week_2_team_work_list={}".format(week_2_team_work_list))
    # 2week_team_work_list = []
    bulk_plan_team_work_list = []
    bulk_customer_order = []
    # exc_list = []
    teams = Team.objects.filter()
    # customers = Customer.objects.filter()
    work_days = WorkDay.objects.filter()
    week_list_i = 0
    # main_loc = Customer.objects.get(our_company=True)
    plan_obj_create = PlanLog.objects.create(complated=False, rejcected=False)
    PlanTeamWork.objects.filter().delete()
    # try:
    for week_list_item in week_list:
        week_list_i += 1
        # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')

        # # print("week_2_team_work_list={}".format(week_2_team_work_list))
        # # print("week_4_team_work_list={}".format(week_4_team_work_list))
        weekly_exc_list = week_no_standart_team_work_list + exc_list_loc(copy.deepcopy(week_2_team_work_list),2,week_list_item) + exc_list_loc(copy.deepcopy(week_4_team_work_list),4,week_list_item)
        exc_list = weekly_exc_list
        # # print("weekly_exc_list={}".format(weekly_exc_list))
        # # print("week_2_team_work_list={}".format(week_2_team_work_list))
        # # print("week_4_team_work_list={}".format(week_4_team_work_list))
        # # print("exc_list_loc(week_2_team_work_list,2,week_list_item)={}".format(exc_list_loc(copy.deepcopy(week_2_team_work_list),2,week_list_item)))
        # # print("exc_list_loc(week_2_team_work_list,4,week_list_item)={}".format(exc_list_loc(copy.deepcopy(week_4_team_work_list),4,week_list_item)))
        # print('""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""')
        # if week_list_item in [1,2,4]:
        for day_list_item in day_list:
            for team_item in teams:
                if day_list_item in [x.day.day for x in team_item.get_team_work_days()]:
                    new_plan_team_work = PlanTeamWork()
                    new_plan_team_work.team = team_item
                    new_plan_team_work.week = week_list_item
                    new_plan_team_work.is_empty = True
                    new_plan_team_work.minute = team_item.get_team_work_day(day_list_item).minute
                    new_plan_team_work.difference_minute = new_plan_team_work.minute
                    new_plan_team_work.day = work_days.filter(day=day_list_item).first()
                    new_plan_team_work.save()

            day_task_custom_days = TaskCustomDay.objects.filter(task_day=day_list_item).values('task__customer_id')#.exclude(task__customer__minute=0.0)
            daily_exc_list = [y.id for y in Customer.objects.exclude(
                id__in=[x['task__customer_id'] for x in day_task_custom_days])]

            for Hours_CHOICES_item in Hours_CHOICES:
                if Hours_CHOICES_item[0]:
                    teamworks = PlanTeamWork.objects.filter(week=week_list_item, is_empty=True).filter(
                        minute=Hours_CHOICES_item[0]).order_by('week', 'day__day')
                    # # print("teamworks={}".format(teamworks.count()))
                    while teamworks.count() > 0:
                        opt_loc_destinations = nm_result1p1(day_list_item,exc_list=exc_list+daily_exc_list, minute=Hours_CHOICES_item[0])
                        if len(opt_loc_destinations) == 0:
                            break
                        print("*************************************************************************************")
                        print("opt_loc_destinations={}".format(opt_loc_destinations))
                        print("*************************************************************************************")
                        # print("opt_loc_destinations={}".format(opt_loc_destinations))
                        # # print("len(opt_loc_destinations)={}".format(len(opt_loc_destinations)))
                        teamworks_item_i = 0
                        for teamworks_item in teamworks[:len(opt_loc_destinations)]:
                            opt_loc_destinations_i = 0
                            ordered_opt_loc_destination_item = opt_loc_destinations[teamworks_item_i]
                            # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])
                            for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
                                new_customer_order = CustomerOrder()
                                exc_list.append(opt_loc_destinations_item)
                                new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
                                new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i + 1)
                                new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
                                new_customer_order.main_process = True
                                new_customer_order.save()
                                bulk_customer_order.append(new_customer_order)
                                # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
                                opt_loc_destinations_i += 1
                            # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
                            # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)

                            if Hours_CHOICES_item[0] - 20 <= ordered_opt_loc_destination_item[-1][0]:
                                teamworks_item.is_full = True
                            teamworks_item.difference_minute = Hours_CHOICES_item[0] - \
                                                               ordered_opt_loc_destination_item[-1][0]
                            teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
                            teamworks_item.is_empty = False
                            teamworks_item.save()
                            teamworks_item_i += 1

        for Hours_CHOICES_item in Hours_CHOICES:
            if Hours_CHOICES_item[0]:
                # # print("DayCHOICES_item[0]={}".format(Hours_CHOICES_item[0]))
                teamworks = PlanTeamWork.objects.filter(week=week_list_item,is_empty=True).filter(minute=Hours_CHOICES_item[0]).order_by('week','day__day')
                # # print("teamworks={}".format(teamworks.count()))
                while teamworks.count() > 0:
                    opt_loc_destinations = nm_result1p1(0,exc_list=exc_list,minute=Hours_CHOICES_item[0])
                    if len(opt_loc_destinations) == 0:
                        break
                    print("*************************************************************************************")
                    print("opt_loc_destinations={}".format(opt_loc_destinations))
                    print("*************************************************************************************")
                    # print("opt_loc_destinations={}".format(opt_loc_destinations))
                    # # print("len(opt_loc_destinations)={}".format(len(opt_loc_destinations)))
                    teamworks_item_i = 0
                    for teamworks_item in teamworks[:len(opt_loc_destinations)]:
                        opt_loc_destinations_i = 0
                        ordered_opt_loc_destination_item = opt_loc_destinations[teamworks_item_i]
                        # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])
                        for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
                            new_customer_order = CustomerOrder()
                            exc_list.append(opt_loc_destinations_item)
                            new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
                            new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i+1)
                            new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
                            new_customer_order.main_process = True
                            new_customer_order.save()
                            bulk_customer_order.append(new_customer_order)
                            # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
                            opt_loc_destinations_i+=1
                        # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
                        # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)

                        if Hours_CHOICES_item[0] - 20 <= ordered_opt_loc_destination_item[-1][0]:
                            teamworks_item.is_full = True
                        teamworks_item.difference_minute = Hours_CHOICES_item[0] - ordered_opt_loc_destination_item[-1][0]
                        teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
                        teamworks_item.is_empty = False
                        teamworks_item.save()
                        teamworks_item_i += 1

    complated = True
    rejcected = False
    # except:
    #     complated = False
    #     rejcected = True
    # print("------------------------------------- Loading..... --------------------------------------------------------")
    # exc_list = []
    # # # print(result([], 300))
    # loc_destinations = CustomerDistance.objects.exclude(Q(customer1_id__in=exc_list) | Q(customer2_id__in=exc_list)).order_by('minute')
    # dest_list = []
    # for loc_destination_item in loc_destinations:
    #     dest_list.append(obj_to_dict_dest(loc_destination_item))
    # # print("calculate_minute([1, 5, 11, 20, 21, 27, 29], 300,dest_list)= {} ".format(calculate_minute([1, 5, 11, 20, 21, 27, 29], 300,dest_list)))
    # # print(exc_list)
    # # print(bulk_customer_order)
    # CustomerOrder.objects.bulk_create(bulk_customer_order)

    plan_obj = PlanLog.objects.filter(complated=False, rejcected=False).first()
    if plan_obj:
        plan_obj.rejcected = rejcected
        plan_obj.complated = complated
        plan_obj.save()
    else:
        PlanLog.objects.create(complated=complated, rejcected=rejcected)
    # print("------------------------------------- Loaded --------------------------------------------------------")
    return '{} bulk customers order created with success!'.format(len(bulk_customer_order))


@shared_task
def sub_main_result_prepare_plan_new_plan():
    from work.models import CustomerOrder, Customer, TaskCustomDay, PlanTeamWork
    from general.task_functions import result1p1 as nm_result1p1
    from general.task_functions import sub_result1p1
    from work.common import Hours_CHOICES
    from general.task_functions import exc_list_loc
    # from .new_main import ordered_customers as nm_ordered_customers
    sub_proses = CustomerOrder.objects.filter(main_process=False)
    sub_proses_customer_list = []
    for sub_proses_item in sub_proses:
        sub_proses_customer_list.append(copy.deepcopy(sub_proses_item.customer_id))
    sub_proses.delete()
    customers = Customer.objects.filter(id__in=sub_proses_customer_list).order_by('id')
    week_2_team_work_list = []
    week_4_team_work_list = []
    week_no_standart_team_work_list = []
    for customers_item in customers:
        if customers_item.work_times == 1 and customers_item.standart_task is False:
            week_no_standart_team_work_list.append(customers_item.id)
        if customers_item.work_times == 2:
            week_2_team_work_list.append(customers_item.id)
        if customers_item.work_times == 4:
            week_4_team_work_list.append(customers_item.id)

    week_list = [1,2,3,4]
    day_list = [1,2,3,4,5,6,7]
    bulk_plan_team_work_list = []
    bulk_customer_order = []

    week_list_i = 0
    for week_list_item in week_list:
        weekly_exc_list = week_no_standart_team_work_list + exc_list_loc(copy.deepcopy(week_2_team_work_list),2,week_list_item) + exc_list_loc(copy.deepcopy(week_4_team_work_list),4,week_list_item)
        # exc_list = weekly_exc_list

        week_list_i += 1
        for day_list_item in day_list:
            hour_task_custom_days = TaskCustomDay.objects.filter(task_day=day_list_item).filter(task__customer_id__in=sub_proses_customer_list).exclude(task__customer__minute=0).filter(task__customer__standart_task=False).values('task__customer_id')
            daily_exc_list = [y.id for y in Customer.objects.exclude(id__in=[x['task__customer_id'] for x in hour_task_custom_days]) ]
            teamworks = PlanTeamWork.objects.filter(week=week_list_item, day__day=day_list_item).filter(is_full=False).order_by('is_empty', '-difference_minute')

            teamworks_item_i = 0
            for teamworks_item in teamworks:
                teamworks_item_i += 1
                if teamworks_item:
                    teamworks_item_get_customers = teamworks_item.get_customers()
                    exc_list = daily_exc_list + weekly_exc_list
                    try:
                        opt_loc_destinations_customer_id = teamworks_item_get_customers.last().customer.id
                    except:
                        opt_loc_destinations_customer_id = 0
                    opt_loc_destinations = sub_result1p1(day_list_item,opt_loc_destinations_customer_id,exc_list=exc_list, minute=teamworks_item.difference_minute)
                    # teamworks = PlanTeamWork.objects.filter(week=1,day__day=day_list_item,is_empty=True).filter(minute=Hours_CHOICES_item[0]).order_by('week','day__day')

                    try:
                        opt_loc_destinations_i = teamworks_item_get_customers.last().order_index
                    except:
                        opt_loc_destinations_i = 0
                    ordered_opt_loc_destination_item = opt_loc_destinations[0]
                    # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])
                    for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
                        new_customer_order = CustomerOrder()
                        daily_exc_list.append(opt_loc_destinations_item)
                        new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
                        new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i + 1)
                        new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
                        new_customer_order.main_process = False
                        new_customer_order.save()
                        # bulk_customer_order.append(new_customer_order)
                        # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
                        opt_loc_destinations_i += 1
                    # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
                    # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)
                    if teamworks_item.difference_minute - 20 <= ordered_opt_loc_destination_item[-1][0]:
                        teamworks_item.is_full = True
                    teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
                    teamworks_item.is_empty = False
                    teamworks_item.save()
                    teamworks_item_i += 1

        hour_task_custom_days = TaskCustomDay.objects.filter(
            task__customer_id__in=sub_proses_customer_list).exclude(task__customer__minute=0).filter(
            task__customer__standart_task=True).values('task__customer_id')
        daily_exc_list = [y.id for y in Customer.objects.exclude(
            id__in=[x['task__customer_id'] for x in hour_task_custom_days])]
        teamworks = PlanTeamWork.objects.filter(week=week_list_item).filter(
            is_full=False).order_by('is_empty', '-difference_minute')

        teamworks_item_i = 0
        for teamworks_item in teamworks:
            teamworks_item_i += 1
            if teamworks_item:
                teamworks_item_get_customers = teamworks_item.get_customers()
                exc_list = daily_exc_list + weekly_exc_list
                try:
                    opt_loc_destinations_customer_id = teamworks_item_get_customers.last().customer.id
                except:
                    opt_loc_destinations_customer_id = 0
                opt_loc_destinations = nm_sub_result1p1(0,opt_loc_destinations_customer_id,exc_list=exc_list,
                                                        minute=teamworks_item.difference_minute)
                try:
                    opt_loc_destinations_i = teamworks_item_get_customers.last().order_index
                except:
                    opt_loc_destinations_i = 0
                ordered_opt_loc_destination_item = opt_loc_destinations[0]
                # teamworks = PlanTeamWork.objects.filter(week=1,day__day=day_list_item,is_empty=True).filter(minute=Hours_CHOICES_item[0]).order_by('week','day__day')
                ordered_opt_loc_destination_item = opt_loc_destinations[0]
                # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])
                for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
                    new_customer_order = CustomerOrder()
                    daily_exc_list.append(opt_loc_destinations_item)
                    new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
                    new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i + 1)
                    new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
                    new_customer_order.main_process = False
                    new_customer_order.save()
                    # bulk_customer_order.append(new_customer_order)
                    # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
                    opt_loc_destinations_i += 1
                # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
                # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)
                if teamworks_item.difference_minute - 20 <= ordered_opt_loc_destination_item[-1][0]:
                    teamworks_item.is_full = True
                teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
                teamworks_item.is_empty = False
                teamworks_item.save()
                teamworks_item_i += 1

        # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')

        # # print("week_2_team_work_list={}".format(week_2_team_work_list))
        # # print("week_4_team_work_list={}".format(week_4_team_work_list))
        weekly_exc_list = week_no_standart_team_work_list + exc_list_loc(copy.deepcopy(week_2_team_work_list),2,week_list_item) + exc_list_loc(copy.deepcopy(week_4_team_work_list),4,week_list_item)
        exc_list = weekly_exc_list




    return True


@shared_task
def new_add_customer_result_prepare_plan_new_plan(new_customer_id):
    from work.models import CustomerOrder, Customer, TaskCustomDay, PlanTeamWork
    from general.task_functions import result1p1 as nm_result1p1
    from general.task_functions import sub_result1p1 as nm_sub_result1p1
    from work.common import Hours_CHOICES
    from general.task_functions import exc_list_loc
    # from .new_main import ordered_customers as nm_ordered_customers

    customers = Customer.objects.filter(id=new_customer_id).order_by('id')
    week_2_team_work_list = []
    week_4_team_work_list = []
    week_no_standart_team_work_list = []
    for customers_item in customers:
        if customers_item.work_times == 1 and customers_item.standart_task is False:
            week_no_standart_team_work_list.append(customers_item.id)
        if customers_item.work_times == 2:
            week_2_team_work_list.append(customers_item.id)
        if customers_item.work_times == 4:
            week_4_team_work_list.append(customers_item.id)

    week_list = [1,2,3,4]
    day_list = [1,2,3,4,5,6,7]
    bulk_plan_team_work_list = []
    bulk_customer_order = []

    week_list_i = 0
    for week_list_item in week_list:
        weekly_exc_list = week_no_standart_team_work_list + exc_list_loc(copy.deepcopy(week_2_team_work_list),2,week_list_item) + exc_list_loc(copy.deepcopy(week_4_team_work_list),4,week_list_item)
        # exc_list = weekly_exc_list

        week_list_i += 1
        for day_list_item in day_list:
            hour_task_custom_days = TaskCustomDay.objects.filter(task_day=day_list_item).filter(task__customer_id=new_customer_id).exclude(task__customer__minute=0).filter(task__customer__standart_task=False).values('task__customer_id')
            daily_exc_list = [y.id for y in Customer.objects.exclude(id__in=[x['task__customer_id'] for x in hour_task_custom_days]) ]
            teamworks = PlanTeamWork.objects.filter(week=week_list_item, day__day=day_list_item).filter(is_full=False).order_by('is_empty', 'difference_minute')

            teamworks_item_i = 0
            for teamworks_item in teamworks:
                teamworks_item_i += 1
                if teamworks_item:
                    teamworks_item_get_customers = teamworks_item.get_customers()
                    exc_list = daily_exc_list + weekly_exc_list
                    try:
                        opt_loc_destinations_customer_id = teamworks_item_get_customers.last().customer.id
                    except:
                        opt_loc_destinations_customer_id = 0

                    try:
                        opt_loc_destinations_i = teamworks_item_get_customers.last().order_index
                    except:
                        opt_loc_destinations_i = 0



                    opt_loc_destinations = nm_sub_result1p1(day_list_item,opt_loc_destinations_customer_id,exc_list=exc_list, minute=teamworks_item.difference_minute)
                    # teamworks = PlanTeamWork.objects.filter(week=1,day__day=day_list_item,is_empty=True).filter(minute=Hours_CHOICES_item[0]).order_by('week','day__day')

                    ordered_opt_loc_destination_item = opt_loc_destinations[0]
                    # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])



                    for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
                        new_customer_order = CustomerOrder()
                        daily_exc_list.append(opt_loc_destinations_item)
                        new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
                        new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i + 1)
                        new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
                        new_customer_order.main_process = False
                        new_customer_order.save()
                        # bulk_customer_order.append(new_customer_order)
                        # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
                        opt_loc_destinations_i += 1
                    # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
                    # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)
                    if teamworks_item.difference_minute - 20 <= ordered_opt_loc_destination_item[-1][0]:
                        teamworks_item.is_full = True
                    teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
                    teamworks_item.is_empty = False
                    teamworks_item.save()
                    # teamworks_item_i += 1

        hour_task_custom_days = TaskCustomDay.objects.filter(
            task__customer_id=new_customer_id).exclude(task__customer__minute=0).filter(
            task__customer__standart_task=True).values('task__customer_id')
        daily_exc_list = [y.id for y in Customer.objects.exclude(
            id__in=[x['task__customer_id'] for x in hour_task_custom_days])]
        teamworks = PlanTeamWork.objects.filter(week=week_list_item).filter(
            is_full=False).order_by('is_empty', '-difference_minute')

        teamworks_item_i = 0
        for teamworks_item in teamworks:
            teamworks_item_i += 1
            if teamworks_item:
                teamworks_item_get_customers = teamworks_item.get_customers()
                exc_list = daily_exc_list + weekly_exc_list
                try:
                    opt_loc_destinations_customer_id = teamworks_item_get_customers.last().customer.id
                except:
                    opt_loc_destinations_customer_id = 0
                opt_loc_destinations = nm_sub_result1p1(0,opt_loc_destinations_customer_id,exc_list=exc_list,
                                                        minute=teamworks_item.difference_minute)
                try:
                    opt_loc_destinations_i = teamworks_item_get_customers.last().order_index
                except:
                    opt_loc_destinations_i = 0
                ordered_opt_loc_destination_item = opt_loc_destinations[0]
                # teamworks = PlanTeamWork.objects.filter(week=1,day__day=day_list_item,is_empty=True).filter(minute=Hours_CHOICES_item[0]).order_by('week','day__day')
                ordered_opt_loc_destination_item = opt_loc_destinations[0]
                # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])
                for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
                    new_customer_order = CustomerOrder()
                    daily_exc_list.append(opt_loc_destinations_item)
                    new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
                    new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i + 1)
                    new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
                    new_customer_order.main_process = False
                    new_customer_order.save()
                    # bulk_customer_order.append(new_customer_order)
                    # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
                    opt_loc_destinations_i += 1
                # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
                # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)
                if teamworks_item.difference_minute - 20 <= ordered_opt_loc_destination_item[-1][0]:
                    teamworks_item.is_full = True
                teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
                teamworks_item.is_empty = False
                teamworks_item.save()
                teamworks_item_i += 1

        # print('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^')

        # # print("week_2_team_work_list={}".format(week_2_team_work_list))
        # # print("week_4_team_work_list={}".format(week_4_team_work_list))
        weekly_exc_list = week_no_standart_team_work_list + exc_list_loc(copy.deepcopy(week_2_team_work_list),2,week_list_item) + exc_list_loc(copy.deepcopy(week_4_team_work_list),4,week_list_item)
        exc_list = weekly_exc_list

    return True

