import os

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.datetime_safe import datetime

from base_user.models import UserPermission
from general.functions import check_permission_for_admins, stripTags
from home.views import base_auth
from work.forms import *
from work.functions import get_28_days_dates, get_week, get_day_name, get_day_from_days
from work.models import *
from django.utils.translation import ugettext_lazy as _
from geoposition import Geoposition

GUser = get_user_model()



@login_required(login_url='base-user:login')
def customer_upload_from_excel(request):
    context = base_auth(req=request)
    user = request.user
    from work.tasks import add_location_from_excel_list

    if check_permission_for_admins(request,'excel-upload-customer') is False:
        raise Http404
    if request.method == 'POST':
        form = ExcelDocumentForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            clean_data = form.cleaned_data
            excelfile = clean_data.get('excelfile')
            extension = os.path.splitext(excelfile.name)[1]
            if not (extension in IMPORT_FILE_TYPES):
                context['message_code'] = 2
                context['message'] = _("Please choose Excel File")
                return render(request, 'work/admin/customers/add-customer-excel.html', context=context)
                # raise forms.ValidationError(
                #     _("This file is not a valid Excel file. Please make sure your input file is an Excel file )"))
            # pass
            excel_document_new = ExcelDocument(excelfile=excelfile)
            excel_document_new.save()
            # return HttpResponse(excelfile)
            context['message_code'] = 1
            # print("excel_document_new.excelfile.url={}".format(excel_document_new.excelfile.url))
            # print("excel_document_new.excelfile={}".format(excel_document_new.excelfile))
            add_location_from_excel_list.delay(excel_document_new.excelfile.url)
            context['message'] = _("Uploaded successfuly")

        else:
            context['message_code'] = 2
            context['message'] = _("Please choose Excel File")
            # return HttpResponse(form.errors)
            # newdoc = ExcelDocumentForm(excelfile = request.FILES['excelfile'])
            # newdoc.save()

            # return HttpResponseRedirect(reverse('credit.views.list'))
    else:
        form = ExcelDocumentForm()
    context['form'] = form
    return render(request, 'work/admin/customers/add-customer-excel.html', context=context)




def calendar_remove(request,td_id):
    user = request.user
    message_code = 0
    if user.usertype == 3:
        try:
            td_obj = TaskDate.objects.filter(task__customer__user=user,id=td_id).filter(date__gt=datetime.today().date())
            if td_obj:
                message_code = 1
                td_obj.delete()
        except:
            pass
    data = {'message_code': message_code}
    return JsonResponse(data)





def calendar_customer_edit(request,td_id):
    user = request.user
    message_code = 0
    _html = ''
    tasks = None
    if user.usertype == 3:
        calendar_edit_form = AdminCalendarForm(request.POST or None)
    else:
        calendar_edit_form = AdminCalendarForm(request.POST or None)
    _html = "{}{}".format(_html, render_to_string('work/include/calendar/calendar-full.html',
                                                      {
                                                          'task_dates':tasks,
                                                          'calendar_edit_form':calendar_edit_form,
                                                       }
                                                  ))
    data = {'message_code': message_code, 'result': _html}
    return JsonResponse(data)



@login_required(login_url='userprofile:sign_in')
def admin_team_plans(request,t_id):
    context = base_auth(req=request)
    user = request.user
    team_obj = get_object_or_404(Team, user=user)

    if user.type == 'admin-person':
        context['team_obj'] = team_obj
        context['week_list'] = [1,2,3]
    else:
        raise Http404
    return render(request, 'work/general/teams/team-plans.html', context=context)





@login_required(login_url='userprofile:sign_in')
def team_plans(request):
    context = base_auth(req=request)
    user = request.user

    if check_permission_for_admins(request,'work-plans') or user.usertype == 2:
        search_form = PlansSearchForm(request.POST or None)
        form_show = True
        if user.usertype == 2:
            employee_obj = get_object_or_404(Employee,user=user)
            team_obj = Team.objects.filter(Q(employees__user=user)).first()
            form_show = False
            if team_obj:
                search_form = PlansSearchForm(request.POST or None,initial={
                    'team':team_obj.id
                })
                context['team_obj'] = team_obj
            context['employee_obj'] = employee_obj

    else:
        raise Http404
    # team_obj = get_object_or_404(Team, user=user)

    context['week_list'] = [1,2,3]
    context['search_form'] = search_form
    context['form_show'] = form_show
    return render(request, 'work/general/teams/plans-search-list.html', context=context)




@login_required(login_url='userprofile:sign_in')
def team_plan_show_ajax(request):
    context = base_auth(req=request)
    profile = request.user
    today = datetime.today()


    print('geldik')
    if check_permission_for_admins(request,'work-plans') or profile.usertype == 2:
        print('permission geldik')
        search_form = PlansSearchForm(request.POST or None)
        if request.method == 'POST' and request.is_ajax() and search_form.is_valid():
            print('post valid geldik')
            cleaned_data = search_form.cleaned_data
            team_field = cleaned_data.get('team',0)
            date_str = date = '{}-{}-{}'.format(today.year,today.month,today.day)
            get_28_days_dates_l = get_28_days_dates(date_str=date_str)
            message_code = 0
            _html = ""
            _team_name = ""
            week_list = [1, 2, 3, 4]
            team_work_day_i = 0
            if team_field:
                team = Team.objects.filter(id=team_field).first()
                _team_name = team.name
                get_28_days_dates_l_i = 0
                taskDate = TaskDate.objects.filter(date__in=get_28_days_dates_l).filter(plan_teamWork_date__plan_team_work__team=team)
                planTeamWorkDate = PlanTeamWorkDate.objects.filter(date__in=get_28_days_dates_l).filter(plan_team_work__team=team)
                for get_28_days_dates_l_item in get_28_days_dates_l:
                    get_28_days_dates_l_i += 1
                    team_work_day_i += 1
                    week_item = get_week(team_work_day_i)
                    _button = ""
                    # week_list_i = 1
                    team_get_plans_w_d_customer_list = []
                    # print("team_get_plans_w_d_customer_list={}".format(team_get_plans_w_d_customer_list))
                    _html = '{0}{1}'.format(_html,
                                              render_to_string('work/include/teams/plan/_week_part.html',
                                                                        {
                                                                            'week_item': week_item,
                                                                            'get_day_name': get_day_name(get_day_from_days(get_28_days_dates_l_i)),
                                                                        }))
                    week_taskDates = taskDate.filter(date=get_28_days_dates_l_item)
                    week_planTeamWorkDate = planTeamWorkDate.filter(date=get_28_days_dates_l_item).first()
                    # team_get_plans_w_d = team_work_day.get_plans_w_d(week_item,team_work_day.day.id)
                    if week_planTeamWorkDate:
                        team_get_plans_w_d = week_planTeamWorkDate.plan_team_work
                        loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by('order_index')
                        team_get_plans_w_d_customer_i = 0
                        for team_get_plans_w_d_customer in loc_oreders:
                            this_day_task_date = week_taskDates.filter(plan_teamWork_date=week_planTeamWorkDate).filter().first()
                            if team_get_plans_w_d_customer.customer_id in [x.task.customer_id for x in week_taskDates]:
                                team_get_plans_w_d_customer_list.append(team_get_plans_w_d_customer.customer_id)
                                team_get_plans_w_d_customer_i +=1

                                _html = '{0}{1}'.format(_html,
                                                          render_to_string('work/include/teams/plan/_team_plans.html',
                                                                                    {
                                                                                        # 'team_work_day_i': team_work_day_i,
                                                                                        'profile_type': profile.usertype,
                                                                                        # 'week_list_i': week_list_i,
                                                                                        'team_get_plans_w_d_customer_i': team_get_plans_w_d_customer_i,
                                                                                        'team_get_plans_w_d': team_get_plans_w_d,
                                                                                        'team_get_plans_w_d_get_customer': team_get_plans_w_d_customer,
                                                                                        'team_get_plans_w_d_get_customer_minute': team_get_plans_w_d_customer.customer.get_customer_day_minute(week_day=team_work_day),
                                                                                    }))
                        if team_get_plans_w_d_customer_i > 0:
                            _button = '{0}'.format(render_to_string('work/include/teams/plan/_team_plans_button.html',
                                                                            {
                                                                                'team_get_plans_w_d_customer_list': team_get_plans_w_d_customer_list,
                                                                                # 'team_get_plans_w_d_customer_i': team_get_plans_w_d_customer_i,
                                                                            }))
                    # _html = '{0}{1}'.format( _html, '</ul></div>')
                    team_get_plans_w_d_customer_list = []
                    _html = '{0}{1}{2}'.format( _html,_button, '</ul></div></div>')
                    message_code = 1
                    # _html = '{0}{1}'.format(_html, '</div>')
            data = {'message_code': message_code, '_html': _html, '_team_name': _team_name}
            # data = {'message_code': message_code, '_html': _html}
            return JsonResponse(data)
        else:
            raise Http404
    else:
        raise Http404





@login_required(login_url='userprofile:sign_in')
def team_plan_show_map_ajax(request):
    # context = base_auth(req=request)
    profile= request.user
    # print("salam")
    if check_permission_for_admins(request,'work-plans') or profile.usertype == 2:
        # print("admin")
        if request.method == 'POST' and request.is_ajax():
            print(request.POST)
            # print(request.POST["customer_list"])
            # print(request.POST.get("csrfmiddlewaretoken"))
            # print(request.POST.getlist("customer_list[]"))
            message_code = 0
            _html = ""
            message_code = 1
            main_customer = Customer.objects.filter(our_company=True).first()
            customer_list = request.POST.getlist('customer_list[]')
            map_data = []
            customers = Customer.objects.filter(id__in=customer_list)
            customer_list_i = 0
            print("customer_list = {}".format(customer_list))
            for customer_list_item in customer_list:
                customer_list_i +=1
                customers_item = customers.filter(id=customer_list_item).first()
                if customers_item:
                    map_data.append([customers_item.address])
            # # print("customers_item.address={}".format(customers_item.address))
            map_data.insert(0,[main_customer.address])
            print("customers={}".format(customers.count()))
            print("map_data={}".format(map_data))

            _html = '{0}'.format(render_to_string('work/include/teams/plan/_direction_map.html',
                                                    {
                                                        'map_data': map_data,
                                                        # 'employee_get_plans_w_d_customer_i': employee_get_plans_w_d_customer_i,
                                                    }))
            data = {'message_code': message_code, '_html': _html}
            # data = {'message_code': message_code, '_html': _html}
            return JsonResponse(data)




@login_required(login_url='base-user:login')
def admin_team_plan_form_generate_ajax(request):
    # context = base_auth(req=request)
    profile= request.user
    # print("salam")
    if check_permission_for_admins(request,'work-plans') or profile.usertype == 2:
        # print("admin")
        if request.method == 'GET' and request.is_ajax():
            # print("get ajax")
            message_code = 0
            _html = ""
            _html_teams_select = "<option value="">{}</option>".format(_("Select Team"))
            _html_teams_work_day_select = "<option value="">{}</option>".format(_("Select Work Day"))
            message_code = 1
            team_plan_id = request.GET.get('team_plan_id')
            l_id = request.GET.get('l_id')
            map_data = []
            team_plan = PlanTeamWork.objects.filter(id=team_plan_id).first()
            if team_plan:
                customer = team_plan.get_customer(l_id)
                teams = Team.objects.filter()
                work_days = WorkDay.objects.all().order_by('day')
                for team_item in teams:
                    _html_teams_select = '{0}{1}'.format(_html_teams_select,'<option value="{}">{}</option>'.format(team_item.id,team_item.name))
                for work_days_item in work_days:
                    _html_teams_work_day_select = '{0}{1}'.format(_html_teams_work_day_select,'<option value="{}">{}</option>'.format(work_days_item.id,work_days_item.get_day_name()))
                # print("customers={}".format(customers.count()))
                # print("map_data={}".format(map_data))

            data = {'message_code': message_code, '_html_teams_select': _html_teams_select, '_html_teams_work_day_select': _html_teams_work_day_select}
            # data = {'message_code': message_code, '_html': _html}
            return JsonResponse(data)
            # return Jso(json.dumps(data, ensure_ascii=False), content_type="application/json")


@login_required(login_url='base-user:login')
def admin_team_plan_form_change_customer_position_ajax(request,cur_cus_ord_id):
    # context = base_auth(req=request)
    profile= request.user
    if check_permission_for_admins(request,'work-plans') or profile.usertype == 2:
        # print("admin")
        if request.method == 'GET' and request.is_ajax():
            # print("get ajax")
            message_code = 0
            _html = ""
            # _html_customers_select = '<option value="">{}</option>'.format(_("Select Customer"))
            try:
                message_code = 1
                teams_select_id = request.GET.get('teams_select_id')
                teams_work_day_id = request.GET.get('teams_work_day_id')
                chng_cos_ord_id = request.GET.get('chng_loc_ord_id')
                map_data = []
                chng_cos_ord_obj = CustomerOrder.objects.filter(id=chng_cos_ord_id).first()
                cur_cos_ord_obj = CustomerOrder.objects.filter(id=cur_cus_ord_id).first()
                # print('cur_cos_ord_obj.customer_id = {} ,chng_cos_ord_obj.customer_id  = {}'.format(cur_cos_ord_obj.customer_id ,chng_cos_ord_obj.customer_id ))
                cur_cos_ord_obj_cos_id =  cur_cos_ord_obj.customer_id
                chng_cos_ord_obj_id =  chng_cos_ord_obj.customer_id
                cur_cos_ord_obj.customer_id ,chng_cos_ord_obj.customer_id = chng_cos_ord_obj_id , cur_cos_ord_obj_cos_id
                # print('cur_cos_ord_obj.customer_id = {} ,chng_cos_ord_obj.customer_id  = {}'.format(cur_cos_ord_obj.customer_id ,chng_cos_ord_obj.customer_id ))
                cur_cos_ord_obj.save()
                chng_cos_ord_obj.save()
            except:
                message_code = 0
                # pass
            data = {'message_code': message_code, '_html_customers_select': _html}
            # data = {'message_code': message_code, '_html': _html}
            return JsonResponse(data)
            # return Jso(json.dumps(data, ensure_ascii=False), content_type="application/json")



@login_required(login_url='base-user:login')
def admin_team_plan_form_customer_ajax(request,e_p_id,l_id):
    # context = base_auth(req=request)
    profile= request.user
    if check_permission_for_admins(request,'work-plans') or profile.usertype == 2:
        # print("admin")
        if request.method == 'GET' and request.is_ajax():
            # print("get ajax")
            message_code = 0
            _html = ""
            _html_customers_select_first = '<option value="">{}</option>'.format(_("Select Customer"))
            _html_customers_select = '<option value="">{}</option>'.format(_("Select Customer"))
            try:
                message_code = 1
                teams_select_id = request.GET.get('teams_select_id')
                teams_work_day_id = request.GET.get('teams_work_day_id')
                map_data = []
                c_team_plan = PlanTeamWork.objects.filter(id=e_p_id).first()
                c_customer = Customer.objects.filter(id=l_id).first()
                if c_team_plan and c_customer:
                    ch_team_plan = PlanTeamWork.objects.filter(team_id=teams_select_id,week=c_team_plan.week,day_id=teams_work_day_id).first()
                    if ch_team_plan:
                        customer_orders = ch_team_plan.get_customers()
                        # teams = Team.objects.filter(status=True)
                        # work_days = WorkDay.objects.all().order_by('day')
                        for customer_orders_item in customer_orders:
                            _html_customers_select = '{0}{1}'.format(_html_customers_select,'<option value="{}">{}</option>'.format(customer_orders_item.id,customer_orders_item.customer.get_customer_name()))
            except:
                message_code = 0
                # pass
            if _html_customers_select == _html_customers_select_first:
                _html_customers_select = ''
            data = {'message_code': message_code, '_html_customers_select': _html_customers_select}
            # data = {'message_code': message_code, '_html': _html}
            return JsonResponse(data)








#
# @login_required(login_url='userprofile:sign_in')
# def team_plan_show_ajax(request):
#     context = base_auth(req=request)
#     profile = request.user
#
#
#     print('geldik')
#     if check_permission_for_admins(request,'work-plans') or profile.usertype == 2:
#         print('permission geldik')
#         search_form = PlansSearchForm(request.POST or None)
#         if request.method == 'POST' and request.is_ajax() and search_form.is_valid():
#             print('post valid geldik')
#             cleaned_data = search_form.cleaned_data
#             team_field = cleaned_data.get('team',0)
#
#             message_code = 0
#             _html = ""
#             _team_name = ""
#             week_list = [1, 2, 3, 4]
#             team_work_day_i = 0
#             if team_field:
#                 team = Team.objects.filter(id=team_field).first()
#                 _team_name = team.name
#                 for team_work_day in team.get_team_work_days():
#                     # team_get_plans_w_d_customer_list = []
#                     team_work_day_i+=1
#                     week_list_i = 0
#                     # _html = '{0}{1}'.format(_html,'')
#                     for week_item in week_list:
#                         _button = ""
#                         week_list_i += 1
#                         team_get_plans_w_d_customer_list = []
#                         # print("team_get_plans_w_d_customer_list={}".format(team_get_plans_w_d_customer_list))
#                         _html = '{0}{1}'.format(_html,
#                                                   render_to_string('work/include/teams/plan/_week_part.html',
#                                                                             {
#                                                                                 'week_item': week_item,
#                                                                                 'team_work_day': team_work_day,
#                                                                             }))
#                         team_get_plans_w_d = team_work_day.get_plans_w_d(week_item,team_work_day.day.id)
#                         loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by('order_index')
#                         team_get_plans_w_d_customer_i = 0
#                         for team_get_plans_w_d_customer in loc_oreders:
#                             team_get_plans_w_d_customer_list.append(team_get_plans_w_d_customer.customer_id)
#                             team_get_plans_w_d_customer_i +=1
#                             _html = '{0}{1}'.format(_html,
#                                                       render_to_string('work/include/teams/plan/_team_plans.html',
#                                                                                 {
#                                                                                     'team_work_day_i': team_work_day_i,
#                                                                                     'profile_type': profile.usertype,
#                                                                                     'week_list_i': week_list_i,
#                                                                                     'team_get_plans_w_d_customer_i': team_get_plans_w_d_customer_i,
#                                                                                     'team_get_plans_w_d': team_get_plans_w_d,
#                                                                                     'team_get_plans_w_d_get_customer': team_get_plans_w_d_customer,
#                                                                                     'team_get_plans_w_d_get_customer_minute': team_get_plans_w_d_customer.customer.get_customer_day_minute(week_day=team_work_day),
#                                                                                 }))
#                         if team_get_plans_w_d_customer_i > 0:
#                             _button = '{0}'.format(render_to_string('work/include/teams/plan/_team_plans_button.html',
#                                                                             {
#                                                                                 'team_get_plans_w_d_customer_list': team_get_plans_w_d_customer_list,
#                                                                                 # 'team_get_plans_w_d_customer_i': team_get_plans_w_d_customer_i,
#                                                                             }))
#                         # _html = '{0}{1}'.format( _html, '</ul></div>')
#                         team_get_plans_w_d_customer_list = []
#                         _html = '{0}{1}{2}'.format( _html,_button, '</ul></div></div>')
#                     message_code = 1
#                     # _html = '{0}{1}'.format(_html, '</div>')
#             data = {'message_code': message_code, '_html': _html, '_team_name': _team_name}
#             # data = {'message_code': message_code, '_html': _html}
#             return JsonResponse(data)
#         else:
#             raise Http404
#     else:
#         raise Http404
#
#
#
