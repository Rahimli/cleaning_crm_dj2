import hashlib
import os
import random
from datetime import timedelta
from uuid import uuid4

from PIL.PngImagePlugin import _idat
from decimal import Decimal
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.db.models import Q
from django.forms import inlineformset_factory, BaseFormSet, formset_factory
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse, Http404
from django.shortcuts import render, get_object_or_404, redirect

# Create your views here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.datetime_safe import datetime

from base_user.models import UserPermission
from cleaning_crm.settings import BASE_DIR
from general.functions import check_permission_for_admins, stripTags
from home.views import base_auth, base
from payment.models import CustomerPayment
from work.forms import *
from work.functions import get_date_type_custom, employee_contract_converter
from work.models import *
from django.utils.translation import ugettext_lazy as _
from geoposition import Geoposition
from django.utils.translation import ugettext as _

from work.tasks import send_atachment_mail, send_contract_html_mail

GUser = get_user_model()


class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False
# def base(req=None):
#     # company_information = CompanyInformation.objects.filter(active=True).order_by('-date').first()
#     # settings_obj = Settings.objects.filter().order_by('-date').first()
#     data = {
#         'now':datetime.now(),
#         # 'base_company_information':company_information,
#         # 'base_settings_obj':settings_obj,
#     }
#     return data
#
# def base_auth(req=None):
#     user = req.user
#     # profile = get_object_or_404(Profile, user=user)
#     # company_information = CompanyInformation.objects.filter(active=True).order_by('-date').first()
#     # settings_obj = Settings.objects.filter().order_by('-date').first()
#     data = {
#         'now':datetime.now(),
#         # 'base_profile':profile,
#         # 'base_plan_log':PlanLog.objects.filter(complated=False,rejcected=False),
#         # 'base_company_information':company_information,
#         # 'base_settings_obj':settings_obj,
#     }
#     return data



@login_required(login_url='base-user:login')
def customers_list(request,op_slug):
    user = request.user
    if check_permission_for_admins(request,'customer-list') is False:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)

    if op_slug == 'active':
        customer_list = Customer.objects.filter(deleted=False).order_by('-date')
    elif op_slug == 'removed':
        customer_list = Customer.objects.filter(deleted=True).order_by('-date')
    else:
        raise Http404
    search_form = CustomerSearchForm(user.id,request.GET or None)
    context['search_form'] = search_form
    context['op_slug'] = op_slug
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            customer_type = clean_data.get('customer_type','')

            full_name = clean_data.get('full_name','')

            company_name = clean_data.get('company_name','')
            contact_person = clean_data.get('contact_person','')
            cvr_number = clean_data.get('cvr_number','')

            phonenumber = clean_data.get('phonenumber','')
            email = clean_data.get('email','')
            street_name = clean_data.get('street_name','')
            zip_code = clean_data.get('zip_code','')
            city = clean_data.get('city','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            square_meters = clean_data.get('square_meters','')
            amount_of_bathrooms = clean_data.get('amount_of_bathrooms','')
            amount_of_kitchens = clean_data.get('amount_of_kitchens','')
            amount_of_floors = clean_data.get('amount_of_floors','')
            if customer_type:
                customer_list = customer_list.filter(customer_type=customer_type)
                if int(customer_type) == 2:
                    if cvr_number:
                        customer_list = customer_list.filter(cvr_number=cvr_number)
                    if contact_person:
                        customer_list = customer_list.filter(contact_person__icontains=contact_person)
                    if company_name:
                        customer_list = customer_list.filter(company_name__icontains=company_name)
                elif int(customer_type) == 1:
                    if full_name:
                        customer_list = customer_list.filter(full_name__icontains=full_name)
            if phonenumber:
                customer_list = customer_list.filter(phonenumber=phonenumber)
            if email:
                customer_list = customer_list.filter(email__icontains=email)
            if street_name:
                customer_list = customer_list.filter(street_name__icontains=street_name)
            if city:
                customer_list = customer_list.filter(city__icontains=city)
            if zip_code:
                customer_list = customer_list.filter(zip_code=zip_code)
            if start_date:
                customer_list = customer_list.filter(start_date__gte=start_date)
            if end_date:
                customer_list = customer_list.filter(start_date__lte=end_date)
            if square_meters:
                customer_list = customer_list.filter(square_meters=square_meters)
            if amount_of_bathrooms:
                customer_list = customer_list.filter(amount_of_bathrooms=amount_of_bathrooms)
            if amount_of_kitchens:
                customer_list = customer_list.filter(amount_of_kitchens=amount_of_kitchens)
            if amount_of_floors:
                customer_list = customer_list.filter(amount_of_floors=amount_of_floors)
            _html = ''
            message_code = 0
            result = ''
            for data_item in customer_list:
                _html = "{}{}".format(_html, render_to_string('work/include/customers/customers-list-item.html',{'data_item':data_item,'my_profile':user}))

            result = "{}{}".format(result, render_to_string('work/include/customers/customers-table.html',
                                                          {'html': _html}))

            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
        # else:
        #     print('search_form.errors={}'.format(search_form.errors))
    return render(request, 'work/admin/customers/customers-list.html', context=context)

@login_required(login_url='base-user:login')
def customers_payment_list(request):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # note_form = NoteForm(request.POST or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    payments = CustomerPayment.objects.filter(customer__user=user).order_by('-datetime')
    search_form = CustomerPaymentSearchForm(request.POST or None)
    context['search_form'] = search_form
    if request.method == 'POST' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data

            amount_max = clean_data.get('amount_max','')
            amount_min = clean_data.get('amount_min','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if payments:
                if amount_min:
                    payments = payments.filter(amount__gte=Decimal(amount_min))
                if amount_max:
                    payments = payments.filter(amount__lte=Decimal(amount_max))
                if start_date:
                    payments = payments.filter(datetime__gte=start_date)
                if end_date:
                    payments = payments.filter(datetime__lte=end_date)
            _html = ''
            message_code = 1
            result = ''
            for data_item in payments:
                _html = "{}{}".format(_html, render_to_string('work/include/payments/payments-list-item.html',{'data_item':data_item,'my_profile':user}))

            result = "{}{}".format(result, render_to_string('work/include/payments/payments-table.html',
                                                          {'html': _html}))

            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
            # data = {'message_code':message_code,'result':_html}
            # return JsonResponse(data)
    return render(request, 'work/admin/customers/payments/payment-list.html', context=context)



@login_required(login_url='base-user:login')
def all_payment_list(request):
    user = request.user
    if check_permission_for_admins(request,'all-payments-list') is False:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # note_form = NoteForm(request.POST or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    payments = CustomerPayment.objects.filter().order_by('-datetime')
    search_form = CustomerPaymentSearchForm(request.POST or None)
    context['search_form'] = search_form
    if request.method == 'POST' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data

            customer = clean_data.get('customer','')
            amount_max = clean_data.get('amount_max','')
            amount_min = clean_data.get('amount_min','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if payments:
                if amount_min:
                    payments = payments.filter(customer_id=int(customer))
                if amount_min:
                    payments = payments.filter(amount__gte=Decimal(amount_min))
                if amount_max:
                    payments = payments.filter(amount__lte=Decimal(amount_max))
                if start_date:
                    payments = payments.filter(datetime__gte=start_date)
                if end_date:
                    payments = payments.filter(datetime__lte=end_date)
            _html = ''
            message_code = 1
            result = ''
            for data_item in payments:
                _html = "{}{}".format(_html, render_to_string('work/include/payments/payments-list-item-admin.html',{'data_item':data_item,'my_profile':user}))

            result = "{}{}".format(result, render_to_string('work/include/payments/payments-table-admin.html',
                                                          {'html': _html}))

            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
            # data = {'message_code':message_code,'result':_html}
            # return JsonResponse(data)
    return render(request, 'work/admin/customers/payments/payment-list-admin.html', context=context)


@login_required(login_url='base-user:login')
def customer_add(request):
    user = request.user

    if check_permission_for_admins(request,'customer-create') is False:
        raise Http404

    now = timezone.now()
    context = base_auth(req=request)
    customer_form = CustomerForm(0,request.POST or None, request.FILES or None)


    if request.method == 'POST' and customer_form.is_valid():
        clean_data = customer_form.cleaned_data
        customer_type = clean_data.get('customer_type','')
        full_name = clean_data.get('full_name','')
        company_name = clean_data.get('company_name','')
        contact_person = clean_data.get('contact_person','')
        cvr_number = clean_data.get('cvr_number','')
        phonenumber = clean_data.get('phonenumber','')
        email = clean_data.get('email','')
        street_name = clean_data.get('street_name','')
        zip_code = clean_data.get('zip_code','')
        city = clean_data.get('city','')
        start_date = clean_data.get('start_date','')
        square_meters = clean_data.get('square_meters','')
        amount_of_bathrooms = clean_data.get('amount_of_bathrooms','')
        amount_of_kitchens = clean_data.get('amount_of_kitchens','')
        amount_of_floors = clean_data.get('amount_of_floors','')
        auto_generated_password = clean_data.get('auto_generated_password','')
        position = clean_data.get('position')
        address = clean_data.get('address')
        longitude = 0
        latitude = 0
        i = 1
        for pos in position:
            if i == 1:
                latitude = Decimal(pos)
            if i == 2:
                longitude = Decimal(pos)
            i += 1
        customer_obj = Customer(
            customer_type=customer_type,
            phonenumber=phonenumber,
            street_name=street_name,
            zip_code=zip_code,
            city=city,
            start_date=start_date,
            square_meters=square_meters,
            amount_of_bathrooms=amount_of_bathrooms,
            amount_of_kitchens=amount_of_kitchens,
            amount_of_floors=amount_of_floors,
            auto_generated_password=auto_generated_password,
            address=address,
            position=Geoposition(latitude, longitude),
        )
        if int(customer_type) == 1:
            customer_obj.full_name = full_name
        if int(customer_type) == 2:
            customer_obj.company_name = company_name
            customer_obj.contact_person = contact_person
            customer_obj.cvr_number = cvr_number

        random_string = str(random.random()).encode('utf8')
        salt = hashlib.sha1(random_string).hexdigest()[:5]
        salted = (salt + email).encode('utf8')

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)
        password = make_password(auto_generated_password, salt=salt)

        user = GUser(email=email, username=email, password=password,usertype=3,is_active=True, is_staff=False, is_superuser=False)
        user.save()
        customer_obj.user = user
        customer_obj.save()
        next_url = request.GET.get('next_url')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:customers-list', kwargs={'op_slug': 'active'}))
        return HttpResponseRedirect(next_url)
    context['customer_form'] = customer_form
    return render(request, 'work/admin/customers/customer-add-or-edit.html', context=context)



@login_required(login_url='base-user:login')
def customer_list(request,c_id):
    user = request.user
    context = base_auth(req=request)

    customer_obj = get_object_or_404(Customer,id=c_id)
    if check_permission_for_admins(request,'customer-create') is False:
        raise Http404
    card_form = CardForm(request.POST or None)

    if request.method == 'POST' and card_form.is_valid():
        clean_data = card_form.cleaned_data
        card_number = clean_data.get('card_number','')
        cvv = clean_data.get('cvv','')
        expire_day = clean_data.get('expire_day','')
        year = clean_data.get('year','')
        stripe_id = clean_data.get('stripe_id','')
        try:
            customer = stripe.Charge.create(
                amount = 489,
                currency = "USD",
                description = "489 dollar",
                card = stripe_id
            )
            return redirect('success')
        except:
            card_form.add_errorc("The card has been declined")
    context['card_form'] = card_form
    return render(request, 'work/admin/customers/add_or_edit_card.html', context=context)



import stripe
stripe.api_key = settings.STRIPE_SECRET
@login_required(login_url='base-user:login')
def customer_add_card(request):
    user = request.user
    context = base_auth(req=request)

    customer_obj = get_object_or_404(Customer,user=user)

    if user.usertype != 3:
        raise Http404
    card_form = CardForm(request.POST or None)

    if request.method == 'POST' and card_form.is_valid():
        clean_data = card_form.cleaned_data
        card_number = clean_data.get('card_number','')
        cvv = clean_data.get('cvv','')
        expire_day = clean_data.get('expire_day','')
        year = clean_data.get('year','')
        stripe_id = clean_data.get('stripe_id','')
        try:
            cs_obj ,created = CustomerStripe.objects.get_or_create(customer=customer_obj)
            if created:
                customer = stripe.Customer.create(
                    source=stripe_id,
                    email=customer_obj.user.email,
                )
                cs_obj.stripe_id = stripe_id
                card_number_str = str(card_number)
                cs_obj.card_number_last = card_number_str[-4:]
                cs_obj.customer_code = customer['sources']['data'][0]['customer']
                customer_obj.stripe_id = stripe_id
                customer_obj.save()
                cs_obj.save()
            else:
                customer = stripe.Customer.modify(cs_obj.customer_code,
                                       source=stripe_id,
                                       )
                card_number_str = str(card_number)

                cs_obj.card_number_last = card_number_str[-4:]
                cs_obj.stripe_id = stripe_id
                customer_obj.stripe_id = stripe_id
                customer_obj.save()
                cs_obj.save()

                # print(customer)
                print("card_number_str[-1:-5]={}".format(card_number_str[-4:]))
            # customer = stripe.Charge.create(
            #     amount = 50,
            #     currency = "USD",
            #     description = "489 dollar",
            #     card = stripe_id
            # )

            # cs_obj.amount = customer['amount']
            # cs_obj.currency = customer['currency']
            # cs_obj.transaction_code = customer['balance_transaction']
            # cs_obj.paid = customer['paid']
            return redirect('success')
        except:
            card_form.add_errorc("The card has been declined")
    context['publishable'] = settings.STRIPE_PUBLISHABLE
    context['card_form'] = card_form
    return render(request, 'work/admin/customers/add_or_edit_card.html', context=context)



@login_required(login_url='base-user:login')
def customer_edit(request,c_id):
    user = request.user
    if check_permission_for_admins(request,'customer-edit') is False:
        raise Http404
    now = timezone.now()
    context = base_auth(req=request)
    customer_obj = get_object_or_404(Customer,id=c_id)
    customer_form = CustomerForm(customer_obj.user_id,request.POST or None, request.FILES or None,initial={
        'customer_type':customer_obj.customer_type,
        'full_name':customer_obj.full_name,
        'company_name':customer_obj.company_name,
        'contact_person':customer_obj.contact_person,
        'cvr_number':customer_obj.cvr_number,
        'phonenumber':customer_obj.phonenumber,
        'email':customer_obj.user.email,
        'street_name':customer_obj.street_name,
        'zip_code':customer_obj.zip_code,
        'city':customer_obj.city,
        'start_date':customer_obj.start_date,
        'square_meters':customer_obj.square_meters,
        'amount_of_bathrooms':customer_obj.amount_of_bathrooms,
        'amount_of_kitchens':customer_obj.amount_of_kitchens,
        'amount_of_floors':customer_obj.amount_of_floors,
        'auto_generated_password':customer_obj.auto_generated_password,
        'position': Geoposition(customer_obj.position.latitude, customer_obj.position.longitude),
        'address': customer_obj.address,
        'user_id':customer_obj.user_id,
    })
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and customer_form.is_valid():
        clean_data = customer_form.cleaned_data
        customer_type = clean_data.get('customer_type','')
        full_name = clean_data.get('full_name','')
        company_name = clean_data.get('company_name','')
        contact_person = clean_data.get('contact_person','')
        cvr_number = clean_data.get('cvr_number','')
        phonenumber = clean_data.get('phonenumber','')
        email = clean_data.get('email','')
        street_name = clean_data.get('street_name','')
        zip_code = clean_data.get('zip_code','')
        city = clean_data.get('city','')
        start_date = clean_data.get('start_date','')
        square_meters = clean_data.get('square_meters','')
        amount_of_bathrooms = clean_data.get('amount_of_bathrooms','')
        amount_of_kitchens = clean_data.get('amount_of_kitchens','')
        amount_of_floors = clean_data.get('amount_of_floors','')
        auto_generated_password = clean_data.get('auto_generated_password','')
        position = clean_data.get('position',None)
        address = clean_data.get('address','')
        customer_obj.customer_type=customer_type
        customer_obj.phonenumber=phonenumber
        customer_obj.street_name=street_name
        customer_obj.zip_code=zip_code
        customer_obj.city=city
        customer_obj.start_date=start_date
        customer_obj.square_meters=square_meters
        customer_obj.amount_of_bathrooms=amount_of_bathrooms
        customer_obj.amount_of_kitchens=amount_of_kitchens
        customer_obj.amount_of_floors=amount_of_floors
        customer_obj.auto_generated_password=auto_generated_password
        customer_obj.address=address
        longitude = 0
        latitude = 0

        # return HttpResponse(businnes_type)
        i = 1
        for pos in position:
            if i == 1:
                latitude = Decimal(pos)
            if i == 2:
                longitude = Decimal(pos)
            i += 1
        customer_obj.position = Geoposition(latitude, longitude)
        if int(customer_type) == 1:
            customer_obj.full_name = full_name
        if int(customer_type) == 2:
            customer_obj.company_name = company_name
            customer_obj.contact_person = contact_person
            customer_obj.cvr_number = cvr_number

        random_string = str(random.random()).encode('utf8')
        salt = hashlib.sha1(random_string).hexdigest()[:5]
        salted = (salt + email).encode('utf8')

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)
        password = make_password(auto_generated_password, salt=salt)


        customer_obj.user.email = email
        customer_obj.user.username = email
        customer_obj.user.password = password
        customer_obj.user.save()
        customer_obj.save()
        next_url = request.GET.get('next_url')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:customers-list', kwargs={'op_slug': 'active'}))
        return HttpResponseRedirect(next_url)
    context['customer_form'] = customer_form
    context['customer_obj'] = customer_obj
    return render(request, 'work/admin/customers/customer-add-or-edit.html', context=context)



@login_required(login_url='base-user:login')
def customer_remove(request,c_id):
    user = request.user
    if check_permission_for_admins(request,'customer-delete') is False:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    customer_obj = get_object_or_404(Customer,id=c_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            if customer_obj.deleted:
                # print('deleted')
                customer_obj.deleted = False
                message = _('Item has activated !')
                message_code = 1
            else:
                customer_obj.deleted = True
                message = _('Item has deleted !')
                message_code = 2
            customer_obj.save()
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'customer-person':
    #     pass
    else:
        raise Http404




# @login_required(login_url='base-user:login')
def contract_confirm_success(request):
    context = base(req=request)

    return render(request, 'work/admin/contracts/contract-confirm-success.html', context=context)

def contract_confirm(request,tc_id):
    user = request.user
    # if user.usertype == 2 or user.usertype == 3:
    #     pass
    # else:
    #     raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base(req=request)
    confirmed_contract_obj = get_object_or_404(ConfirmedContract,uid_key=tc_id,confirm=False,expiry_date__gte=datetime.now() + timedelta(days=2))
    confirm_form = ConfirmedContractForm(request.POST or None, request.FILES or None)
    if request.method == 'POST' and confirm_form.is_valid():
        cleaned_data = confirm_form.cleaned_data
        confirm_f = cleaned_data.get('confirm')
        if confirm_f:
            confirm = True
        else:
            confirm = False
        confirmed_contract_obj.confirm = confirm
        if confirm:
            if confirmed_contract_obj.employee:
                confirmed_contract_obj.employee.user.is_active = True
                confirmed_contract_obj.employee.user.save()
            if confirmed_contract_obj.customer:
                confirmed_contract_obj.customer.user.is_active = True
                confirmed_contract_obj.customer.user.save()
        confirmed_contract_obj.save()
        return HttpResponseRedirect(reverse('work:contract-confirm-success'))
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'customer-person':
    #     pass
    context['confirmed_contract_obj'] = confirmed_contract_obj
    context['confirm_form'] = confirm_form
    return render(request, 'work/admin/contracts/contract-confirm.html', context=context)




@login_required(login_url='base-user:login')
def employees_list(request,op_slug):
    if check_permission_for_admins(request,'employee-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if op_slug == 'active':
        employee_list = Employee.objects.filter(deleted=False)
    elif op_slug == 'removed':
        employee_list = Employee.objects.filter(deleted=True)
    else:
        raise Http404
    # context['employee_list'] = employee_list

    search_form = EmployeeSearchForm(user.id,request.POST or None)
    context['search_form'] = search_form
    context['op_slug'] = op_slug
    if request.method == 'POST' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            employee_type = clean_data.get('employee_type','')
            subcontractor = clean_data.get('subcontractor','')

            full_name = clean_data.get('full_name','')
            cpr_number = clean_data.get('cpr_number','')
            birth_date = clean_data.get('birth_date','')

            phonenumber = clean_data.get('phonenumber','')
            email = clean_data.get('email','')

            street_name = clean_data.get('street_name','')
            zip_code = clean_data.get('zip_code','')
            city = clean_data.get('city','')

            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')

            employment_type = clean_data.get('employment_type','')
            amount_of_hours = clean_data.get('amount_of_hours','')
            hourly_rate = clean_data.get('hourly_rate','')
            if employee_type:
                employee_list = employee_list.filter(employee_type=employee_type)
                if int(employee_type) == 2:
                    if subcontractor:
                        employee_list = employee_list.filter(subcontractor_id=subcontractor)
            if full_name:
                employee_list = employee_list.filter(full_name__icontains=full_name)
            if cpr_number:
                employee_list = employee_list.filter(email__icontains=cpr_number)
            if birth_date:
                employee_list = employee_list.filter(birth_date=birth_date)

            if phonenumber:
                employee_list = employee_list.filter(phonenumber=phonenumber)
            if email:
                employee_list = employee_list.filter(email__icontains=email)

            if street_name:
                employee_list = employee_list.filter(street_name__icontains=street_name)
            if city:
                employee_list = employee_list.filter(city__icontains=city)
            if zip_code:
                employee_list = employee_list.filter(zip_code=zip_code)

            if start_date:
                employee_list = employee_list.filter(start_date__gte=start_date)
            if end_date:
                employee_list = employee_list.filter(start_date__lte=end_date)

            if employment_type:
                employee_list = employee_list.filter(employment_type=employment_type)
                if int(employment_type) == 1:
                    employee_list = employee_list.filter(amount_of_hours=amount_of_hours)
                if int(employment_type) == 2:
                    employee_list = employee_list.filter(hourly_rate=hourly_rate)
            _html = ''
            message_code = 0
            result = ''
            for data_item in employee_list:
                _html = "{}{}".format(_html, render_to_string('work/include/employees/employees-list-item.html',{'data_item':data_item,'my_profile':user}))

            result = "{}{}".format(result, render_to_string('work/include/employees/employees-table.html',
                                                          {'html': _html}))

            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
    return render(request, 'work/admin/employees/employees-list.html', context=context)

import pdfkit
@login_required(login_url='base-user:login')
def employee_add(request):
    if check_permission_for_admins(request,'employee-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)

    employee_form = EmployeeForm(0,request.POST or None, request.FILES or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and employee_form.is_valid():
        clean_data = employee_form.cleaned_data
        employee_type = clean_data.get('employee_type')
        subcontractor = clean_data.get('subcontractor')
        full_name = clean_data.get('full_name')
        cpr_number = clean_data.get('cpr_number')
        birth_date = clean_data.get('birth_date')
        phonenumber = clean_data.get('phonenumber')
        email = clean_data.get('email')
        street_name = clean_data.get('street_name')
        zip_code = clean_data.get('zip_code')
        city = clean_data.get('city')
        start_date = clean_data.get('start_date')
        employment_type = clean_data.get('employment_type')
        amount_of_hours = clean_data.get('amount_of_hours')
        hourly_rate = clean_data.get('hourly_rate')
        contract_file = clean_data.get('contract_file')
        social_security_card = clean_data.get('social_security_card')
        contract_template = clean_data.get('contract_template')
        picture = clean_data.get('picture')
        auto_generated_password = clean_data.get('auto_generated_password')
        employee_obj = Employee(
            employee_type=employee_type,
            full_name=full_name,
            cpr_number=cpr_number,
            birth_date=birth_date,
            phonenumber=phonenumber,
            street_name=street_name,
            zip_code=zip_code,
            city=city,
            start_date=start_date,
            employment_type=employment_type,
            auto_generated_password=auto_generated_password,
        )
        if contract_file:
            employee_obj.contract_file = contract_file
            # send_atachment_mail(email=email,subject=_('Contract'),html=_("Employee contract file"),attach_path=str(employee_obj.contract_file.url))
        else:
            employee_obj.social_security_card = social_security_card
            employee_obj.picture = picture
        if int(employee_type) == 2:
            employee_obj.subcontractor_id = subcontractor

        if int(employment_type) == 1:
            employee_obj.amount_of_hours = amount_of_hours
        elif int(employment_type) == 2:
            employee_obj.hourly_rate = hourly_rate

        random_string = str(random.random()).encode('utf8')
        salt = hashlib.sha1(random_string).hexdigest()[:5]
        salted = (salt + email).encode('utf8')

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)
        password = make_password(auto_generated_password, salt=salt)

        user = GUser(email=email, username=email, password=password,usertype=2,is_active=False, is_staff=False, is_superuser=False)
        user.save()
        employee_obj.user = user
        # try:
        employee_obj.save()
        if employee_obj.contract_file:
            confirmed_contract_obj = ConfirmedContract.objects.create(confirm=False,employee=employee_obj,expiry_date=datetime.now() + timedelta(days=2),contract_url=employee_obj.contract_file.url)
            confirmed_contract__url = "{}://{}{}".format(request.scheme, request.META['HTTP_HOST'],
                                                          reverse('work:contract-confirm',
                                                                  kwargs={'tc_id': confirmed_contract_obj.uid_key}))
            confirmed_contract__html = '<p>{}</p><p><a href="{}">{}</a></p>'.format(_("Employee contract file"),confirmed_contract__url,_("Go to Confirm page"))
            send_atachment_mail.delay(email,_('Contract'),confirmed_contract__html,str(employee_obj.contract_file.url))
            # send_contract_html_mail(email=email,subject=_('Contract generate'),html=_("Employee contract file generate"),contract_html='<html><head></head><body><h1>Title</h1></body></html>')
        else:
            contract_template_obj = ContractTemplate.objects.filter(id=contract_template).first()

            if contract_template_obj:
                confirmed_contract_obj = ConfirmedContract(confirm=False, employee=employee_obj,
                                                 expiry_date=datetime.now() + timedelta(days=2),
                                                 contract_url='file-url')
                confirmed_contract__url = "{}://{}{}".format(request.scheme,request.META['HTTP_HOST'],reverse('work:contract-confirm', kwargs={'tc_id': confirmed_contract_obj.uid_key}))
                confirmed_contract__html = '<p>{}</p><p><a href="{}">{}</a></p>'.format(_("Employee contract file"),
                                                                              confirmed_contract__url,_("Go to Confirm page"))

                confirmed_contract_obj.save()

                contract_html_general = "{}".format( render_to_string('work/include/contract-template/_generate-contract.html',{'content_html': contract_template_obj.content,'request':request}))
                contract_html_generated = employee_contract_converter(contract_html_general,employee_obj)

                send_contract_html_mail.delay(confirmed_contract_obj.uid_key,contract_html_generated=contract_html_generated,email=email,subject=_('Contract generate'),html=confirmed_contract__html)# with --page-size=Legal and --orientation=Landscape
        # except:
        #     user.delete()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:employees-list',kwargs={'op_slug':'active'}))
        return HttpResponseRedirect(next_url)




    context['employee_form'] = employee_form
    return render(request, 'work/admin/employees/employee-add-or-edit.html', context=context)


@login_required(login_url='base-user:login')
def employee_edit(request,e_id):
    if check_permission_for_admins(request,'employee-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    employee_obj = get_object_or_404(Employee,id=e_id)
    employee_form = EmployeeForm(employee_obj.user_id,request.POST or None, request.FILES or None,initial={
        'employee_type':employee_obj.employee_type,
        'subcontractor':employee_obj.subcontractor,
        'full_name':employee_obj.full_name,
        'cpr_number':employee_obj.cpr_number,
        'birth_date':employee_obj.birth_date,
        'phonenumber':employee_obj.phonenumber,
        'email':employee_obj.user.email,
        'street_name':employee_obj.street_name,
        'zip_code':employee_obj.zip_code,
        'city':employee_obj.city,
        'start_date':employee_obj.start_date,
        'employment_type':employee_obj.employment_type,
        'amount_of_hours':employee_obj.amount_of_hours,
        'hourly_rate':employee_obj.hourly_rate,
        'contract_file':employee_obj.contract_file,
        'auto_generated_password':employee_obj.auto_generated_password,
    })
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and employee_form.is_valid():
        clean_data = employee_form.cleaned_data
        employee_type = clean_data.get('employee_type')
        subcontractor = clean_data.get('subcontractor')
        full_name = clean_data.get('full_name')
        cpr_number = clean_data.get('cpr_number')
        birth_date = clean_data.get('birth_date')
        phonenumber = clean_data.get('phonenumber')
        email = clean_data.get('email')
        street_name = clean_data.get('street_name')
        zip_code = clean_data.get('zip_code')
        city = clean_data.get('city')
        start_date = clean_data.get('start_date')
        employment_type = clean_data.get('employment_type')
        amount_of_hours = clean_data.get('amount_of_hours')
        hourly_rate = clean_data.get('hourly_rate')
        contract_file = clean_data.get('contract_file')
        social_security_card = clean_data.get('social_security_card')
        picture = clean_data.get('picture')
        auto_generated_password = clean_data.get('auto_generated_password')

        employee_obj.employee_type=employee_type
        employee_obj.full_name=full_name
        employee_obj.cpr_number=cpr_number
        employee_obj.birth_date=birth_date
        employee_obj.phonenumber=phonenumber
        employee_obj.street_name=street_name
        employee_obj.zip_code=zip_code
        employee_obj.city=city
        employee_obj.start_date=start_date
        employee_obj.employment_type=employment_type
        employee_obj.contract_file=contract_file
        employee_obj.auto_generated_password=auto_generated_password

        if int(employee_type) == 2:
            employee_obj.subcontractor_id = subcontractor

        if int(employment_type) == 1:
            employee_obj.amount_of_hours = amount_of_hours
        elif int(employment_type) == 2:
            employee_obj.hourly_rate = hourly_rate

        random_string = str(random.random()).encode('utf8')
        salt = hashlib.sha1(random_string).hexdigest()[:5]
        salted = (salt + email).encode('utf8')

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)
        password = make_password(auto_generated_password, salt=salt)

        employee_obj.user.email = email
        employee_obj.user.username = email
        employee_obj.user.password = password


        if contract_file:
            employee_obj.contract_file = contract_file
            # send_atachment_mail(email=email,subject=_('Contract'),html=_("Employee contract file"),attach_path=str(employee_obj.contract_file.url))
        else:
            employee_obj.social_security_card = social_security_card
            employee_obj.picture = picture




        employee_obj.user.save()
        employee_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:employees-list',kwargs={'op_slug':'active'}))
        return HttpResponseRedirect(next_url)




    context['employee_form'] = employee_form
    context['employee_obj'] = employee_obj
    return render(request, 'work/admin/employees/employee-add-or-edit.html', context=context)



@login_required(login_url='base-user:login')
def employee_remove(request,e_id):
    if check_permission_for_admins(request,'employee-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    employee_obj = get_object_or_404(Employee,id=e_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            if employee_obj.deleted:
                employee_obj.deleted = False
                message = _('Item has activated !')
                message_code = 1
            else:
                employee_obj.deleted = True
                message = _('Item has deleted !')
                message_code = 2
            employee_obj.save()
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def subcontractors_list(request,op_slug):
    if check_permission_for_admins(request,'subcontractor-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)

    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if op_slug == 'active':
        datas = Subcontractor.objects.filter(deleted=False)
    elif op_slug == 'removed':
        datas = Subcontractor.objects.filter(deleted=True)
    else:
        raise Http404
    search_form = SubcontractorSearchForm(request.GET or None)
    context['search_form'] = search_form
    context['op_slug'] = op_slug
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            company_name = clean_data.get('company_name','')
            cvr_number = clean_data.get('cvr_number','')
            street_name = clean_data.get('zip_code','')
            city = clean_data.get('city','')
            phonenumber = clean_data.get('phonenumber','')
            full_name = clean_data.get('full_name','')
            email = clean_data.get('email','')
            if company_name:
                datas = datas.filter(company_name__icontains=company_name)
            if cvr_number:
                datas = datas.filter(cvr_number=cvr_number)
            if street_name:
                datas = datas.filter(street_name__icontains=street_name)
            if city:
                datas = datas.filter(city__icontains=city)
            if street_name:
                datas = datas.filter(street_name__icontains=street_name)
            if phonenumber:
                datas = datas.filter(cvr_number=phonenumber)
            if full_name:
                datas = datas.filter(full_name__icontains=full_name)
            if email:
                datas = datas.filter(email__icontains=email)
            _html = ''
            message_code = 0
            result = ''
            for data_item in datas:
                _html = "{}{}".format(_html, render_to_string('work/include/subcontractors/subcontractors-list-item.html',{'data_item':data_item,'my_profile':user}))

            result = "{}{}".format(result, render_to_string('work/include/subcontractors/subcontractors-table.html',
                                                          {'html': _html}))

            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
    context['datas'] = datas
    return render(request, 'work/admin/subcontractors/subcontractors-list.html', context=context)

@login_required(login_url='base-user:login')
def subcontractors_add(request):
    if check_permission_for_admins(request,'subcontractor-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    subcontractor_form = SubcontractorForm(request.POST or None, request.FILES or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and subcontractor_form.is_valid():
        clean_data = subcontractor_form.cleaned_data
        company_name = clean_data.get('company_name')
        cvr_number = clean_data.get('cvr_number')
        street_name = clean_data.get('street_name')
        zip_code = clean_data.get('zip_code')
        city = clean_data.get('city')
        phonenumber = clean_data.get('phonenumber')
        full_name = clean_data.get('full_name')
        email = clean_data.get('email')
        subcontractor_obj = Subcontractor(
            company_name=company_name,
            cvr_number=cvr_number,
            street_name=street_name,
            zip_code=zip_code,
            city=city,
            full_name=full_name,
            phonenumber=phonenumber,
            email=email,
        )
        subcontractor_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:subcontractors-list',kwargs={'op_slug':'active'}))
        return HttpResponseRedirect(next_url)
    context['subcontractor_form'] = subcontractor_form
    return render(request, 'work/admin/subcontractors/subcontractor-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def subcontractor_edit(request,s_id):
    if check_permission_for_admins(request,'subcontractor-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    subcontractor_obj = get_object_or_404(Subcontractor,id=s_id)
    subcontractor_form = SubcontractorForm(request.POST or None,initial={
        'company_name': subcontractor_obj.company_name,
        'cvr_number': subcontractor_obj.cvr_number,
        'street_name': subcontractor_obj.street_name,
        'zip_code': subcontractor_obj.zip_code,
        'city': subcontractor_obj.city,
        'phonenumber': subcontractor_obj.phonenumber,
        'full_name': subcontractor_obj.full_name,
        'email': subcontractor_obj.email,
    })
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and subcontractor_form.is_valid():
        clean_data = subcontractor_form.cleaned_data
        company_name = clean_data.get('company_name')
        cvr_number = clean_data.get('cvr_number')
        street_name = clean_data.get('street_name')
        zip_code = clean_data.get('zip_code')
        city = clean_data.get('city')
        phonenumber = clean_data.get('phonenumber')
        full_name = clean_data.get('full_name')
        email = clean_data.get('email')
        subcontractor_obj.company_name = company_name
        subcontractor_obj.cvr_number = cvr_number
        subcontractor_obj.street_name = street_name
        subcontractor_obj.zip_code = zip_code
        subcontractor_obj.city = city
        subcontractor_obj.full_name = full_name
        subcontractor_obj.phonenumber = phonenumber
        subcontractor_obj.email = email
        subcontractor_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:subcontractors-list',kwargs={'op_slug':'active'}))
        return HttpResponseRedirect(next_url)
    context['subcontractor_form'] = subcontractor_form
    context['subcontractor_obj'] = subcontractor_obj
    return render(request, 'work/admin/subcontractors/subcontractor-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def subcontractor_remove(request,s_id):
    if check_permission_for_admins(request,'subcontractor-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    subcontractor_obj = get_object_or_404(Subcontractor,id=s_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            if subcontractor_obj.deleted:
                subcontractor_obj.deleted = False
                message = _('Item has activated !')
                message_code = 1
            else:
                subcontractor_obj.deleted = True
                message = _('Item has deleted !')
                message_code = 2
            subcontractor_obj.save()
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404


@login_required(login_url='base-user:login')
def teams_list(request):
    if check_permission_for_admins(request,'team-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    search_form = TeamSearchForm(request.GET or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['search_form'] = search_form
    teams = Team.objects.filter().order_by('-date')
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            name = clean_data.get('name','')
            employee = clean_data.get('employee','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if name:
                teams = teams.filter(name__icontains=name)
            if employee:
                employee_obj = Employee.objects.filter(id=employee)
                teams = teams.filter(Q(employees=employee_obj))
            if start_date:
                teams = teams.filter(date__gte=start_date)
            if end_date:
                teams = teams.filter(date__lte=end_date)
            _html = '<div class="col-md-12 text-center">{}</div>'.format(_('No have data'))
            message_code = 0
            # result = ''
            if teams:
                _html = ''
                for data_item in teams:
                    _html = "{}{}".format(_html, render_to_string('work/include/teams/teams-list-item.html',{'data_item':data_item,'my_profile':user}))
            data = {'message_code':message_code,'result':_html}
            return JsonResponse(data)
        # else:
        #     print(search_form.errors)
    return render(request, 'work/admin/teams/teams-list.html', context=context)

@login_required(login_url='base-user:login')
def team_add(request):
    if check_permission_for_admins(request,'team-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    # next_url = reverse('work:tasks-list', kwargs={'company_id': company.id})
    context = base_auth(req=request)
    team_form = TeamForm(0,request.POST or None)
    if request.method == 'POST' and team_form.is_valid():
        extra = 1
    else:
        extra = 3

    TeamWorkDayFormSet = formset_factory(TeamWorkDayForm,max_num=7,extra=1, formset=RequiredFormSet)

    team_work_days_form_formset = TeamWorkDayFormSet(request.POST or None)

    if request.method == 'POST' and team_form.is_valid():
        clean_data = team_form.cleaned_data
        name = clean_data.get('name')
        employees = clean_data.get('employees')
        team_obj = Team(name=name)

        employees_o = Employee.objects.filter(id__in=employees)
        team_obj.save()
        for employees_o_item in employees_o:
            team_obj.employees.add(employees_o_item)
        team_obj.save()
        # for employees_o_item in employees_o:
        #     team_obj.employees.add(employees_o_item)
        team_obj.save()
        for form in team_work_days_form_formset:
            if form.is_valid():
                cleaned_data = form.cleaned_data
                print('is valid = {}'.format(cleaned_data))
                instance = form.save(commit=False)
                instance.team = team_obj
                instance.save()


        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:teams-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['team_form'] = team_form
    context['team_work_days_form_formset'] = team_work_days_form_formset
    return render(request, 'work/admin/teams/team-add-or-edit.html', context=context)


@login_required(login_url='base-user:login')
def team_edit(request,t_id):
    if check_permission_for_admins(request,'team-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    # next_url = reverse('work:tasks-list', kwargs={'company_id': company.id})
    context = base_auth(req=request)

    team_obj = get_object_or_404(Team,id=t_id)
    team_form = TeamForm(team_obj.id,request.POST or None,initial={
        'name':team_obj.name,
        'employees':[x.id for x in team_obj.employees.all()],
    })

    team_work_days = TeamWorkDay.objects.filter(team=team_obj)
    # FormSet = formset_factory(TaskCustomDayForm, extra=len(task_custom_days)
    # some_formset =   FormSet(initial=[{'id': x.id} for x in task_custom_days])
    inital_data = [{'day': x.day_id,
                    'minute': x.minute
                    }
                   for x in team_work_days]

    TeamWorkDayFormSet = formset_factory(TeamWorkDayForm, extra=1, max_num=7, formset=RequiredFormSet)

    team_work_days_form_formset = TeamWorkDayFormSet(request.POST or None, initial=inital_data)


    if request.method == 'POST' and team_form.is_valid():
        clean_data = team_form.cleaned_data
        name = clean_data.get('name')
        employees = clean_data.get('employees')
        team_obj.name = name

        employees_o = Employee.objects.filter(id__in=employees)
        team_obj.save()
        team_obj.employees.clear()
        for employees_o_item in employees_o:
            team_obj.employees.add(employees_o_item)
        team_obj.save()
        form_team_list = []
        for form in team_work_days_form_formset:
            if form.is_valid():
                cleaned_data = form.cleaned_data
                team_work_obj , created = TeamWorkDay.objects.update_or_create(team=team_obj,day=cleaned_data['day'])
                print("cleaned_data.get('minute') = {}".format(cleaned_data.get('minute')))
                print("cleaned_data = {}".format(cleaned_data))
                print("********************************************************************************")
                team_work_obj.minute = cleaned_data.get('minute')
                team_work_obj.save()
                form_team_list.append(team_work_obj.id)
                # instance = form.save(commit=False)
                # instance.team = team_obj
                # try:
                #     instance.save()
                # except:
                #     pass
                print("instance.fixed_cleaning_programs = {}".format(cleaned_data))
        # for employees_o_item in employees_o:
        #     team_obj.employees.add(employees_o_item)
        team_obj.save()
        TeamWorkDay.objects.filter(team=team_obj).exclude(id__in=form_team_list).delete()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:teams-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['team_form'] = team_form
    context['team_obj'] = team_obj
    context['team_work_days_form_formset'] = team_work_days_form_formset
    return render(request, 'work/admin/teams/team-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def team_remove(request,t_id):
    if check_permission_for_admins(request,'team-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    team_obj = get_object_or_404(Team,id=t_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            team_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def complaints_list(request):
    user = request.user
    if check_permission_for_admins(request,'complaint-list') or user.usertype == 3:
        pass
    else:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404


    if check_permission_for_admins(request, 'complaint-list'):
        complaints = Complaint.objects.all().order_by('-date')
    else:
        complaints = Complaint.objects.filter(user=user).order_by('-date')
    search_form = ReminderSearchForm(request.GET or None)
    context['search_form'] = search_form
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            search_key = clean_data.get('search_key','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if search_key:
                complaints = complaints.filter(Q(name__icontains=search_key)|Q(content__icontains=search_key))
            if start_date:
                complaints = complaints.filter(date_time__gte=start_date)
            if end_date:
                complaints = complaints.filter(date_time__lte=end_date)
            _html = ''
            message_code = 0
            result = ''
            for data_item in complaints:
                _html = "{}{}".format(_html, render_to_string('work/include/complaints/comlaints-list-item.html',{'data_item':data_item,'my_profile':user}))

            result = "{}{}".format(result, render_to_string('work/include/complaints/complaints-table.html',
                                                          {'html': _html}))

            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
    context['datas'] = complaints
    return render(request, 'work/general/complaints/complaints.html', context=context)



@login_required(login_url='base-user:login')
def complaint_add(request):
    user = request.user
    if check_permission_for_admins(request,'complaint-list') or user.usertype == 3:
        pass
    else:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    complaint_form = ComplaintForm(request.POST or None)
    if request.method == 'POST' and complaint_form.is_valid():
        clean_data = complaint_form.cleaned_data
        name = clean_data.get('name')
        content = clean_data.get('content')
        complaint_obj = Complaint()
        complaint_obj.user=user
        complaint_obj.name=name
        complaint_obj.content=content
        complaint_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:complaints-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['complaint_form'] = complaint_form
    return render(request, 'work/general/complaints/complaint-add-or-edit.html', context=context)



@login_required(login_url='base-user:login')
def complaint_edit(request,c_id):
    user = request.user
    if check_permission_for_admins(request,'complaint-list') or user.usertype == 3:
        pass
    else:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    complaint_obj = get_object_or_404(Complaint,id=c_id,user=user)
    context = base_auth(req=request)

    complaint_form = ComplaintForm(request.POST or None)
    if request.method == 'POST' and complaint_form.is_valid():
        clean_data = complaint_form.cleaned_data
        name = clean_data.get('name')
        content = clean_data.get('content')

        # complaint_obj.user=user
        complaint_obj.name=name
        complaint_obj.content=content
        complaint_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:complaints-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['complaint_form'] = complaint_form
    return render(request, 'work/general/complaints/complaint-add-or-edit.html', context=context)



@login_required(login_url='base-user:login')
def complaint_remove(request,c_id):
    user = request.user
    if check_permission_for_admins(request,'complaint-list') or user.usertype == 3:
        pass
    else:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    complaint_obj = get_object_or_404(Complaint,id=c_id,user=user)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            complaint_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404



@login_required(login_url='base-user:login')
def work_plan(request):
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    # context['customer_count'] = customers.count()
    return render(request, 'home/dashboard.html', context=context)




@login_required(login_url='base-user:login')
def reminders_list(request):
    if check_permission_for_admins(request,'reminder-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    search_form = ReminderSearchForm(request.GET or None)
    context['search_form'] = search_form
    reminders = Reminder.objects.filter(user=user).order_by('-date_time')
    # if user.usertype != 1:
    #     reminders = reminders.filter(user=user)
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            search_key = clean_data.get('search_key','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if search_key:
                reminders = reminders.filter(name__icontains=search_key)
            if start_date:
                reminders = reminders.filter(date_time__gte=start_date)
            if end_date:
                reminders = reminders.filter(date_time__lte=end_date)
            _html = '<div class="col-md-12 text-center">{}</div>'.format(_('No have data'))
            message_code = 0
            # result = ''
            if reminders:
                _html = ''
                for data_item in reminders:
                    _html = "{}{}".format(_html, render_to_string('work/include/reminders/reminder-list-item.html',{'data_item':data_item,'my_profile':user}))
            data = {'message_code':message_code,'_html':_html}
            return JsonResponse(data)
    context['datas'] = reminders
    return render(request, 'work/admin/reminders/reminders-list.html', context=context)


@login_required(login_url='base-user:login')
def reminder_add(request):
    if check_permission_for_admins(request,'reminder-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    reminder_form = ReminderForm(request.POST or None)
    if request.method == 'POST' and reminder_form.is_valid():
        clean_data = reminder_form.cleaned_data
        name = clean_data.get('name','')
        user_f = clean_data.get('user',None)
        date_time = clean_data.get('date_time','')
        reminder_obj = Reminder(name=name,date_time=date_time)
        if user_f:
            reminder_obj.user_id=user_f
        else:
            reminder_obj.user=user
        reminder_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:reminders-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['reminder_form'] = reminder_form
    return render(request, 'work/admin/reminders/reminder-add-or-edit.html', context=context)


@login_required(login_url='base-user:login')
def reminder_edit(request,r_id):
    if check_permission_for_admins(request,'reminder-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if user.usertype != 1:
        reminder_obj = get_object_or_404(Reminder,id=r_id,user=user)
    else:
        reminder_obj = get_object_or_404(Reminder, id=r_id)

    reminder_form = ReminderForm(request.POST or None,initial={
        'user':reminder_obj.user_id,
        'name':reminder_obj.name,
        'date_time':reminder_obj.date_time,})
    if request.method == 'POST' and reminder_form.is_valid():
        clean_data = reminder_form.cleaned_data
        name = clean_data.get('name')
        user_f = clean_data.get('user')
        date_time = clean_data.get('date_time')
        reminder_obj.name = name
        if user_f:
            reminder_obj.user_id=user_f
        else:
            reminder_obj.user=user
        reminder_obj.date_time = date_time
        reminder_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:reminders-list'))
        return HttpResponseRedirect(redirect_to=next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['reminder_form'] = reminder_form
    context['reminder_obj'] = reminder_obj
    return render(request, 'work/admin/reminders/reminder-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def reminder_remove(request,r_id):
    if check_permission_for_admins(request,'reminder-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if user.usertype != 1:
        reminder_obj = get_object_or_404(Reminder,id=r_id,user=user)
    else:
        reminder_obj = get_object_or_404(Reminder, id=r_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            reminder_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404




@login_required(login_url='base-user:login')
def tasks_list(request):
    user = request.user
    if check_permission_for_admins(request,'task-list') is False:
        raise Http404
    now = timezone.now()
    context = base_auth(req=request)

    search_form = TaskSearchForm(request.POST or None)
    context['search_form'] = search_form
    tasks = Task.objects.filter().order_by('-date')
    if user.usertype == 3:
        tasks.filter(customer__user=user)
    # if user.usertype != 1:
    #     reminders = reminders.filter(user=user)
    # print('second')
    if request.method == 'POST' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            search_key = clean_data.get('search_key','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if search_key:
                tasks = tasks.filter(name__icontains=search_key)
            # if start_date:
            #     tasks = tasks.filter(date_time__gte=start_date)
            # if end_date:
            #     tasks = tasks.filter(date_time__lte=end_date)
            _html = '<div class="col-md-12 text-center">{}</div>'.format(_('No have data'))
            message_code = 1
            # result = ''
            if tasks:
                _html = ''
                for data_item in tasks:
                    _html = "{}{}".format(_html, render_to_string('work/include/tasks/task-list-item.html',{'data_item':data_item,'my_profile':user}))
            data = {'message_code':message_code,'result':_html}
            return JsonResponse(data)
        # else:
        #     print(search_form.errors)
    return render(request, 'work/admin/tasks/tasks-list.html', context=context)


@login_required(login_url='base-user:login')
def task_add(request):
    from django.forms.formsets import formset_factory, BaseFormSet
    user = request.user
    if check_permission_for_admins(request,'task-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)


    TaskCustomDayFormSet = formset_factory(TaskCustomDayForm,extra=1, max_num=7, formset=RequiredFormSet)


    task_custom_day_form_formset = TaskCustomDayFormSet(request.POST or None)



    task_form = TaskForm(0,request.POST or None)
    if request.method == 'POST' and task_form.is_valid():
        cleaned_data = task_form.cleaned_data
        task_type = cleaned_data.get('task_type','')
        name = cleaned_data.get('name','')
        date_type_fixed = cleaned_data.get('date_type_fixed','')
        date_type_custom = cleaned_data.get('date_type_custom','')
        customer = cleaned_data.get('customer','')
        square_meter_cleaning_program = cleaned_data.get('square_meter_cleaning_program','')
        fixed_cleaning_programs = cleaned_data.get('fixed_cleaning_programs',[])
        total_time = cleaned_data.get('total_time','')
        total_price = cleaned_data.get('total_price','')
        description = cleaned_data.get('description','')

        task_obj = Task()
        task_obj.task_type = int(task_type)
        if int(task_type) == 2:
            task_obj.date_type_fixed = int(date_type_fixed)

        task_obj.name = name
        task_obj.customer = get_object_or_404(Customer,id=int(customer))
        if square_meter_cleaning_program:
            task_obj.square_meter_cleaning_program = get_object_or_404(CleaningProgram,id=int(square_meter_cleaning_program))
        task_obj.total_time = str(total_time)
        task_obj.total_price = str(total_price)
        task_obj.description = description
        task_obj.save()
        if int(task_type) == 1 or date_type_fixed and int(date_type_fixed) != 4 and fixed_cleaning_programs :
            for item in CleaningProgram.objects.filter(id__in=fixed_cleaning_programs):
                task_obj.fixed_cleaning_programs.add(item)
        if int(task_type) == 2:
            task_obj.date_type_fixed = int(date_type_fixed)
            if int(date_type_fixed) == 4:
                for form in task_custom_day_form_formset:
                    if form.is_valid():
                        cleaned_data = form.cleaned_data
                        if cleaned_data.get('price') == 0 or cleaned_data.get('time') == 0:
                            pass
                        else:
                            instance, created = TaskCustomDay.objects.update_or_create(task=task_obj,
                                                                                       task_day=cleaned_data.get(
                                                                                           'task_day'))
                            instance.price = cleaned_data.get('price')
                            instance.time = cleaned_data.get('time')
                            instance.desc = cleaned_data.get('desc')
                            instance.square_meter_cleaning_program = cleaned_data.get('square_meter_cleaning_program')
                            instance.fixed_cleaning_programs.clear()
                            for item in cleaned_data.get('fixed_cleaning_programs'):
                                instance.fixed_cleaning_programs.add(item)
                            instance.save()


        next_url = reverse('work:tasks-list')
        return HttpResponseRedirect(next_url)
    context['task_custom_day_form_formset'] = task_custom_day_form_formset
    context['task_form'] = task_form
    return render(request, 'work/admin/tasks/task-add-or-edit.html', context=context)



@login_required(login_url='base-user:login')
def task_edit(request,t_id):
    from django.forms.formsets import formset_factory, BaseFormSet
    user = request.user
    if check_permission_for_admins(request,'task-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    task_obj = get_object_or_404(Task,id=t_id)
    context = base_auth(req=request)
    task_form = TaskForm(task_obj.id,request.POST or None,initial={
        'task_type':task_obj.task_type,
        'name':task_obj.name,
        'date_type_fixed':task_obj.date_type_fixed,
        'customer':task_obj.customer_id,
        'square_meter_cleaning_program':task_obj.square_meter_cleaning_program_id,
        'fixed_cleaning_programs':[x.id for x in task_obj.fixed_cleaning_programs.all()],
        'total_time':task_obj.total_time,
        'total_price':task_obj.total_price,
        'description':task_obj.description,
    })
    task_custom_days = TaskCustomDay.objects.filter(task=task_obj)
    # FormSet = formset_factory(TaskCustomDayForm, extra=len(task_custom_days)
    # some_formset =   FormSet(initial=[{'id': x.id} for x in task_custom_days])
    inital_data = [{'task_day':x.task_day,
                    'square_meter_cleaning_program':x.square_meter_cleaning_program,
                    'fixed_cleaning_programs':x.fixed_cleaning_programs.all(),
                    'time':x.time,
                    'price':x.price,
                    'desc':x.desc,
                    }
                   for x in task_custom_days ]


    TaskCustomDayFormSet = formset_factory(TaskCustomDayForm,extra=1, max_num=7, formset=RequiredFormSet)


    task_custom_day_form_formset = TaskCustomDayFormSet(request.POST or None, initial=inital_data)
    # task_custom_day_form_formset = TaskCustomDayFormSet(request.POST or None, queryset=task_custom_days)
    # for subform, data in zip(task_custom_day_form_formset.forms, inital_data):
    #     subform.initial = data

    if request.method == 'POST' and task_form.is_valid():
        cleaned_data = task_form.cleaned_data
        task_type = cleaned_data.get('task_type','')
        name = cleaned_data.get('name','')
        date_type_fixed = cleaned_data.get('date_type_fixed','')
        date_type_custom = cleaned_data.get('date_type_custom','')
        customer = cleaned_data.get('customer','')
        square_meter_cleaning_program = cleaned_data.get('square_meter_cleaning_program','')
        fixed_cleaning_programs = cleaned_data.get('fixed_cleaning_programs',[])
        total_time = cleaned_data.get('total_time','')
        total_price = cleaned_data.get('total_price','')
        description = cleaned_data.get('description','')

        task_obj.task_type = int(task_type)
        # print("cleaned_data = {}".format(cleaned_data))
        # if int(task_type) == 2:
        task_obj.date_type_fixed = int(date_type_fixed)
        #     if int(date_type_fixed) == 4:
        #         bulk_task_custom_list = []
        #         print("date_type_custom = {}".format(date_type_custom))
        #         for date_type_custom_item in date_type_custom:
        #             bulk_task_custom_list.append(TaskCustomDay(task=task_obj,task_day=int(date_type_custom_item)))
        #         TaskCustomDay.objects.bulk_create(bulk_task_custom_list)
        task_obj.name = name
        task_obj.customer = get_object_or_404(Customer,id=int(customer))
        if square_meter_cleaning_program:
            task_obj.square_meter_cleaning_program = get_object_or_404(CleaningProgram,id=int(square_meter_cleaning_program))
        task_obj.total_time = str(total_time)
        task_obj.total_price = str(total_price)
        task_obj.description = description
        task_obj.fixed_cleaning_programs.clear()
        if int(task_type) == 1 or date_type_fixed and int(date_type_fixed) != 4 and fixed_cleaning_programs :
            for item in CleaningProgram.objects.filter(id__in=fixed_cleaning_programs):
                task_obj.fixed_cleaning_programs.add(item)
        print('task.save()')

        # if task_custom_day_form_formset.is_valid():

        # return HttpResponse(task_custom_day_form_formset)
        task_obj.save()
        TaskCustomDay.objects.filter(task=task_obj).delete()
        if int(task_type) == 2:
            task_obj.date_type_fixed = int(date_type_fixed)
            if int(date_type_fixed) == 4:
                for form in task_custom_day_form_formset:
                    if form.is_valid():
                        cleaned_data = form.cleaned_data
                        if cleaned_data.get('price') == 0 or cleaned_data.get('time') == 0:
                            pass
                        else:
                            instance, created = TaskCustomDay.objects.update_or_create(task=task_obj,
                                                                                       task_day=cleaned_data.get(
                                                                                           'task_day'))
                            instance.price = cleaned_data.get('price')
                            instance.time = cleaned_data.get('time')
                            instance.desc = cleaned_data.get('desc')
                            instance.square_meter_cleaning_program = cleaned_data.get('square_meter_cleaning_program')
                            instance.fixed_cleaning_programs.clear()
                            for item in cleaned_data.get('fixed_cleaning_programs'):
                                instance.fixed_cleaning_programs.add(item)
                            instance.save()
                            # return HttpResponse()
                            # instance.fixed_cleaning_programs.add(cleaned_data.get('fixed_cleaning_programs'))
                            instance.save()
                            print("instance.fixed_cleaning_programs = {}".format(cleaned_data))

                            # instance.fixed_cleaning_programs.save_m2m()
                        print("task_custom_day_form_formset = {}".format(task_custom_day_form_formset))
                        print("cleaned_data = {}".format(cleaned_data))

        next_url = reverse('work:tasks-list')
        # next_url = reverse('work:task-edit', kwargs={'t_id': task_obj.id})
        return HttpResponseRedirect(next_url)
    #     raise Http404
    context['task_obj'] = task_obj
    context['task_form'] = task_form
    context['task_custom_day_form_formset'] = task_custom_day_form_formset
    return render(request, 'work/admin/tasks/task-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def task_remove(request,t_id):
    if check_permission_for_admins(request,'task-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if user.usertype != 1:
        task_obj = get_object_or_404(Task,id=t_id,user=user)
    else:
        task_obj = get_object_or_404(Task, id=t_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            task_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def calendar(request):
    user = request.user
    if user.usertype != 3:
        if check_permission_for_admins(request,'calendar-show') is False or user.usertype == 2:
            raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    search_form = CalendarSearchForm(request.POST or None)
    if user.usertype == 3:
        calendar_edit_form = AdminCalendarForm(request.POST or None)
    else:
        calendar_edit_form = AdminCalendarForm(request.POST or None)
    calendar_form = AdminCalendarForm(request.POST or None)
    context['search_form'] = search_form
    if request.method == 'POST'  and search_form.is_valid() and request.is_ajax():
        clean_data = search_form.cleaned_data
        search_key = clean_data.get('search_key','')
        customer = clean_data.get('customer','')
        start_date = clean_data.get('start_date','')
        end_date = clean_data.get('end_date','')
        if user.usertype == 3:
            tasks = TaskDate.objects.filter()
        else:
            tasks = TaskDate.objects.all()
        # if search_key:
        #     tasks = tasks.filter(Q(title__icontains=search_key) | Q(content__icontains=search_key))
        # if start_date:
        #     tasks = tasks.filter(date__gte=start_date)
        # if end_date:
        #     tasks = tasks.filter(date__lte=end_date)
        # content = clean_data.get('content')
        # note_obj.title=title
        # note_obj.content=stripTags(content,['js'])
        # note_obj.save()
        # next_url = request.GET.get('next')
        # if next_url:
        #     next_url = next_url
        # else:
        #     next_url = "{}".format(reverse('work:notes-list'))
        # return HttpResponseRedirect(next_url)
        _html = ''
        message_code = 0
        # for data_item in reminders:
        _html = "{}{}".format(_html, render_to_string('work/include/calendar/calendar-full.html',
                                                          {
                                                              'task_dates':tasks,
                                                              'calendar_edit_form':calendar_edit_form,
                                                           }
                                                      ))
        data = {'message_code': message_code, 'result': _html}
        return JsonResponse(data)
    # context['customer_count'] = customers.count()
    return render(request, 'work/admin/calendar.html', context=context)



@login_required(login_url='base-user:login')
def economy(request):
    if check_permission_for_admins(request,'economy') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    # context['customer_count'] = customers.count()
    return render(request, 'work/admin/economy.html', context=context)


@login_required(login_url='base-user:login')
def salary(request):
    if check_permission_for_admins(request,'salary') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    # context['customer_count'] = customers.count()
    return render(request, 'work/admin/salary.html', context=context)




@login_required(login_url='base-user:login')
def settings_view(request):
    if check_permission_for_admins(request,'settings') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    # context['customer_count'] = customers.count()
    return render(request, 'home/dashboard.html', context=context)






@login_required(login_url='base-user:login')
def services_list(request):
    if check_permission_for_admins(request,'service-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    datas = Service.objects.filter().order_by('type')

    search_form = ServiceSearchForm(request.GET or None)
    context['search_form'] = search_form
    # print("^^^^^^^^^^^^^^^^^^^^^^^datas={}".format(datas.count()))
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            name = clean_data.get('name','')
            type = clean_data.get('type','')
            price_max = clean_data.get('price_max','')
            price_min = clean_data.get('price_min','')
            time_type = clean_data.get('time_type','')
            time_max = clean_data.get('time_max','')
            time_min = clean_data.get('time_min','')
            # print("^^^^^^^^^^^^^^^^^^^^^^^datas={}".format(datas.count()))
            if name:
                datas = datas.filter(name__icontains=name)
            if type:
                datas = datas.filter(type=type)
            if price_min:
                datas = datas.filter(price__gte=Decimal(price_min))
            if price_max:
                datas = datas.filter(price__lte=Decimal(price_max))
            if time_type:
                datas = datas.filter(time_type=time_type)
            if time_min:
                datas = datas.filter(time__gte=Decimal(time_min))
            if time_max:
                datas = datas.filter(time__lte=Decimal(time_max))
            _html = ''
            message_code = 0
            result = ''
            # print("^^^^^^^^^^^^^^^^^^^^^^^datas={}".format(datas.count()))
            for data_item in datas:
                _html = "{}{}".format(_html, render_to_string('work/include/services/service-list-item.html',{'data_item':data_item,'my_profile':user}))

            data = {'message_code':message_code,'result':_html}
            return JsonResponse(data)


    return render(request, 'work/admin/services/services-list.html', context=context)


@login_required(login_url='base-user:login')
def service_add(request):
    if check_permission_for_admins(request,'service-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    service_form = ServiceForm(request.POST or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and service_form.is_valid():
        clean_data = service_form.cleaned_data
        name = clean_data.get('name')
        type = clean_data.get('type')
        price = clean_data.get('price')
        time_type = clean_data.get('time_type')
        time = clean_data.get('time')
        service_obj = Service(type=type,name=name,price=price,time_type=type,time=time)
        service_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:services-list'))
        return HttpResponseRedirect(next_url)
    context['service_form'] = service_form
    return render(request, 'work/admin/services/service-add-or-edit.html', context=context)






@login_required(login_url='base-user:login')
def service_edit(request,s_id):
    if check_permission_for_admins(request,'service-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    service_obj = get_object_or_404(Service,id=s_id)
    now = timezone.now()
    context = base_auth(req=request)
    service_form = ServiceForm(request.POST or None,initial={
        'type':service_obj.type,
        'name':service_obj.name,
        'price':service_obj.price,
        'time_type':service_obj.time_type,
        'time':service_obj.time,
    })
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    if request.method == 'POST' and service_form.is_valid():
        clean_data = service_form.cleaned_data
        name = clean_data.get('name','')
        type = clean_data.get('type','')
        price = clean_data.get('price','')
        time_type = clean_data.get('time_type','')
        time = clean_data.get('time','')
        service_obj.type=type
        service_obj.name=name
        service_obj.price=price
        service_obj.time_type=type
        service_obj.time=time
        service_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:services-list'))
        return HttpResponseRedirect(next_url)
    context['service_form'] = service_form
    return render(request, 'work/admin/services/service-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def service_remove(request,s_id):
    if check_permission_for_admins(request,'service-remove') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    service_obj = get_object_or_404(Service,id=s_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            service_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def cleaning_programs_list(request):
    if check_permission_for_admins(request,'cleaning-program-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    datas = CleaningProgram.objects.all().order_by('-date')
    search_form = CleaningProgramSearchForm(request.POST or None)
    context['search_form'] = search_form
    if request.method == 'POST' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            name = clean_data.get('name','')
            assign_service = clean_data.get('assign_service','')
            if name:
                datas = datas.filter(name__icontains=name)
            if assign_service:
                datas = datas.filter(assign_service_id=assign_service)
            # _html = '<div class="col-md-12 text-center">{}</div>'.format(_('No have data'))
            message_code = 0
            result = ''
            # result = ''
            if datas:
                _html = ''
                for data_item in datas:
                    _html = "{}{}".format(_html, render_to_string('work/include/cleaning-programs/cleaning-programs-list-item.html',
                                                                  {'data_item': data_item, 'my_profile': user}))

                result = "{}{}".format(result, render_to_string('work/include/cleaning-programs/cleaning-programs-table.html',
                                                                {'html': _html}))
            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
    return render(request, 'work/admin/cleaning-programs/cleaning-programs-list.html', context=context)


@login_required(login_url='base-user:login')
def cleaning_program_add(request):
    if check_permission_for_admins(request,'cleaning-program-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    cleaning_program_form = CleaningProgramForm(request.POST or None)
    if request.method == 'POST' and cleaning_program_form.is_valid():
        clean_data = cleaning_program_form.cleaned_data
        name = clean_data.get('name')
        assign_service = clean_data.get('assign_service')
        cleaning_program_obj = CleaningProgram()
        cleaning_program_obj.assign_service_id=assign_service
        cleaning_program_obj.name=name
        cleaning_program_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:cleaning-programs-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['cleaning_program_form'] = cleaning_program_form
    return render(request, 'work/admin/cleaning-programs/cleaning-program-add-or-edit.html', context=context)


@login_required(login_url='base-user:login')
def cleaning_program_edit(request,c_id):
    if check_permission_for_admins(request,'cleaning-program-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    cleaning_program_obj = get_object_or_404(CleaningProgram,id=c_id)
    context = base_auth(req=request)
    cleaning_program_form = CleaningProgramForm(request.POST or None,initial={
        'name': cleaning_program_obj.name,
        'assign_service': cleaning_program_obj.assign_service.id,
    })
    if request.method == 'POST' and cleaning_program_form.is_valid():
        clean_data = cleaning_program_form.cleaned_data
        name = clean_data.get('name')
        assign_service = clean_data.get('assign_service')
        cleaning_program_obj.assign_service_id=assign_service
        cleaning_program_obj.name=name
        cleaning_program_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:cleaning-programs-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['cleaning_program_form'] = cleaning_program_form
    context['cleaning_program_obj'] = cleaning_program_obj
    return render(request, 'work/admin/cleaning-programs/cleaning-program-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def cleaning_program_remove(request,c_id):
    if check_permission_for_admins(request,'cleaning-program-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    cleaning_program_obj = get_object_or_404(CleaningProgram,id=c_id)
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            cleaning_program_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def contract_templates(request):
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    # context['customer_count'] = customers.count()
    return render(request, 'home/dashboard.html', context=context)


@login_required(login_url='base-user:login')
def contract_template_add(request):
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    # context['customer_count'] = customers.count()
    return render(request, 'home/dashboard.html', context=context)



@login_required(login_url='base-user:login')
def snippets_list(request):
    if check_permission_for_admins(request,'snippet-list') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    snippet_form = SnippetForm(request.POST or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    snippets = Snippet.objects.all()
    search_form = SnippetSearchForm(request.GET or None)
    context['search_form'] = search_form
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            search_key = clean_data.get('search_key','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if search_key:
                snippets = snippets.filter(Q(snippet__icontains=search_key))
            if start_date:
                snippets = snippets.filter(date__gte=start_date)
            if end_date:
                snippets = snippets.filter(date__lte=end_date)
            _html = ''
            message_code = 1
            result = ''
            for data_item in snippets:
                _html = "{}{}".format(_html, render_to_string('work/include/snippets/snippets-list-item.html',{'data_item':data_item,'my_profile':user}))

            data = {'message_code':message_code,'result':_html}
            return JsonResponse(data)
    return render(request, 'work/admin/snippets/snippets-list.html', context=context)


@login_required(login_url='base-user:login')
def snippet_add(request):
    if check_permission_for_admins(request,'snippet-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    snippet_form = SnippetForm(request.POST or None)
    if request.method == 'POST' and snippet_form.is_valid():
        clean_data = snippet_form.cleaned_data
        snippet = clean_data.get('snippet')
        # content = clean_data.get('content')
        snippet_user_type = clean_data.get('snippet_user_type')
        snippet_obj = Snippet()
        snippet_obj.snippet_user_type=snippet_user_type
        snippet_obj.snippet=snippet
        # snippet_obj.content=content
        snippet_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:snippets-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['snippet_form'] = snippet_form
    return render(request, 'work/admin/snippets/snippet-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def snippet_edit(request,s_id):
    if check_permission_for_admins(request,'snippet-edit') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    snippet_obj = get_object_or_404(Snippet,id=s_id)
    snippet_form = SnippetForm(request.POST or None,initial={
        'snippet':snippet_obj.snippet,
        'snippet_user_type':snippet_obj.snippet_user_type,
    })
    if request.method == 'POST' and snippet_form.is_valid():
        clean_data = snippet_form.cleaned_data
        snippet = clean_data.get('snippet')
        # content = clean_data.get('content')
        snippet_user_type = clean_data.get('snippet_user_type')
        snippet_obj.snippet=snippet
        snippet_obj.snippet_user_type=snippet_user_type
        # snippet_obj.content=content
        # snippet_obj.slug=slug
        snippet_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:snippets-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['snippet_form'] = snippet_form
    context['snippet_obj'] = snippet_obj
    return render(request, 'work/admin/snippets/snippet-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def snippet_remove(request,s_id):
    if check_permission_for_admins(request,'snippet-delete') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    snippet_obj = get_object_or_404(Snippet,id=s_id)

    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            snippet_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def notes_list(request):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # note_form = NoteForm(request.POST or None)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    notes = Note.objects.filter(user=user)
    search_form = NoteSearchForm(request.GET or None)
    context['search_form'] = search_form
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            search_key = clean_data.get('search_key','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if search_key:
                notes = notes.filter(Q(title__icontains=search_key)|Q(content__icontains=search_key))
            if start_date:
                notes = notes.filter(date__gte=start_date)
            if end_date:
                notes = notes.filter(date__lte=end_date)
            _html = ''
            message_code = 1
            result = ''
            for data_item in notes:
                _html = "{}{}".format(_html, render_to_string('work/include/notes/notes-list-item.html',{'data_item':data_item,'my_profile':user}))

            data = {'message_code':message_code,'result':_html}
            return JsonResponse(data)
    return render(request, 'work/general/notes/notes-list.html', context=context)


@login_required(login_url='base-user:login')
def note_add(request):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    note_form = NoteForm(request.POST or None)
    if request.method == 'POST' and note_form.is_valid():
        clean_data = note_form.cleaned_data
        title = clean_data.get('title')
        content = clean_data.get('content')
        note_obj = Note()
        note_obj.user=user
        note_obj.title=title
        note_obj.content=stripTags(content,['js'])
        note_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:notes-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['note_form'] = note_form
    return render(request, 'work/general/notes/note-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def note_edit(request,n_id):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    note_obj = get_object_or_404(Note,id=n_id)
    note_form = NoteForm(request.POST or None,initial={
        'title':note_obj.title,
        'content':note_obj.content,
    })
    if request.method == 'POST' and note_form.is_valid():
        clean_data = note_form.cleaned_data
        title = clean_data.get('title')
        content = clean_data.get('content')
        note_obj.title=title
        note_obj.content=stripTags(content,['js'])
        note_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:notes-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['note_form'] = note_form
    context['note_obj'] = note_obj
    return render(request, 'work/general/notes/note-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def note_view(request,n_id):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    note_obj = get_object_or_404(Note,id=n_id)

    context['note_obj'] = note_obj

    response = render(request, 'work/general/notes/note-view.html', context=context)
    return  response






@login_required(login_url='base-user:login')
def note_remove(request,n_id):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    note_obj = get_object_or_404(Note,id=n_id)

    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            note_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404




@login_required(login_url='base-user:login')
def contract_template_list(request):
    user = request.user
    if check_permission_for_admins(request,'contract-template-list') is False:
        raise Http404
    now = timezone.now()
    context = base_auth(req=request)
    datas = ContractTemplate.objects.all().order_by('-date')
    search_form = ContractTemplateSearchForm(request.POST or None)
    context['search_form'] = search_form
    if request.method == 'POST' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            search_key = clean_data.get('search_key','')
            start_date = clean_data.get('start_date','')
            end_date = clean_data.get('end_date','')
            if search_key:
                datas = datas.filter(title__icontains=search_key)
            if start_date:
                datas = datas.filter(date__gte=start_date)
            if end_date:
                datas = datas.filter(date__lte=end_date)
            # _html = '<div class="col-md-12 text-center">{}</div>'.format(_('No have data'))
            message_code = 1
            count_i = 0
            result = ''
            if datas:
                _html = ''
                for data_item in datas:
                    _html = "{}{}".format(_html, render_to_string('work/include/contract-template/contract-template-list-item.html',
                                                                  {'data_item': data_item, 'my_profile': user}))

                result = "{}{}".format(result, render_to_string('work/include/contract-template/contract-template-table.html',
                                                                {'html': _html}))
            data = {'message_code':message_code,'result':result}
            return JsonResponse(data)
    return render(request, 'work/admin/contracts/contract-templates-list.html', context=context)




@login_required(login_url='base-user:login')
def contract_template_add(request):
    user = request.user
    if check_permission_for_admins(request,'contract-template-add') is False:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    contract_template_form = ContractTemplateForm(request.POST or None)
    context['snippets'] = Snippet.objects.all().order_by('snippet')
    if request.method == 'POST' and contract_template_form.is_valid():
        clean_data = contract_template_form.cleaned_data
        user_type = clean_data.get('user_type')
        title = clean_data.get('title')
        content = clean_data.get('content')
        contract_template_obj = ContractTemplate()
        contract_template_obj.user_type=user_type
        contract_template_obj.title=title
        contract_template_obj.content=stripTags(content,['js'])
        contract_template_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:contract-template-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['contract_template_form'] = contract_template_form
    return render(request, 'work/admin/contracts/contract-template-add-or-edit.html', context=context)





@login_required(login_url='base-user:login')
def contract_template_edit(request,c_id):
    user = request.user
    if check_permission_for_admins(request,'contract-template-edit') is False:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    contract_template_obj = get_object_or_404(ContractTemplate,id=c_id)
    context['snippets'] = Snippet.objects.all().order_by('snippet')
    contract_template_form = ContractTemplateForm(request.POST or None,initial={
        'user_type':contract_template_obj.user_type,
        'title':contract_template_obj.title,
        'content':contract_template_obj.content,
    })
    if request.method == 'POST' and contract_template_form.is_valid():
        clean_data = contract_template_form.cleaned_data
        user_type = clean_data.get('user_type')
        title = clean_data.get('title')
        content = clean_data.get('content')
        contract_template_obj.title=title
        contract_template_obj.user_type=user_type
        contract_template_obj.content=stripTags(content,['js'])
        contract_template_obj.save()
        next_url = request.GET.get('next')
        if next_url:
            next_url = next_url
        else:
            next_url = "{}".format(reverse('work:contract-template-list'))
        return HttpResponseRedirect(next_url)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    # else:
    #     raise Http404
    context['contract_template_form'] = contract_template_form
    context['contract_template_obj'] = contract_template_obj
    return render(request, 'work/admin/contracts/contract-template-add-or-edit.html', context=context)




@login_required(login_url='base-user:login')
def contract_template_remove(request,ct_id):
    user = request.user
    if user.usertype != 3:
        raise Http404
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    contract_template_obj = get_object_or_404(ContractTemplate,id=ct_id)

    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            contract_template_obj.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    # if profile.type == 'admin-person':
    #     pass
    # elif profile.type == 'employee-person':
    #     pass
    else:
        raise Http404





@login_required(login_url='base-user:login')
def admin_list(request):
    user = request.user
    if request.user.usertype != 1:
        return HttpResponseRedirect(reverse('home:dashboard'))
    context = base_auth(req=request)
    search_form = AdminSearchForm(request.GET or None)
    context['search_form'] = search_form
    all_admins = GUser.objects.filter(Q(usertype=1)|Q(usertype=4)).order_by('usertype')
    if request.method == 'GET' and request.is_ajax():
        if search_form.is_valid():
            clean_data = search_form.cleaned_data
            first_name = clean_data.get('first_name','')
            last_name = clean_data.get('last_name','')
            email = clean_data.get('email','')
            usertype = clean_data.get('usertype','')
            permission = clean_data.get('permission','')
            if first_name:
                all_admins = all_admins.filter(Q(first_name__icontains=first_name))
            if last_name:
                all_admins = all_admins.filter(Q(last_name__icontains=last_name))
            if email:
                all_admins = all_admins.filter(Q(email__icontains=email))
            if usertype:
                all_admins = all_admins.filter(Q(usertype=usertype))
            if permission:
                pass
                # all_admins = all_admins.filter(Q(snippet__icontains=permission)|Q(content__icontains=permission))
            _html = ''
            message_code = 1
            result = ''
            adminstrative_count = 0
            admin_count = 0
            admin_show = False
            adminstrative_show = False
            for data_item in all_admins:
                if data_item.usertype == 1:
                    if admin_count == 0 and admin_show is False:
                        admin_show = True
                    else:
                        admin_show = False
                    admin_count += 1
                else:
                    admin_show = False
                if data_item.usertype == 4:
                    if adminstrative_count == 0 and adminstrative_show is False:
                        adminstrative_show = True
                    else:
                        adminstrative_show = False
                    adminstrative_count += 1
                else:
                    adminstrative_show = False

                print("admin_count = {}".format(admin_count))
                print("admin_show = {}".format(admin_show))
                _html = "{}{}".format(_html, render_to_string('work/include/admin-users/admin-users-list-item.html', {'data_item':data_item, 'my_profile':user,'adminstrative_count':adminstrative_count,'adminstrative_show':adminstrative_show,'admin_count':admin_count,'admin_show':admin_show}))

            data = {'message_code':message_code,'result':_html}
            return JsonResponse(data)

    response = render(request, 'work/admin/admin-users/admins-list.html', context=context)
    return  response

def admin_add(request):
    if request.user.usertype != 1:
        return HttpResponseRedirect(reverse('home:dashboard'))
    admin_form = AdminForm(0,request.POST or None)
    context = base_auth(req=request)
    context['admin_form'] = admin_form
    if request.method == 'POST':
        if admin_form.is_valid():
            clean_data = admin_form.cleaned_data
            name = clean_data.get('name','')
            surname = clean_data.get('surname','')
            email = clean_data.get('email','')
            m_password = clean_data.get('password','')
            permissions = clean_data.get('permissions',[])
            usertype = clean_data.get('usertype','')
            # retype_password = clean_data.get('retype_password')
            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]

            # activation_key = hashlib.sha1(salted).hexdigest()
            #
            # key_expires = datetime.datetime.today() + datetime.timedelta(1)
            password = make_password(m_password, salt=salt)
            user_obj = GUser(first_name=name,last_name=surname,email=email, username=email, password=password, usertype=usertype, is_active=True)
            if int(usertype) == 1:
                user_obj.is_staff = True
                user_obj.is_superuser = True
            else:
                user_obj.is_staff = False
                user_obj.is_superuser = False
            user_obj.save()
            stocks = []
            UserPermission.objects.filter(user=user_obj).delete()
            if int(usertype) == 4:
                if permissions:
                    for permission_item in permissions:
                        userper = UserPermission(user=user_obj, permission=permission_item)
                        stocks.append(userper)
                    UserPermission.objects.bulk_create(stocks)
            next_url = request.GET.get('next')
            if next_url:
                next_url = next_url
            else:
                next_url = "{}".format(reverse('work:admin-list'))
            return HttpResponseRedirect(next_url)
    response = render(request, 'work/admin/admin-users/admin-add-or-edit.html', context=context)
    return  response




def admin_edit(request,u_id):
    if request.user.usertype != 1:
        return HttpResponseRedirect(reverse('home:dashboard'))
    g_user = get_object_or_404(GUser,id=u_id)
    m_user = request.user
    if m_user.usertype == 1 and (g_user.usertype != 1 or m_user.id == g_user.id):
        pass
    else:
        raise Http404
    permissions_list = []
    for permission_item in UserPermission.objects.filter(user=g_user):
        permissions_list.append(permission_item.permission)
    admin_form = AdminEditForm(g_user.id,request.POST or None,initial={
        'name':g_user.first_name,
        'surname':g_user.last_name,
        'email':g_user.email,
        'usertype':g_user.usertype,
        'permissions':permissions_list,
    })
    context = base_auth(req=request)
    context['admin_form'] = admin_form
    context['g_user'] = g_user
    if request.method == 'POST':
        if admin_form.is_valid():
            clean_data = admin_form.cleaned_data
            name = clean_data.get('name','')
            surname = clean_data.get('surname','')
            email = clean_data.get('email','')
            m_password = clean_data.get('password','')
            permissions = clean_data.get('permissions',[])
            usertype = clean_data.get('usertype','')
            # retype_password = clean_data.get('retype_password')

            random_string = str(random.random()).encode('utf8')
            salt = hashlib.sha1(random_string).hexdigest()[:5]
            password = make_password(m_password, salt=salt)

            g_user.first_name = name
            g_user.last_name = surname
            g_user.email = email
            g_user.username = email
            g_user.usertype = usertype
            if m_password:
                g_user.password = password
            if int(usertype) == 1:
                g_user.is_staff = True
                g_user.is_superuser = True
            else:
                g_user.is_staff = False
                g_user.is_superuser = False
            g_user.save()
            stocks = []
            UserPermission.objects.filter(user=g_user).delete()
            if int(usertype) == 4:
                if permissions:
                    for permission_item in permissions:
                        userper = UserPermission(user=g_user, permission=permission_item)
                        stocks.append(userper)
                    UserPermission.objects.bulk_create(stocks)
            next_url = request.GET.get('next')
            if next_url:
                next_url = next_url
            else:
                next_url = "{}".format(reverse('work:admin-list'))
            return HttpResponseRedirect(next_url)
    response = render(request, 'work/admin/admin-users/admin-add-or-edit.html', context=context)
    return  response




def admin_remove(request,u_id):
    if request.user.usertype != 1:
        return HttpResponseRedirect(reverse('home:dashboard'))
    g_user = get_object_or_404(GUser,id=u_id)
    m_user = request.user
    if m_user.usertype == 1 and (g_user.usertype != 1 or m_user.id == g_user.id):
        pass
    else:
        raise Http404
    if request.method == 'GET' and request.is_ajax():
        message_code = 0
        message = ''
        try:
            g_user.delete()
            message_code = 1
            message = _('Item has deleted !')
        except:
            message = _('Something wrong happened')
        data = {'message_code':message_code,'message':message}
        return JsonResponse(data=data)
    else:
        raise Http404



def admin_own_change(request):
    user = request.user
    admin_form = AdminForm(0,request.POST or None,initial={
        'name':user.first_name,
        'surname':user.last_name,
        'email':user.email,
    })
    context = base_auth(req=request)
    context['admin_form'] = admin_form
    if request.method == 'POST':
        if admin_form.is_valid():
            clean_data = admin_form.cleaned_data
            name = clean_data.get('name')
            surname = clean_data.get('surname')
            email = clean_data.get('email')
            password = clean_data.get('password')
            # retype_password = clean_data.get('retype_password')
            user = GUser(first_name=name,last_name=surname,email=email, username=email, password=password, usertype=1, is_active=True, is_staff=True,
                         is_superuser=True)
            user.save()
            next_url = request.GET.get('next')
            if next_url:
                next_url = next_url
            else:
                next_url = "{}".format(reverse('work:snippets-list'))
            return HttpResponseRedirect(next_url)
    response = render(request, 'work/admin/admin-users/admin-add-or-edit.html', context=context)
    return  response


@login_required(login_url='base-user:login')
def change_password(request):
    user = request.user
    next_url = request.GET.get('next_url')
    form = ChangePasswordForm(user.username,request.POST or None)
    context = base_auth(req=request)
    context['form'] = form
    if request.method == 'POST':
        if form.is_valid():
            clean_data = form.cleaned_data
            new_password = clean_data.get('new_password')
            if user.usertype == 2:
                e_o = get_object_or_404(Employee,user=user)
                e_o.auto_generated_password = new_password
                e_o.save()
            if user.usertype == 3:
                c_o = get_object_or_404(Customer,user=user)
                c_o.auto_generated_password = new_password
                c_o.save()
            # random_string = str(random.random()).encode('utf8')
            # salt = hashlib.sha1(random_string).hexdigest()[:5]
            # password = make_password(new_password, salt=salt)
            user.set_password(new_password)
            user.save()

            form = ChangePasswordForm(user.username)
            context['message'] = _('password_changed_successfully')
            context['form'] = form
            # else:

    return render(request, 'base-user/change-password.html', context=context)




@login_required(login_url='base-user:login')
def user_profile(request,ec_id):
    profile = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    if profile.usertype != 1:
        raise Http404

    view_profile = get_object_or_404(GUser,id=ec_id)
    if view_profile.usertype == 2:
        today_reminders = Reminder.objects.filter(user=view_profile,date_time__day=now.day,date_time__month=now.month,date_time__year=now.year)
        data = get_object_or_404(Employee,user=view_profile)
        context['data'] = data
        # context['today_reminder'] = today_reminders
    elif view_profile.usertype == 3:
        data = get_object_or_404(Customer,user=view_profile)
        context['data'] = data
    # else:
    #     raise Http404
    # return HttpResponse(customers[:20].count())
    context['view_profile'] = view_profile
    # context['customer_count'] = customers.count()
    # context['last_customers'] = customers.all()[:20]
    # context['last_employees'] = employees.all()[:20]
    # context['tours'] = Tour.objects.filter(active=True)
    return render(request, 'work/profiles/profile.html', context=context)
