# from django.conf.urls import url
# from django.conf.urls.i18n import i18n_patterns
# from .views import *
from django.urls import path, re_path

from api_employee.views import EmployeeDayOrWeekTaskView, EmployeeThisDayOrWeekTaskView

app_name = 'api_customer'
urlpatterns = [
    path('employee-plan/<slug:day_week_slug>/<int:year>-<int:month>-<int:day>/', EmployeeDayOrWeekTaskView.as_view(), name='employee-date-plan'),
    path('employee-plan-this/<slug:day_week_slug>/', EmployeeThisDayOrWeekTaskView.as_view(), name='employee-this-date-plan'),
    # path('employee-details/', EmployeeThisDayOrWeekTaskView.as_view(), name='employee-this-date-plan'),
]