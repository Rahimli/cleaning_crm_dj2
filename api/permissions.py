from rest_framework.permissions import BasePermission

from general.functions import check_permission_for_admins


class IsAdministration(BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    message = 'You must be owner bla'

    # my_safe_method = ['PUT','GET',"DELETE"]

    def has_permission(self, request, view):
        if check_permission_for_admins(req=request,page_key=view.app_pname):
            return True
        return False







class IsCustomer(BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    message =   'You must be owner bla '
    # my_safe_method = ['PUT','GET',"DELETE"]

    def has_permission(self, request, view):
        if request.user.usertype == 3:
            return True
        return False



class IsEmployee(BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    message = 'You must be owner bla'

    # my_safe_method = ['PUT','GET',"DELETE"]

    def has_permission(self, request, view):
        if request.user.usertype == 2:
            return True
        return False




