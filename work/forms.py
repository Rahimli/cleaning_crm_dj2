from django import forms
from django.contrib import auth
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth import get_user_model
from django.core.exceptions import NON_FIELD_ERRORS
from django.db.models import Q
from django.utils.translation import ugettext as _
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from geoposition import forms as geoforms

from base_user.tools.common import USERTYPES, USERTYPES_for_admin
from work.common import rule_models_choices
from work.validators import integer_8_length_validate, integer_4_length_validate, integer_16_length_validate, \
    integer_3_length_validate, integer_2_length_validate, card_expire_day_validate

from work.models import *
from tinymce.widgets import TinyMCE
GUser = get_user_model()

class TinyMCEWidget(TinyMCE):
    def use_required_attribute(self, *args):
        return False


class EmployeeForm(forms.Form):
    employee_type = forms.ChoiceField(required=True, choices=employee_type_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    subcontractor = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    full_name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    cpr_number = forms.IntegerField(validators=[integer_8_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    birth_date = forms.DateField(required=True,
                                     widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':10,}))
    phonenumber = forms.IntegerField(validators=[integer_8_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    email = forms.EmailField(required=True,
                             widget=forms.EmailInput(attrs={'autocomplete':'off','class': 'form-control',}))
    street_name = forms.CharField(max_length=255, min_length=2, required=False,
                              widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    zip_code = forms.IntegerField(validators=[integer_4_length_validate], required=False,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    city = forms.CharField(max_length=255, min_length=2, required=False,
                           widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    employment_type = forms.ChoiceField(required=True, choices=employment_type_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    amount_of_hours = forms.DecimalField(required=False,max_digits=19,decimal_places=2,
                                  widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    hourly_rate = forms.DecimalField(required=False,max_digits=19,decimal_places=2,
                                  widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    contract_file = forms.FileField(required=False)
    social_security_card = forms.ImageField(required=False)
    picture = forms.ImageField(required=False)
    contract_template = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    auto_generated_password = forms.CharField(max_length=255,required=True,min_length=8,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16}))
    user_id = 0

    def __init__(self,cu_id,*args, **kwargs):
        super(EmployeeForm, self).__init__(*args, **kwargs)
        self.user_id = cu_id
        self.fields['subcontractor'].choices = [['',_('Choose Employee')]] + [[x.id, x.company_name] for x in Subcontractor.objects.all()]
        self.fields['contract_template'].choices = [['',_('Choose Contract template')]] + [[x.id, x.title] for x in ContractTemplate.objects.filter(user_type=1)]

    def clean(self):
        cleaned_data = self.cleaned_data
        employee_type = cleaned_data.get('employee_type')
        employment_type = cleaned_data.get('employment_type')
        subcontractor = cleaned_data.get('subcontractor')
        contract_template = cleaned_data.get('contract_template')
        amount_of_hours = cleaned_data.get('amount_of_hours')
        hourly_rate = cleaned_data.get('hourly_rate')
        email = cleaned_data.get('email')
        contract_file = cleaned_data.get('contract_file')
        social_security_card = cleaned_data.get('social_security_card',None)
        picture = cleaned_data.get('picture',None)
        if self.user_id == 0:
            user_obj = GUser.objects.filter(email=email)
            if contract_file:
                pass
            else:
                if social_security_card or picture or contract_template:
                    if social_security_card and picture and contract_template:
                        pass
                    elif social_security_card:
                        self._errors['picture'] = _('Picture is required')
                    elif picture:
                        self._errors['social_security_card'] = _('Social security card is required')
                    elif contract_template:
                        self._errors['contract_template'] = _('Contract template is required')
                else:
                    self._errors['contract_file'] = _('Contract file is required')

        else:
            user_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        if user_obj:
            self._errors['email'] = _('This email allready has taken')
        if employee_type == 2:
            if subcontractor == '':
                self._errors['subcontractor'] = _('Subcontractor is required')
        if employment_type == 1:
            if amount_of_hours == '':
                self._errors['amount_of_hours'] = _('Amount of hours is required')
        elif employment_type == 2:
            if hourly_rate == '':
                self._errors['hourly_rate'] = _('Hourly rate is required')
        return cleaned_data





class EmployeeSearchForm(forms.Form):
    employee_type = forms.ChoiceField(required=False, choices=employee_type_search_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    subcontractor = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    full_name = forms.CharField(max_length=255, min_length=2, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Full name"),'autocomplete': 'off', 'class': 'form-control', }))
    cpr_number = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Social security number"),'autocomplete': 'off', 'class': 'form-control', }))
    birth_date = forms.DateField(required=False,
                                     widget=forms.TextInput(attrs={'placeholder': _("Birth date"),'autocomplete':'off','class': 'form-control','size':10,}))

    phonenumber = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Phone number"),'autocomplete': 'off', 'class': 'form-control', }))
    email = forms.EmailField(required=False,
                             widget=forms.EmailInput(attrs={'placeholder': _("Email"),'autocomplete':'off','class': 'form-control',}))
    street_name = forms.CharField(max_length=255, required=False,
                              widget=forms.TextInput(attrs={'placeholder': _("Street name"),'autocomplete': 'off', 'class': 'form-control', }))
    zip_code = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Zip code"),'autocomplete': 'off', 'class': 'form-control', }))
    city = forms.CharField(max_length=255, required=False,
                           widget=forms.TextInput(attrs={'placeholder': _("City"),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Strart date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))
    employment_type = forms.ChoiceField(required=False, choices=employment_type_search_choices,
                                  widget=forms.Select(attrs={'placeholder': _("Employment type"),'autocomplete': 'off', 'class': 'form-control', }))
    amount_of_hours = forms.DecimalField(required=False,max_digits=19,decimal_places=2,
                                  widget=forms.TextInput(attrs={'placeholder': _("Amount of hours"),'autocomplete': 'off', 'class': 'form-control', }))
    hourly_rate = forms.DecimalField(required=False,max_digits=19,decimal_places=2,
                                  widget=forms.TextInput(attrs={'placeholder': _("Hourly rate"),'autocomplete': 'off', 'class': 'form-control', }))
    user_id = 0

    def __init__(self,cu_id,*args, **kwargs):
        super(EmployeeSearchForm, self).__init__(*args, **kwargs)
        self.user_id = cu_id
        self.fields['subcontractor'].choices = [['',_('Choose Employee')]] + [[x.id, x.company_name] for x in Subcontractor.objects.filter(deleted=False)]

expired_day = (
    ("01",1),
    ("02",2),
    ("03",3),
    ("04",4),
    ("05",5),
    ("06",6),
    ("07",7),
    ("08",8),
    ("09",9),
    ("10",10),
    ("11",11),
    ("12",12),
)
class CardForm(forms.Form):
    stripe_id = forms.CharField(max_length=255, required=True, widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))

    card_number = forms.IntegerField(validators=[integer_16_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    cvv = forms.IntegerField(validators=[integer_3_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    expire_month = forms.ChoiceField(choices=expired_day, required=True,
                                widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    year = forms.IntegerField(validators=[integer_4_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    def clean(self):
        cleaned_data = self.cleaned_data
        expire_month = cleaned_data.get('expire_month')
        year = cleaned_data.get('year')
        import datetime
        now = datetime.datetime.now()
        now_str = '{}-{}'.format(now.month,now.year)
        card_date_str = '{}-{}'.format(expire_month,year)
        if self.is_valid():
            if datetime.datetime.strptime(now_str, "%m-%Y") > datetime.datetime.strptime(card_date_str, "%m-%Y"):
                self._errors['card_number'] = _('Card is expired')
        return cleaned_data

    def add_errorc(self, message):
        self._errors[NON_FIELD_ERRORS] = self.error_class([message])


IMPORT_FILE_TYPES = ['.xls','.xlsx', ]

class ExcelDocumentForm(forms.Form):
    excelfile = forms.FileField(label='Select a file')

def clean(self):
    data = super(ExcelDocumentForm, self).clean()

    if 'excelfile' not in data:
        raise forms.ValidationError(_('The Excel file is required to proceed'))

    excelfile = data['excelfile']
    extension = os.path.splitext(excelfile.name)[1]
    if not (extension in IMPORT_FILE_TYPES):
        raise forms.ValidationError(_("This file is not a valid Excel file. Please make sure your input file is an Excel file )"))

    return data



class CustomerForm(forms.Form):
    customer_type = forms.ChoiceField(required=True, choices=customer_type_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    full_name = forms.CharField(max_length=255, min_length=2, required=False, widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    company_name = forms.CharField(max_length=255, min_length=2, required=False,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    contact_person = forms.CharField(max_length=255, min_length=2, required=False,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    cvr_number = forms.IntegerField(validators=[integer_8_length_validate], required=False,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    phonenumber = forms.IntegerField(validators=[integer_8_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    email = forms.EmailField(required=True,
                             widget=forms.EmailInput(attrs={'autocomplete':'off','class': 'form-control',}))
    street_name = forms.CharField(max_length=255, min_length=2, required=False,
                              widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    zip_code = forms.IntegerField(validators=[integer_4_length_validate], required=False,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    city = forms.CharField(max_length=255, min_length=2, required=False,
                           widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))

    square_meters = forms.DecimalField(max_digits=19,decimal_places=3,required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    amount_of_bathrooms = forms.DecimalField(max_digits=19,decimal_places=3,required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    amount_of_kitchens = forms.DecimalField(max_digits=19,decimal_places=3,required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    amount_of_floors = forms.DecimalField(max_digits=19,decimal_places=3,required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    auto_generated_password = forms.CharField(max_length=255,required=True,min_length=8,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    position = geoforms.GeopositionField()
    address = forms.CharField(required=True,
                              widget=forms.Textarea(attrs={'cols': 80, 'rows': 5,'class': 'form-control'}))
    user_id = 0
    def __init__(self,cu_id,*args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)
        self.user_id = cu_id

    def clean(self):
        cleaned_data = self.cleaned_data
        customer_type = cleaned_data.get('customer_type')
        full_name = cleaned_data.get('full_name')
        company_name = cleaned_data.get('company_name')
        contact_person = cleaned_data.get('contact_person')
        cvr_number = cleaned_data.get('cvr_number')
        email = cleaned_data.get('email')
        # print('*****************************************************************************')
        # print(self.user_id)
        # print('*****************************************************************************')
        if self.user_id == 0:
            user_obj = GUser.objects.filter(email=email)
        else:
            user_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)

        if user_obj:
            self._errors['email'] = _('This email allready has taken')
        if customer_type == 1:
            if full_name == '':
                self._errors['full_name'] = _('Full Name is required')
        if customer_type == 2:
            if company_name == '':
                self._errors['company_name'] = _('Company name is required')
            if contact_person == '':
                self._errors['contact_person'] = _('Contact person is required')
            if cvr_number == '':
                self._errors['cvr_number'] = _('Cvr number is required')
        return cleaned_data


class CustomerSearchForm(forms.Form):
    customer_type = forms.ChoiceField(required=False, choices=customer_type_search_choices,
                                  widget=forms.Select(attrs={'placeholder': _("Customer type"),'autocomplete': 'off', 'class': 'form-control', }))
    full_name = forms.CharField(max_length=255, required=False,

                                 widget=forms.TextInput(attrs={'placeholder': _("Full name"),'autocomplete': 'off', 'class': 'form-control', }))

    company_name = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Company name"),'autocomplete': 'off', 'class': 'form-control', }))
    contact_person = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Contact person"),'autocomplete': 'off', 'class': 'form-control', }))
    cvr_number = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Cvr number"),'autocomplete': 'off', 'class': 'form-control', }))

    phonenumber = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Phone number"),'autocomplete': 'off', 'class': 'form-control', }))
    email = forms.EmailField(required=False,
                             widget=forms.EmailInput(attrs={'placeholder': _("Email"),'autocomplete':'off','class': 'form-control',}))
    street_name = forms.CharField(max_length=255, required=False,
                              widget=forms.TextInput(attrs={'placeholder': _("Street name"),'autocomplete': 'off', 'class': 'form-control', }))
    zip_code = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Zip code"),'autocomplete': 'off', 'class': 'form-control', }))
    city = forms.CharField(max_length=255, required=False,
                           widget=forms.TextInput(attrs={'placeholder': _("City"),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Strart date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))

    square_meters = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Square meters"),'autocomplete':'off','class': 'form-control','size':16,}))
    amount_of_bathrooms = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Amount of bathrooms"),'autocomplete':'off','class': 'form-control','size':16,}))
    amount_of_kitchens = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Amount of kitchens"),'autocomplete':'off','class': 'form-control','size':16,}))
    amount_of_floors = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Amount of floors"),'autocomplete':'off','class': 'form-control','size':16,}))
    user_id = 0
    def __init__(self,cu_id,*args, **kwargs):
        super(CustomerSearchForm, self).__init__(*args, **kwargs)
        self.user_id = cu_id




class SubcontractorForm(forms.Form):

    company_name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    cvr_number = forms.IntegerField(validators=[integer_8_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    street_name = forms.CharField(max_length=255, min_length=2, required=True,
                              widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    zip_code = forms.IntegerField(validators=[integer_4_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    city = forms.CharField(max_length=255, min_length=2, required=True,
                           widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    # Contact Person
    phonenumber = forms.IntegerField(validators=[integer_8_length_validate], required=True,
                                widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    full_name = forms.CharField(max_length=255, min_length=2, required=True,

                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    email = forms.EmailField(required=True,
                             widget=forms.EmailInput(attrs={'autocomplete':'off','class': 'form-control',}))


class SubcontractorSearchForm(forms.Form):

    company_name = forms.CharField(max_length=255,  required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Company name"),'autocomplete': 'off', 'class': 'form-control', }))
    cvr_number = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Company registration number"),'autocomplete': 'off', 'class': 'form-control', }))
    street_name = forms.CharField(max_length=255,required=False,
                              widget=forms.TextInput(attrs={'placeholder': _("Street name"),'autocomplete': 'off', 'class': 'form-control', }))
    zip_code = forms.IntegerField( required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Zip code"),'autocomplete': 'off', 'class': 'form-control', }))
    city = forms.CharField(max_length=255, required=False,
                           widget=forms.TextInput(attrs={'placeholder': _("City"),'autocomplete': 'off', 'class': 'form-control', }))
    # Contact Person
    phonenumber = forms.IntegerField(required=False,
                                widget=forms.TextInput(attrs={'placeholder': _("Phone number"),'autocomplete': 'off', 'class': 'form-control', }))
    full_name = forms.CharField(max_length=255, required=False,

                                 widget=forms.TextInput(attrs={'placeholder': _("Full Name"),'autocomplete': 'off', 'class': 'form-control', }))
    email = forms.EmailField(required=False,
                             widget=forms.EmailInput(attrs={'placeholder': _("Email"),'autocomplete':'off','class': 'form-control',}))



class TeamForm(forms.Form):
    name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    employees = forms.MultipleChoiceField(required=True, choices=[],
                                  widget=forms.SelectMultiple(attrs={'autocomplete': 'off', 'class': 'form-control select2-multiple', }))
    team_id = 0
    def __init__(self,t_id,*args, **kwargs):
        super(TeamForm, self).__init__(*args, **kwargs)
        self.team_id = t_id
        employee_exc_list = []
        teams_obj = Team.objects
        if t_id != 0:
            teams_obj = teams_obj.exclude(id=t_id)
        else:
            teams_obj = teams_obj.all()
        for team_item in teams_obj:
            for employee_item in team_item.employees.all():
                employee_exc_list.append(employee_item.id)

        self.fields['employees'].choices = [[x.id, x.full_name] for x in Employee.objects.exclude(id__in=employee_exc_list).filter(deleted=False)]

    def clean(self):
        cleaned_data = self.cleaned_data
        employees = cleaned_data.get('employees')
        if len(employees)<2:
            self._errors['employees'] = _('Employee count must be greater than 1')
        return cleaned_data





class TeamWorkDayForm(forms.ModelForm):

    class Meta:
        model = TeamWorkDay
        fields = ('day','minute',)
        exclude = ('team',)
        widgets = {
            'minute' : forms.Select(attrs={'class': 'form-control','placeholder':_('description')}),
            'day' : forms.Select(attrs={'class': 'form-control'}),
        }
    # def __init__(self,*args, **kwargs):
    #     super(TeamWorkDayForm, self).__init__(*args, **kwargs)
    #     # self.user_id = cu_id
    #     self.fields['square_meter_cleaning_program'].queryset = CleaningProgram.objects.filter(assign_service__type=2)
    #     self.fields['fixed_cleaning_programs'].queryset = CleaningProgram.objects.filter(assign_service__type=1)






class TeamSearchForm(forms.Form):
    name = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Name"),'autocomplete': 'off', 'class': 'form-control', }))
    employee = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Strart date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))
    def __init__(self,*args, **kwargs):
        super(TeamSearchForm, self).__init__(*args, **kwargs)
        self.fields['employee'].choices = [['', _("Choose Employee")]] + [[x.id, x.full_name] for x in Employee.objects.filter(deleted=False)]



class ReminderForm(forms.Form):
    user = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    date_time = forms.DateTimeField(required=True,
                                   widget=forms.TextInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    def __init__(self,*args, **kwargs):
        super(ReminderForm, self).__init__(*args, **kwargs)
        all_admins = GUser.objects.filter(Q(usertype=1)|Q(usertype=4)).order_by('usertype')
        self.fields['user'].choices = [['', _("Choose User")]] + [[x.id, "{} {}".format(x.first_name,x.last_name)] for x in all_admins]



class ReminderSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))




class TaskForm(forms.Form):
    task_type = forms.ChoiceField(required=True, choices=task_type_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    # date_type = models.PositiveIntegerField(choices=task_type_fixed)
    date_type_fixed = forms.ChoiceField(required=True, choices=task_type_fixed_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    # date_type_custom = forms.MultipleChoiceField(required=True, choices=week_days_choices,
    #                               widget=forms.SelectMultiple(attrs={'autocomplete': 'off', 'class': 'form-control select2-multiple', }))
    customer = forms.ChoiceField(required=True, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    square_meter_cleaning_program = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    fixed_cleaning_programs = forms.MultipleChoiceField(required=False, choices=[],
                                      widget=forms.SelectMultiple(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    total_time = forms.CharField(max_length=255, required=False,
                                 widget=forms.NumberInput(attrs={'autocomplete': 'off', 'class': 'form-control','readonly':False }))
    total_price = forms.CharField(max_length=255, required=False,
                                 widget=forms.NumberInput(attrs={'autocomplete': 'off', 'class': 'form-control','readonly':False, }))
    description = forms.CharField(required=False,
                                 widget=forms.Textarea(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    task_id = 0

    def __init__(self,t_id,*args, **kwargs):
        super(TaskForm, self).__init__(*args, **kwargs)
        self.task_id = t_id
        self.fields['customer'].choices = [['', _("Choose Customer")]] + [[x.id, x.get_customer_name()] for x in Customer.objects.filter(deleted=False)]
        self.fields['square_meter_cleaning_program'].choices = [['', _("Choose Cleaning Program")]] + [[x.id, x.name] for x in CleaningProgram.objects.filter(assign_service__type=2)]
        self.fields['fixed_cleaning_programs'].choices = [[x.id, x.name] for x in CleaningProgram.objects.filter(assign_service__type=1)]
    def clean(self):
        cleaned_data = self.cleaned_data
        customer = int(cleaned_data.get('customer'))
        task_type = int(cleaned_data.get('task_type'))
        # date_type_fixed = cleaned_data.get('date_type_fixed')
        # task_type = int(cleaned_data.get('task_type'))
        if (task_type == 2):
            task_obj = Task.objects.filter(customer_id=customer,task_type=2)
            if task_obj:
                if self.task_id != 0:
                    if task_obj.exclude(id=self.task_id):
                        self._errors['task_type'] = _('Task type has taken for this Customer')
                else:
                    self._errors['task_type'] = _('Task type has taken for this Customer')
        return cleaned_data


class TaskCustomDayForm(forms.ModelForm):

    class Meta:
        model = TaskCustomDay
        fields = ('task_day','square_meter_cleaning_program','fixed_cleaning_programs','time','price','desc',)
        exclude = ('task',)
        widgets = {
            'desc': forms.Textarea(attrs={'autocomplete': 'off','rows':3, 'class': 'task-custom-day-part-item-desc form-control', }),
            'time' : forms.NumberInput(attrs={'class': 'task-custom-day-part-item-time form-control','placeholder':_('description')}),
            'price' : forms.NumberInput(attrs={'class': 'task-custom-day-part-item-price form-control','placeholder':_('description')}),
            'task_day' : forms.Select(attrs={'class': 'task-custom-day-part-item-day form-control'}),
            'square_meter_cleaning_program' : forms.Select(attrs={'class': 'task-custom-day-part-item-smcp form-control calculate-class'}),
            'fixed_cleaning_programs' : forms.SelectMultiple(attrs={'class': 'task-custom-day-part-item-fcp form-control calculate-class'}),
            # 'price' : forms.DecimalField(required=False,decimal_places=19,max_digits=2,default=0,
            #                          widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control','readonly':False })),
            # 'task_day' : forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }),
            # '',
            # '',
        }
    def __init__(self,*args, **kwargs):
        super(TaskCustomDayForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['square_meter_cleaning_program'].queryset = CleaningProgram.objects.filter(assign_service__type=2)
        self.fields['fixed_cleaning_programs'].queryset = CleaningProgram.objects.filter(assign_service__type=1)




class PlansSearchForm(forms.Form):
    team = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    def __init__(self,*args, **kwargs):
        super(PlansSearchForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['team'].choices = [[x.id, x.name] for x in Team.objects.filter()]



class TaskSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    customer = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))
    def __init__(self,*args, **kwargs):
        super(TaskSearchForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['customer'].choices = [['', _("Choose Customer")]] + [[x.id, x.get_customer_name()] for x in Customer.objects.filter(deleted=False)]
        # self.fields['cleaning_program'].choices = [[x.id, x.name] for x in CleaningProgram.objects.filter()]




class ServiceForm(forms.Form):
    name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    type = forms.ChoiceField(required=True, choices=service_type_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    price = forms.DecimalField(max_digits=19,decimal_places=2,required=True,
                                   widget=forms.NumberInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))
    time_type = forms.ChoiceField(required=False, choices=service_time_type_choices,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', 'disabled':True}))
    time = forms.DecimalField(max_digits=19,decimal_places=2,required=True,
                                   widget=forms.NumberInput(attrs={'autocomplete':'off','class': 'form-control','size':16,}))




class ServiceSearchForm(forms.Form):
    name = forms.CharField(max_length=255,required=False,
                                 widget=forms.TextInput(attrs={'placeholder':_('Name'),'autocomplete': 'off', 'class': 'form-control', }))
    type = forms.ChoiceField(required=False, choices=service_type_search_choices,
                                  widget=forms.Select(attrs={'placeholder':_('Type'),'autocomplete': 'off', 'class': 'form-control', }))
    price_max = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder':_('Price max'),'autocomplete':'off','class': 'form-control','size':16,}))
    price_min = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder':_('Price min'),'autocomplete':'off','class': 'form-control','size':16,}))
    time_type = forms.ChoiceField(required=False, choices=service_time_search_type_choices,
                                  widget=forms.Select(attrs={'placeholder':_('Time type'),'autocomplete': 'off', 'class': 'form-control', }))
    time_max = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder':_('Time max'),'autocomplete':'off','class': 'form-control','size':16,}))
    time_min = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder':_('Time min'),'autocomplete':'off','class': 'form-control','size':16,}))




class CleaningProgramForm(forms.Form):
    name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    assign_service = forms.ChoiceField(required=True, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    def __init__(self,*args, **kwargs):
        super(CleaningProgramForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['assign_service'].choices = [['', _("Choose Service")]] + [[x.id, x.name] for x in Service.objects.all()]


class CleaningProgramSearchForm(forms.Form):
    name = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    assign_service = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    def __init__(self,*args, **kwargs):
        super(CleaningProgramSearchForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['assign_service'].choices = [['', _("Choose Service")]] + [[x.id, x.name] for x in Service.objects.all()]


class ComplaintForm(forms.Form):
    name = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    content = forms.CharField(required=True,
                                 widget=forms.Textarea(attrs={'autocomplete': 'off', 'class': 'form-control', }))



class ComplaintSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))




class SnippetForm(forms.Form):
    snippet_user_type = forms.ChoiceField(required=True,choices=SNIPPET_CONRACT_USERTYPE_CHOICE,
                                 widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    # content = forms.CharField(required=True,
    #                              widget=forms.Textarea(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    snippet = forms.ChoiceField( choices=SNIPPET_EMPLOYEE_CUSTOMER_CHOICE, required=True,
                                 widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))



class SnippetSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))






class NoteForm(forms.Form):
    title = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    content = forms.CharField(required=True,
                              widget=SummernoteWidget(attrs={'cols': 80, 'rows': 30}))



class NoteSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))




class CustomerPaymentSearchForm(forms.Form):
    customer = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))

    amount_max = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                   widget=forms.TextInput(attrs={'placeholder':_('Price max'),'autocomplete':'off','class': 'form-control','size':16,}))
    amount_min = forms.DecimalField(max_digits=19,decimal_places=3,required=False,
                                    widget=forms.TextInput(attrs={'placeholder': _('Price min'), 'autocomplete': 'off',
                                                                  'class': 'form-control', 'size': 16, }))

    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))

    def __init__(self,*args, **kwargs):
        super(CustomerPaymentSearchForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['customer'].choices = [['', _("Choose Customer")]] + [[x.id, x.get_customer_name()] for x in Customer.objects.filter(deleted=False).order_by('full_name')]



class ContractTemplateForm(forms.Form):
    user_type = forms.ChoiceField(required=True,choices=SNIPPET_CONRACT_USERTYPE_CHOICE,
                                 widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    title = forms.CharField(max_length=255, min_length=2, required=True,
                                 widget=forms.TextInput(attrs={'autocomplete': 'off', 'class': 'form-control', }))
    content = forms.CharField(required=True,
                              widget=SummernoteWidget(attrs={'cols': 80, 'rows': 30}))



class ContractTemplateSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))




class CalendarSearchForm(forms.Form):
    search_key = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder': _("Search key..."),'autocomplete': 'off', 'class': 'form-control', }))
    customer = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    start_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("Start date"),'autocomplete':'off','class': 'form-control','size':16,}))
    end_date = forms.DateField(required=False,
                                   widget=forms.TextInput(attrs={'placeholder': _("End date"),'autocomplete':'off','class': 'form-control','size':16,}))
    def __init__(self,*args, **kwargs):
        super(CalendarSearchForm, self).__init__(*args, **kwargs)
        # self.user_id = cu_id
        self.fields['customer'].choices = [['', _("Choose Customer")]] + [[x.id, x.full_name] for x in Customer.objects.filter(deleted=False).order_by('full_name')]


class AdminForm(forms.Form):
    name = forms.CharField(required=True,max_length=254, label="First name", error_messages={})
    surname = forms.CharField(required=True,max_length=254, label="Last name", error_messages={})
    email = forms.EmailField(required=True,error_messages={})
    usertype = forms.ChoiceField(required=True, choices=USERTYPES_for_admin,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    permissions = forms.MultipleChoiceField(required=False,
        choices=rule_models_choices,
        widget=FilteredSelectMultiple("verbose name", is_stacked=False, attrs={'class':'form-control','id':'my_multi_select1'}),
        )
    password = forms.CharField(required=True,label=_("Password"))
    retype_password = forms.CharField(required=True,label=_("Password confirm"))
    user_id = 0

    def __init__(self,u_id, *args, **kwargs):
        self.user_id = u_id
        super(AdminForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={
            'placeholder': _("First name"), 'class': 'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'placeholder': _("Last name"), 'class': 'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'placeholder': _("Email"), 'class': 'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Password"), 'class': 'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Password confirm"), 'class': 'form-control'})


    def clean(self):
        cleaned_data = super(AdminForm, self).clean()

        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')
        if self.user_id == 0:
            user_obj = GUser.objects.filter(email=email)
        else:
            user_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        if user_obj:
            self._errors['email'] = _('Employee is allready use')
        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError(_("Passwords not same"))
        return cleaned_data





class AdminEditForm(forms.Form):
    name = forms.CharField(required=True,max_length=254, label="First name", error_messages={})
    surname = forms.CharField(required=True,max_length=254, label="Last name", error_messages={})
    email = forms.EmailField(required=True,error_messages={})
    usertype = forms.ChoiceField(required=True, choices=USERTYPES_for_admin,
                                  widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
    permissions = forms.MultipleChoiceField(required=False,
        choices=rule_models_choices,
        widget=FilteredSelectMultiple("verbose name", is_stacked=False, attrs={'class':'form-control','id':'my_multi_select1'}),
        )
    password = forms.CharField(required=False,label=_("Password"))
    retype_password = forms.CharField(required=False,label=_("Password confirm"))
    user_id = 0

    def __init__(self,u_id, *args, **kwargs):
        self.user_id = u_id
        super(AdminEditForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={
            'placeholder': _("First name"), 'class': 'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'placeholder': _("Last name"), 'class': 'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'placeholder': _("Email"), 'class': 'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Password"), 'class': 'form-control'})
        self.fields['retype_password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Password confirm"), 'class': 'form-control'})


    def clean(self):
        cleaned_data = super(AdminEditForm, self).clean()

        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        password_confirm = cleaned_data.get('retype_password')
        if self.user_id == 0:
            user_obj = GUser.objects.filter(email=email)
        else:
            user_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        if user_obj:
            self._errors['email'] = _('Employee is allready use')
        if password and password_confirm:
            if password != password_confirm:
                raise forms.ValidationError(_("Passwords not same"))
        return cleaned_data


# class AdminEditForm(forms.Form):
#     name = forms.CharField(required=True,max_length=254, label="First name", error_messages={})
#     surname = forms.CharField(required=True,max_length=254, label="Last name", error_messages={})
#     email = forms.EmailField(required=True,error_messages={})
#     usertype = forms.ChoiceField(required=True, choices=USERTYPES_for_admin,
#                                   widget=forms.Select(attrs={'autocomplete': 'off', 'class': 'form-control select2-single', }))
#     permissions = forms.MultipleChoiceField(
#         choices=rule_models_choices,
#         widget=FilteredSelectMultiple("verbose name", is_stacked=False, attrs={'class':'form-control','id':'my_multi_select1'}),
#         )
#     password = forms.CharField(required=False,label=_("Password"))
#     retype_password = forms.CharField(required=True,label=_("Password confirm"))
#     user_id = 0
#
#     def __init__(self,u_id, *args, **kwargs):
#         self.user_id = u_id
#         super(AdminEditForm, self).__init__(*args, **kwargs)
#         self.fields['name'].widget = forms.TextInput(attrs={
#             'placeholder': _("First name"), 'class': 'form-control'})
#         self.fields['surname'].widget = forms.TextInput(attrs={
#             'placeholder': _("Last name"), 'class': 'form-control'})
#         self.fields['email'].widget = forms.EmailInput(attrs={
#             'placeholder': _("Email"), 'class': 'form-control'})
#         self.fields['password'].widget = forms.PasswordInput(attrs={
#             'placeholder': _("Password"), 'class': 'form-control'})
#         self.fields['retype_password'].widget = forms.PasswordInput(attrs={
#             'placeholder': _("Password confirm"), 'class': 'form-control'})
#


class AdminSearchForm(forms.Form):
    first_name = forms.CharField(max_length=255,required=False,
                                 widget=forms.TextInput(attrs={'placeholder':_('First name'),'autocomplete': 'off', 'class': 'form-control', }))
    last_name = forms.CharField(max_length=255, required=False,
                                 widget=forms.TextInput(attrs={'placeholder':_('Last name'),'autocomplete': 'off', 'class': 'form-control', }))
    usertype = forms.ChoiceField(required=False, choices=USERTYPES_for_admin,
                                  widget=forms.Select(attrs={'placeholder':_('User ype'),'autocomplete': 'off', 'class': 'form-control select2-single', }))
    email = forms.CharField(required=False,
                             widget=forms.TextInput(attrs={'placeholder':_('Email'),'autocomplete':'off','class': 'form-control',}))
    permission = forms.ChoiceField(required=False, choices=[],
                                  widget=forms.Select(attrs={'placeholder':_('Permission'),'autocomplete': 'off', 'class': 'form-control select2-single', }))


class AdminChangeForm(forms.Form):
    name = forms.CharField(max_length=254, label="First name", error_messages={})
    surname = forms.CharField(max_length=254, label="Last name", error_messages={})
    email = forms.EmailField(error_messages={})
    password = forms.CharField(label=_("Password"))
    retype_password = forms.CharField(label=_("Password confirm"))
    user_id = 0
    def __init__(self,u_id, *args, **kwargs):
        self.user_id = u_id
        super(AdminChangeForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget = forms.TextInput(attrs={
            'placeholder': _("First name"), 'class': 'form-control'})
        self.fields['surname'].widget = forms.TextInput(attrs={
            'placeholder': _("Last name"), 'class': 'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={
            'placeholder': _("Email"), 'class': 'form-control'})
        self.fields['password'].widget = forms.PasswordInput(attrs={
            'placeholder': _("Password"), 'class': 'form-control'})

    def clean(self):
        cleaned_data = super(AdminChangeForm, self).clean()

        email = cleaned_data.get('email')
        password = cleaned_data.get('password')
        if self.user_id == 0:
            user_obj = GUser.objects.filter(email=email)
        else:
            user_obj = GUser.objects.filter(email=email).exclude(id=self.user_id)
        if user_obj is None:
            self._errors['email'] = 'Password is incorrect'
        return cleaned_data


class ChangePasswordForm(forms.Form):
    old_password = forms.CharField(max_length=255,min_length=2,required=True,widget=forms.PasswordInput(attrs={
            'placeholder': _("Old Password"), 'class': 'form-control'}))
    new_password = forms.CharField(max_length=255,min_length=8,required=True,widget=forms.TextInput(attrs={
            'placeholder': _("New Password"), 'class': 'form-control'}))
    username = 'example'
    def __init__(self,username,*args, **kwargs):
        super(ChangePasswordForm, self).__init__(*args, **kwargs)
        self.username = username
    def clean(self):
        cleaned_data = self.cleaned_data
        old_password = cleaned_data.get('old_password')
        new_password = cleaned_data.get('new_password')
        a_user = auth.authenticate(username=self.username, password=old_password)
        if a_user is None:
            self._errors['old_password'] = 'Password is incorrect'
        # if len(new_password) <=7:
        #     self._errors['new_password'] = 'password_greater_than_7'
        # if len(repeat_password) <=7:
        #     self._errors['repeat_password'] = 'password_greater_than_7'
        # print(identified)





class ConfirmedContractForm(forms.Form):
    confirm = forms.BooleanField(required=True,widget=forms.CheckboxInput(attrs={
            'class': ''}))


class AdminCalendarForm(forms.Form):
    employees = forms.MultipleChoiceField(required=True, choices=[],
                                  widget=forms.SelectMultiple(attrs={'autocomplete': 'off', 'class': 'form-control select2-multiple', }))
    is_canceled = forms.BooleanField(required=True, widget=forms.CheckboxInput(attrs={
        'class': ''}))
    def __init__(self,*args, **kwargs):
        super(AdminCalendarForm, self).__init__(*args, **kwargs)
        employee_exc_list = []
        # for employee_item in Employee.objects.filter(user__is_active=True):
        #     employee_exc_list.append(employee_item.id)

        self.fields['employees'].choices = [[x.id, x.full_name] for x in Employee.objects.filter(user__is_active=True).filter(deleted=False)]

    def clean(self):
        cleaned_data = self.cleaned_data
        employees = cleaned_data.get('employees')
        if len(employees)<2:
            self._errors['employees'] = _('Employee count must be greater than 1')
        return cleaned_data




















