from rest_framework import serializers

from work.models import Complaint, Note


class ListComplaintSerializer(serializers.ModelSerializer):
    edit_url         = serializers.SerializerMethodField(read_only=True)
    view_url         = serializers.SerializerMethodField(read_only=True)
    destroy_url         = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Complaint
        fields = ('edit_url','view_url','destroy_url','user','name', 'is_read','date',)
    def get_edit_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[0]
    def get_view_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[1]
    def get_destroy_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[2]


class CreateUpdateComplaintSerializer(serializers.ModelSerializer):
    class Meta:
        model = Complaint
        fields = ('user','name', 'content')


class ListNoteSerializer(serializers.ModelSerializer):
    edit_url         = serializers.SerializerMethodField(read_only=True)
    view_url         = serializers.SerializerMethodField(read_only=True)
    destroy_url         = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Note
        fields = ('edit_url','view_url','destroy_url','user','title', 'content','date',)
    def get_edit_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[0]
    def get_view_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[1]
    def get_destroy_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[2]


class CreateUpdateNoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Note
        fields = ('title', 'content')