from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from rest_framework.generics import ListCreateAPIView

from .views import *
from django.urls import path


app_name = 'api_administration'
urlpatterns = [
    path('reminders/list/', ReminderListApiView.as_view(), name='reminder-list'),
    path('reminders/create/', ReminderListApiView.as_view(), name='reminder-add'),
    path('reminders/edit/<int:pk>/', ReminderListApiView.as_view(), name='reminder-edit'),
    path('reminders/view/<int:pk>/', ReminderListApiView.as_view(), name='reminder-view'),
    path('reminders/remove/<int:pk>/',ReminderListApiView.as_view(), name='reminder-remove'),

]
