from django.apps import AppConfig


class ApiAdministrationConfig(AppConfig):
    name = 'api_administration'
