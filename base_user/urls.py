from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns

from .views import *

app_name = 'base-user'
urlpatterns = [
	url('login/', log_in, name='login'),

	url('logout/', log_out, name='logout'),

]
