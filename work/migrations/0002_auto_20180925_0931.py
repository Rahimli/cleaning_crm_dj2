# Generated by Django 2.0 on 2018-09-25 07:31

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('work', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PlanTeamWorkDate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('plan_team_work', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='work.PlanTeamWork')),
            ],
        ),
        migrations.CreateModel(
            name='PlanTeamWorkDateEmployee',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('employee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='work.Employee')),
                ('plan_team_work_date', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='work.PlanTeamWorkDate')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='planteamworkdateemployee',
            unique_together={('plan_team_work_date', 'employee')},
        ),
    ]
