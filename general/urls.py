
from django.urls import path

from .views import *


app_name = 'general'
urlpatterns = [
	path('plan-prepare/ajax/<slug:op_slug>/', plan_prepare_ajax, name='plan-prepare-ajax'),
]
