from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone

from general.functions import check_permission_for_admins
from home.views import base_auth
from work.forms import TaskCustomDayForm, TaskForm
from work.models import Customer, CleaningProgram, TaskCustomDay, Task
from work.views import RequiredFormSet


@login_required(login_url='base-user:login')
def task_calculate(request):
    from django.forms.formsets import formset_factory, BaseFormSet
    user = request.user
    if check_permission_for_admins(request,'task-create') is False:
        raise Http404
    user = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)

    data = {}
    if request.method == 'POST' and request.is_ajax():
        customer = request.POST.get('customer','')
        square_meter_cleaning_program = request.POST.get('square_meter_cleaning_program',None)
        fixed_cleaning_programs = request.POST.getlist('fixed_cleaning_programs[]',[])
        fixed_cleaning_programs_list = []
        for fixed_cleaning_program_item in fixed_cleaning_programs:
            fixed_cleaning_programs_list.append(int(fixed_cleaning_program_item))
        print("customer = {}".format(customer))
        print("square_meter_cleaning_program = {}".format(square_meter_cleaning_program))
        print("fixed_cleaning_programs = {}".format(fixed_cleaning_programs))
        customer_obj = Customer.objects.filter(id=customer).first()
        try:
            square_meter_cleaning_program_obj = CleaningProgram.objects.filter(assign_service__type=2,id=square_meter_cleaning_program).first()
        except:
            square_meter_cleaning_program_obj = None
        fixed_cleaning_programs_obj = CleaningProgram.objects.filter(assign_service__type=1,id__in=fixed_cleaning_programs_list)
        result_price = 0
        result_time = 0
        if customer_obj:
            if square_meter_cleaning_program_obj:
                result_time += customer_obj.square_meters * square_meter_cleaning_program_obj.assign_service.time
                result_price += customer_obj.square_meters * square_meter_cleaning_program_obj.assign_service.price
            if fixed_cleaning_programs_obj:
                print('salam')
                for fixed_item in fixed_cleaning_programs_obj:
                    result_time += fixed_item.assign_service.time
                    result_price += fixed_item.assign_service.price
        data = {'result_time':result_time,'result_price':result_price}
        return JsonResponse(data=data)

