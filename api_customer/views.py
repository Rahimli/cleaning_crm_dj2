from datetime import datetime

from django.db.models import Q
from django.http import Http404
from django.shortcuts import render

# Create your views here.
from django.template.loader import render_to_string
from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.permissions import IsCustomer, IsEmployee
from api_customer.permissions import IsOwnerComplaint, IsOwnerGeneral
from api_customer.serializers import CreateUpdateComplaintSerializer, ListComplaintSerializer, ListNoteSerializer, \
    CreateUpdateNoteSerializer
from work.functions import get_day, get_week
from work.models import Complaint, Note, Team, CustomerOrder


class ComplaintListApiView(generics.ListAPIView):
    # queryset = Complaint.objects.filter()
    app_pname  = 'complaint-list'
    serializer_class = ListComplaintSerializer
    permission_classes = (IsAuthenticated,IsCustomer,)
    def get_queryset(self):
        user = self.request.user
        complaints = Complaint.objects.filter(user=user).order_by('-date')
        search_key = self.request.query_params.get('search_key', '')
        start_date = self.request.query_params.get('start_date', '')
        end_date = self.request.query_params.get('end_date', '')
        if search_key:
            complaints = complaints.filter(Q(name__icontains=search_key) | Q(content__icontains=search_key))
        if start_date:
            complaints = complaints.filter(date_time__gte=start_date)
        if end_date:
            complaints = complaints.filter(date_time__lte=end_date)
        return complaints



class ComplaintApiView(APIView):
    # queryset = Complaint.objects.filter()
    app_pname  = 'complaint-list'
    serializer_class = ListComplaintSerializer
    permission_classes = (IsAuthenticated,IsCustomer,)
    queryset = Complaint.objects.filter().order_by('-date')
    def get_object(self, pk):
        try:
            return Complaint.objects.get(pk=pk)
        except Complaint.DoesNotExist:
            raise Http404
    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = ListComplaintSerializer(snippet)
        return Response({'status':"success",'data':serializer.data})
    # def get(self):
    #
    #     user = self.request.user
    #     complaints = Complaint.objects.filter(user=user).order_by('-date')
    #     search_key = self.request.query_params.get('search_key', '')
    #     start_date = self.request.query_params.get('start_date', '')
    #     end_date = self.request.query_params.get('end_date', '')
    #     if search_key:
    #         complaints = complaints.filter(Q(name__icontains=search_key) | Q(content__icontains=search_key))
    #     if start_date:
    #         complaints = complaints.filter(date_time__gte=start_date)
    #     if end_date:
    #         complaints = complaints.filter(date_time__lte=end_date)
    #     return complaints

class ComplaintCreateApiView(generics.CreateAPIView):
    app_pname  = 'complaint-create'
    queryset = Complaint.objects.filter()
    serializer_class = CreateUpdateComplaintSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        # Note the use of `get_queryset()` instead of `self.queryset`
        serializer.save(user=self.request.user)


class ComplaintUpdateApiView(generics.RetrieveUpdateAPIView):
    app_pname  = 'complaint-edit'
    queryset = Complaint.objects.filter()
    serializer_class = CreateUpdateComplaintSerializer
    permission_classes = (IsAuthenticated,IsOwnerComplaint,)


class ComplaintRetrieveAPIView(generics.RetrieveAPIView):
    app_pname  = 'complaint-view'
    queryset = Complaint.objects.filter()
    serializer_class = ListComplaintSerializer
    permission_classes = (IsAuthenticated,IsOwnerComplaint,)


class ComplaintDestroyApiView(generics.DestroyAPIView):
    app_pname  = 'complaint-remove'
    queryset = Complaint.objects.filter()
    serializer_class = CreateUpdateComplaintSerializer
    permission_classes = (IsAuthenticated,IsOwnerComplaint,)


class NoteListApiView(generics.ListAPIView):
    # queryset = Note.objects.filter()
    app_pname  = 'note-list'
    serializer_class = ListNoteSerializer
    permission_classes = (IsAuthenticated,IsCustomer,)
    def get_queryset(self):
        user = self.request.user
        notes = Note.objects.filter(user=user).order_by('-date')
        search_key = self.request.query_params.get('search_key', '')
        start_date = self.request.query_params.get('start_date', '')
        end_date = self.request.query_params.get('end_date', '')
        if search_key:
            notes = notes.filter(Q(title__icontains=search_key)|Q(content__icontains=search_key))
        if start_date:
            notes = notes.filter(date__gte=start_date)
        if end_date:
            notes = notes.filter(date__lte=end_date)
        return notes

class NoteCreateApiView(generics.CreateAPIView):
    app_pname  = 'note-create'
    queryset = Note.objects.filter()
    serializer_class = CreateUpdateNoteSerializer
    permission_classes = (IsAuthenticated,)

    def perform_create(self, serializer):
        # Note the use of `get_queryset()` instead of `self.queryset`
        serializer.save(user=self.request.user)
        return


class NoteUpdateApiView(generics.RetrieveUpdateAPIView):
    app_pname  = 'note-edit'
    queryset = Note.objects.filter()
    serializer_class = CreateUpdateNoteSerializer
    permission_classes = (IsAuthenticated,IsOwnerGeneral,)


class NoteRetrieveAPIView(generics.RetrieveAPIView):
    app_pname  = 'note-view'
    queryset = Note.objects.filter()
    serializer_class = ListNoteSerializer
    permission_classes = (IsAuthenticated,IsOwnerGeneral,)


class NoteDestroyApiView(generics.DestroyAPIView):
    app_pname  = 'note-remove'
    queryset = Note.objects.filter()
    serializer_class = CreateUpdateNoteSerializer
    permission_classes = (IsAuthenticated,IsOwnerGeneral,)

    # def perform_create(self, serializer):
    #     Note the use of `get_queryset()` instead of `self.queryset`
        # serializer.save(user=self.request.user)



class PlanListApiView(APIView):
    # queryset = Complaint.objects.filter()
    app_pname  = 'reminder-list'
    permission_classes = (IsAuthenticated,IsEmployee)
    def get(self, request, *args, **kwargs):
        confirm_date = datetime.strptime('Jun 1 2018  1:33PM', '%b %d %Y %I:%M%p')
        now = datetime.now()
        day = get_day('SEP 7 2018')
        week = get_week(now - confirm_date)
        user = self.request.user
        week_list = [1, 2, 3, 4]
        _html = ''
        team_field = request.GET.get('team',0)
        if team_field:
            team_work_day_i = 0
            team = Team.objects.filter(id=team_field).first()
            _team_name = team.name
            for team_work_day in team.get_team_work_days():
                # team_get_plans_w_d_customer_list = []
                team_work_day_i+=1
                week_list_i = 0
                # _html = '{0}{1}'.format(_html,'')
                for week_item in week_list:
                    _button = ""
                    week_list_i += 1
                    team_get_plans_w_d_customer_list = []
                    # print("team_get_plans_w_d_customer_list={}".format(team_get_plans_w_d_customer_list))
                    _html = '{0}{1}'.format(_html,
                                              render_to_string('work/include/teams/plan/_week_part.html',
                                                                        {
                                                                            'week_item': week_item,
                                                                            'team_work_day': team_work_day,
                                                                        }))
                    team_get_plans_w_d = team_work_day.get_plans_w_d(week_item,team_work_day.day.id)
                    loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by('order_index')
                    team_get_plans_w_d_customer_i = 0
                    print("#################### loc_oreders = {} ".format(loc_oreders))
                    for team_get_plans_w_d_customer in loc_oreders:
                        team_get_plans_w_d_customer_list.append(team_get_plans_w_d_customer.customer_id)
                        team_get_plans_w_d_customer_i +=1
                        _html = '{0}{1}'.format(_html,
                                                  render_to_string('work/include/teams/plan/_team_plans.html',
                                                                            {
                                                                                'team_work_day_i': team_work_day_i,
                                                                                'profile_type': profile.usertype,
                                                                                'week_list_i': week_list_i,
                                                                                'team_get_plans_w_d_customer_i': team_get_plans_w_d_customer_i,
                                                                                'team_get_plans_w_d': team_get_plans_w_d,
                                                                                'team_get_plans_w_d_get_customer': team_get_plans_w_d_customer,
                                                                            }))
                    if team_get_plans_w_d_customer_i > 0:
                        _button = '{0}'.format(render_to_string('work/include/teams/plan/_team_plans_button.html',
                                                                        {
                                                                            'team_get_plans_w_d_customer_list': team_get_plans_w_d_customer_list,
                                                                            # 'team_get_plans_w_d_customer_i': team_get_plans_w_d_customer_i,
                                                                        }))
                    # _html = '{0}{1}'.format( _html, '</ul></div>')
                    team_get_plans_w_d_customer_list = []
                    _html = '{0}{1}{2}'.format( _html,_button, '</ul></div></div>')
                message_code = 1
                # _html = '{0}{1}'.format(_html, '</div>')

        return Response({})