from rest_framework import permissions
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import BasePermission,SAFE_METHODS

from work.models import Complaint


class IsOwnerComplaint(BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    message =   'You must be owner bla '
    my_safe_method = ['PUT','GET',"DELETE"]

    def has_permission(self, request, view):
        complaint = get_object_or_404(Complaint,pk=view.kwargs['pk'])
        if request.method in self.my_safe_method and complaint.user.usertype == 3:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS and obj.user == request.user:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.user == request.user




class IsOwnerGeneral(BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """
    message =   'You must be owner bla '
    my_safe_method = ['PUT','GET',"DELETE"]

    def has_permission(self, request, view):
        if request.method in self.my_safe_method:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in SAFE_METHODS and obj.get_owner() == request.user:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.get_owner() == request.user




