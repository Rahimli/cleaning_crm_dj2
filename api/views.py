from django.db.models import Q
from django.urls import reverse
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from api.serializers import *
from rest_framework import generics, permissions
from rest_framework.permissions import IsAdminUser,IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from work.models import *
from api import permissions as general_permissions
User = get_user_model()




class ApiRoot(APIView):
    """Create/Get Chat session messages."""

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        context = {}
        user = request.user
        if user.usertype == 1 or user.usertype == 4:
            pass
        elif user.usertype == 2:

            employee_obj = get_object_or_404(Employee, user=self.request.user)
            team_obj = Team.objects.filter(Q(employees__user=self.request.user)).first()
            team_detail = {}
            if team_obj:
                team_detail = team_obj.to_json()
            context.update({
                'team_details':team_detail,
                'details':team_detail,
                'this_day_plan': reverse('employee-api:employee-this-date-plan',kwargs={'day_week_slug':'day'}, request=request),
                'this_week_plan': reverse('employee-api:employee-this-date-plan',kwargs ={'day_week_slug':'week'}, request=request)
            })
        elif user.usertype == 3:
            context.update({
                'complaints': reverse('customer-api:complaints-list', request=request, format=format)
            })
        return Response(context)