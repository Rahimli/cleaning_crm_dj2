import copy
import datetime
from django.contrib.auth import get_user_model
from django.db.models import Q
from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from api import permissions as general_permissions
from api.functions import get_this_week_dates
from chat.models import ChatSession
from general.functions import check_permission_for_admins
from work.models import Team, Employee, TeamWorkDay, PlanTeamWorkDate, CustomerOrder

User = get_user_model()

class EmployeeDayOrWeekTaskView(APIView):
    """Create/Get Chat session messages."""

    permission_classes = (permissions.IsAuthenticated,general_permissions.IsEmployee)

    def get(self, request, *args, **kwargs):
        """return all messages in a chat session."""
        day_week_slug = kwargs['day_week_slug']
        year = kwargs['year']
        month = kwargs['month']
        day = kwargs['day']
        date = '{}-{}-{}'.format(year,month,day)
        date_time_str = '2018-06-29 08:15:27.243860'
        date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d')
        print("date_time_obj={}".format(date_time_obj.date()))
        print("date={}".format(date))
        # date
        # date = kwargs['uri']

        employee_obj = get_object_or_404(Employee, user=self.request.user)
        team_obj = Team.objects.filter(Q(employees__user=self.request.user)).first()

        # chat_session = ChatSession.objects.get(uri=uri)
        team_detail = {}
        if day_week_slug == 'day':
            locs = []

            if team_obj:
                team_get_plans_w_d = PlanTeamWorkDate.objects.filter(plan_team_work__team=team_obj,date=date).first()
                if team_get_plans_w_d:
                    loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by('order_index')
                    team_get_plans_w_d_customer_i = 0
                    for loc_oreder_item in loc_oreders:
                        loc_oreder_item_customer = loc_oreder_item.customer
                        team_get_plans_w_d_customer_i += 1
                        locs.append({
                            "customer_name":loc_oreder_item_customer.get_customer_name(),
                            "customer_address":loc_oreder_item_customer.address,
                            "customer_minute":loc_oreder_item_customer.address,
                            "customer_detail":loc_oreder_item_customer.address,
                        })
                team_detail = team_obj.to_json()

            # messages = [chat_session_message.to_json()
            #     for chat_session_message in chat_session.messages.all()]

            return Response({
                'team_details': team_detail,
                'locations': locs,
                'date':date_time_obj.date(),
                'week_day':date_time_obj.isoweekday(),
            })
        elif day_week_slug == 'week':
            team_detail = {}
            full_week = []
            get_this_week_dates_l = get_this_week_dates(date)
            if team_obj:
                week_day_i = 0
                for get_this_week_dates_l_item in get_this_week_dates_l:
                    week_day_i += 1
                    locs = []
                    team_get_plans_w_d = PlanTeamWorkDate.objects.filter(plan_team_work__team=team_obj,
                                                                         date=get_this_week_dates_l_item).first()
                    if team_get_plans_w_d:
                        loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by(
                            'order_index')
                        team_get_plans_w_d_customer_i = 0
                        for loc_oreder_item in loc_oreders:
                            loc_oreder_item_customer = loc_oreder_item.customer
                            team_get_plans_w_d_customer_i += 1
                            locs.append({
                                "customer_name": loc_oreder_item_customer.get_customer_name(),
                                "customer_address": loc_oreder_item_customer.address,
                                "customer_minute": loc_oreder_item_customer.address,
                                "customer_detail": loc_oreder_item_customer.address,
                            })
                    team_detail = team_obj.to_json()
                    full_week.append(
                        {'date': get_this_week_dates_l_item, 'week_day': week_day_i, 'locations': copy.deepcopy(locs)})
            # messages = [chat_session_message.to_json()
            #     for chat_session_message in chat_session.messages.all()]

            return Response({
                'team_details': team_detail,
                'full_week': full_week,
            })





class EmployeeThisDayOrWeekTaskView(APIView):
    """Create/Get Chat session messages."""

    permission_classes = (permissions.IsAuthenticated,general_permissions.IsEmployee)

    def get(self, request, *args, **kwargs):
        """return all messages in a chat session."""
        day_week_slug = kwargs['day_week_slug']
        date_now = datetime.datetime.now()
        date = '{}-{}-{}'.format(date_now.year,date_now.month,date_now.day)
        date_time_str = '2018-06-29 08:15:27.243860'
        date_time_obj = datetime.datetime.strptime(date, '%Y-%m-%d')
        print("date_time_obj={}".format(date_time_obj.date()))
        print("date={}".format(date))
        # date
        # date = kwargs['uri']

        employee_obj = get_object_or_404(Employee, user=self.request.user)
        team_obj = Team.objects.filter(Q(employees__user=self.request.user)).first()

        # chat_session = ChatSession.objects.get(uri=uri)
        team_detail = {}
        if day_week_slug == 'day':
            locs = []

            if team_obj:
                team_get_plans_w_d = PlanTeamWorkDate.objects.filter(plan_team_work__team=team_obj,date=date).first()
                if team_get_plans_w_d:
                    loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by('order_index')
                    team_get_plans_w_d_customer_i = 0
                    for loc_oreder_item in loc_oreders:
                        loc_oreder_item_customer = loc_oreder_item.customer
                        team_get_plans_w_d_customer_i += 1
                        locs.append({
                            "customer_name":loc_oreder_item_customer.get_customer_name(),
                            "customer_address":loc_oreder_item_customer.address,
                            "customer_minute":loc_oreder_item_customer.address,
                            "customer_detail":loc_oreder_item_customer.address,
                        })
                team_detail = team_obj.to_json()

            # messages = [chat_session_message.to_json()
            #     for chat_session_message in chat_session.messages.all()]

            return Response({
                'team_details': team_detail,
                'locations': locs,
                'date':date_time_obj.date(),
                'week_day':date_time_obj.isoweekday(),
            })
        elif day_week_slug == 'week':
            team_detail = {}
            full_week = []
            get_this_week_dates_l = get_this_week_dates(date)
            if team_obj:
                week_day_i = 0
                for get_this_week_dates_l_item in get_this_week_dates_l:
                    week_day_i += 1
                    locs = []
                    team_get_plans_w_d = PlanTeamWorkDate.objects.filter(plan_team_work__team=team_obj,
                                                                         date=get_this_week_dates_l_item).first()
                    if team_get_plans_w_d:
                        loc_oreders = CustomerOrder.objects.filter(plan_team_work=team_get_plans_w_d).order_by(
                            'order_index')
                        team_get_plans_w_d_customer_i = 0
                        for loc_oreder_item in loc_oreders:
                            loc_oreder_item_customer = loc_oreder_item.customer
                            team_get_plans_w_d_customer_i += 1
                            locs.append({
                                "customer_name": loc_oreder_item_customer.get_customer_name(),
                                "customer_address": loc_oreder_item_customer.address,
                                "customer_minute": loc_oreder_item_customer.address,
                                "customer_detail": loc_oreder_item_customer.address,
                            })
                    team_detail = team_obj.to_json()
                    full_week.append(
                        {'date': get_this_week_dates_l_item, 'week_day': week_day_i, 'locations': copy.deepcopy(locs)})
            # messages = [chat_session_message.to_json()
            #     for chat_session_message in chat_session.messages.all()]

            return Response({
                'team_details': team_detail,
                'full_week': full_week,
            })


