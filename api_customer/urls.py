# from django.conf.urls import url
# from django.conf.urls.i18n import i18n_patterns
# from .views import *
from django.urls import path

from api_customer.views import ComplaintCreateApiView, ComplaintListApiView, ComplaintUpdateApiView, \
    ComplaintDestroyApiView, ComplaintRetrieveAPIView, NoteListApiView, NoteCreateApiView, NoteUpdateApiView, \
    NoteRetrieveAPIView, NoteDestroyApiView, ComplaintApiView

app_name = 'api_customer'
urlpatterns = [
    path('complaints/list/', ComplaintListApiView.as_view(), name='complaints-list'),
    path('complaints/view1/<int:pk>/', ComplaintApiView.as_view(), name='complaints-2-list'),
    path('complaints/write/', ComplaintCreateApiView.as_view(), name='complaint-add'),
    path('complaints/edit/<int:pk>/', ComplaintUpdateApiView.as_view(), name='complaint-edit'),
    path('complaints/view/<int:pk>/', ComplaintRetrieveAPIView.as_view(), name='complaint-view'),
    path('complaints/remove/<int:pk>/',ComplaintDestroyApiView.as_view(), name='complaint-remove'),

    path('notes/list/', NoteListApiView.as_view(), name='notes-list'),
    path('notes/write/', NoteCreateApiView.as_view(), name='note-add'),
    path('notes/edit/<int:pk>/', NoteUpdateApiView.as_view(), name='note-edit'),
    path('notes/view/<int:pk>/', NoteRetrieveAPIView.as_view(), name='note-view'),
    path('notes/remove/<int:pk>/',NoteDestroyApiView.as_view(), name='note-remove'),
	# path('logout/$', log_out, name='logout'),

]
