from django.contrib import admin

# Register your models here.
from work.models import *

admin.site.register(Customer)

admin.site.register(Employee)
admin.site.register(Subcontractor)
admin.site.register(Complaint)
admin.site.register(Team)

admin.site.register(Task)
admin.site.register(TaskDate)
admin.site.register(TaskCustomDay)
# admin.site.register(PlanTeamWork)
admin.site.register(PlanTeamWorkDate)

admin.site.register(Snippet)
admin.site.register(Reminder)


admin.site.register(TeamWorkDay)
admin.site.register(WorkDay)
admin.site.register(PlanTeamWork)
admin.site.register(CustomerOrder)
admin.site.register(ConfirmedContract)