from django.shortcuts import render
import hashlib
import random

from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.shortcuts import render

# Create your views here.
from django.urls import reverse
from django.utils.datetime_safe import datetime
from django.utils.translation import ugettext as _
from django.core.paginator import PageNotAnInteger, EmptyPage, Paginator
# from flynsarmy_paginator.paginator import FlynsarmyPaginator
from django.template.loader import render_to_string
from django.contrib.auth import authenticate, login, get_user_model, logout

from base_user.forms import LoginForm, UserRegistrationBaseView

GUser = get_user_model()

def base(req=None):
    # company_information = CompanyInformation.objects.filter(active=True).order_by('-date').first()
    # settings_obj = Settings.objects.filter().order_by('-date').first()
    data = {
        'now':datetime.now(),
        # 'base_company_information':company_information,
        # 'base_settings_obj':settings_obj,
    }
    return data

# Create your views here.
def log_out(request):
    if request.user.is_authenticated:
        logout(request)
    # next_url = request.GET.get('next_url')
    # if next_url:
    #     pass
    # else:
    next_url = reverse('base-user:login')
    return HttpResponseRedirect(next_url)


def log_in(request):
    login_form = LoginForm(request.POST or None)
    template_name = 'home/nr-index.html'
    context = base(req=request)
    context['login_form'] = login_form
    next_url = request.GET.get('next_url')
    context['next_url'] = next_url
    # return HttpResponse(next_url)
    message_login = ''
    if request.method == 'POST':
        if login_form.is_valid():
            clean_data = login_form.cleaned_data
            email = clean_data.get('lemail')
            password = clean_data.get('lpassword')
            remember_me = clean_data.get('remember_me')

            # if remember_me:
            #     remember_me = True
            # else:
            #     remember_me = False

            print('valid')
            try:
                user_email = GUser.objects.get(email=email)
                print("user_email={}".format(user_email.username))
                a_user = auth.authenticate(username=user_email.username,password=password)
                if a_user is not None:
                    if a_user.is_active:
                        print("user.is_active")
                        auth.login(request, a_user)
                        # return HttpResponse(next_url)
                        if next_url =='None' or not next_url:
                            next_url = reverse('home:dashboard')
                        else:
                            pass
                        # return HttpResponse(next_url)
                        return HttpResponseRedirect(next_url)
                    else:
                        print("user.is_active not ")
                        message_login = _("Please wait for confirmed account")
                # print('try')

            except:
                message_login = _("Email or password is incorrect")


            # else:
            #     # print("user.is_active not")
            #     # print(a_user)
            #     message_login = _("email_or_password_incorrect")

        context['message_login'] = message_login
    response = render(request, 'base-user/login.html', context=context)
    return  response

