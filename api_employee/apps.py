from django.apps import AppConfig


class ApiEmployeeConfig(AppConfig):
    name = 'api_employee'
