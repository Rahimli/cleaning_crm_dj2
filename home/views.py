import copy

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.utils import timezone
from django.utils.datetime_safe import datetime

from general.models import PlanSettings, PlanLog
from work.models import *

GUser = get_user_model()

def base(req=None):
    # company_information = CompanyInformation.objects.filter(active=True).order_by('-date').first()
    # settings_obj = Settings.objects.filter().order_by('-date').first()
    data = {
        'now':datetime.now(),
        # 'base_company_information':company_information,
        # 'base_settings_obj':settings_obj,
    }
    return data

def base_auth(req=None):
    user = req.user
    # profile = get_object_or_404(Profile, user=user)
    # company_information = CompanyInformation.objects.filter(active=True).order_by('-date').first()
    # settings_obj = Settings.objects.filter().order_by('-date').first()
    data = {
        'now':datetime.now(),
        'base_profile':user,
        'base_profile_permissions':user.permissions(),
        'base_plan_log':PlanLog.objects.filter(complated=False,rejcected=False),
        'base_settings_obj':PlanSettings.objects.filter().order_by('-date').first(),
        # 'base_plan_log':PlanLog.objects.filter(complated=False,rejcected=False),
        # 'base_company_information':company_information,
        # 'base_settings_obj':settings_obj,
    }
    return data



from general.task_functions import result1p1 as nm_result1p1
@login_required(login_url='base-user:login')
def dashboard(request):
    profile = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    # return HttpResponse(request.user.is_staff)
    today_reminders = Reminder.objects.filter(user=profile,date_time__day=now.day,date_time__month=now.month,date_time__year=now.year)
    context['today_reminders'] = today_reminders
    if profile.usertype == 1:
        pass
    elif profile.usertype == 2:
        data = get_object_or_404(Employee,user=profile)
        context['data'] = data
    elif profile.usertype == 3:
        data = get_object_or_404(Customer,user=profile)

        context['data'] = data
        context['notes'] = Note.objects.filter(user=profile)[:2]
    # else:
    #     raise Http404
    # return HttpResponse(customers[:20].count())
    # context['employee_count'] = employees.count()

    return render(request, 'home/dashboard.html', context=context)




from general.task_functions import result1p1 as nm_result1p1
@login_required(login_url='base-user:login')
def home_test(request):
    profile = request.user
    # profile = get_object_or_404(Profile, user=user)
    # tourpackeges = None
    now = timezone.now()
    context = base_auth(req=request)
    today_reminders = Reminder.objects.filter(user=profile,date_time__day=now.day,date_time__month=now.month,date_time__year=now.year)
    context['today_reminders'] = today_reminders
    if profile.usertype == 1:
        pass
    elif profile.usertype == 2:
        data = get_object_or_404(Employee,user=profile)
        context['data'] = data
    elif profile.usertype == 3:
        data = get_object_or_404(Customer,user=profile)

        context['data'] = data
        context['notes'] = Note.objects.filter(user=profile)[:5]
    # else:
    #     raise Http404
    # return HttpResponse(customers[:20].count())
    # context['employee_count'] = employees.count()
    day_list = [1,2,3,4,5,6,7]
    p, created = Complaint.objects.get_or_create(
        user=profile,
        name = 'test 1',
        content = 'content test 1',
    )
    # main_loc = Customer.objects.get(our_company=True)
    print("Complaint obj = {}".format(p))
    print("Complaint created = {}".format(created))
    # for day_list_item in day_list:
    #     hour_task_custom_days = TaskCustomDay.objects.filter(task_day=day_list_item).exclude(task__customer__minute=0).filter(task__customer__standart_task=False).values('task__customer_id')
    #     daily_exc_list = [y.id for y in Customer.objects.exclude(id__in=[x['task__customer_id'] for x in hour_task_custom_days]) ]
    #     for Hours_CHOICES_item in Hours_CHOICES:
    #         if Hours_CHOICES_item[0]:
    #             opt_loc_destinations = nm_result1p1(exc_list=daily_exc_list.remove(main_loc.id), minute=Hours_CHOICES_item[0])
    #             teamworks = PlanTeamWork.objects.filter(week=1,day__day=day_list_item,is_empty=True).filter(minute=Hours_CHOICES_item[0]).order_by('week','day__day')
    #
    #             teamworks_item_i = 0
    #             for teamworks_item in teamworks[:len(opt_loc_destinations)]:
    #                 opt_loc_destinations_i = 0
    #                 ordered_opt_loc_destination_item = opt_loc_destinations[teamworks_item_i]
    #                 # ordered_opt_loc_destination_item = nm_ordered_customers(main_loc.id,opt_loc_destinations[teamworks_item_i])
    #                 for opt_loc_destinations_item in ordered_opt_loc_destination_item[1:-1]:
    #                     new_customer_order = CustomerOrder()
    #                     daily_exc_list.append(opt_loc_destinations_item)
    #                     new_customer_order.plan_team_work = copy.deepcopy(teamworks_item)
    #                     new_customer_order.order_index = copy.deepcopy(opt_loc_destinations_i + 1)
    #                     new_customer_order.customer_id = copy.deepcopy(opt_loc_destinations_item)
    #                     new_customer_order.main_process = True
    #                     new_customer_order.save()
    #                     # bulk_customer_order.append(new_customer_order)
    #                     # print('opt_loc_destinations[teamworks_item_i] = {}'.format(opt_loc_destinations[teamworks_item_i]))
    #                     opt_loc_destinations_i += 1
    #                 # # print("task.py 542 - teamworks_item = {}".format(teamworks_item))
    #                 # teamworks_item.difference_minute = teamworks_item.minute - nm_calculate_minute_1times(ordered_opt_loc_destination_item,loc_id,main_customer.id,plan_emp_works_weeks_item.minute)
    #                 if Hours_CHOICES_item[0] - 20 <= ordered_opt_loc_destination_item[-1][0]:
    #                     teamworks_item.is_full = True
    #                 teamworks_item.minute = ordered_opt_loc_destination_item[-1][0]
    #                 teamworks_item.is_empty = False
    #                 teamworks_item.save()
    #                 teamworks_item_i += 1
    #
    #
    #
    #     print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    #     print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
    #     print("hour_task_custom_days = {} - {}".format(day_list_item,[x['task__customer_id'] for x in hour_task_custom_days]))
    import os

    import pdfkit
    # pdfkit.from_url('http://goldnews.az', 'goldnews.pdf')
    # body = '<html><body><h1>Salam</h1><img src="http://127.0.0.1:8090/static/main/dist/assets/images/avatars/default-user.png"/></body></html>'
    # pdfkit.from_string(body, 'images/out23456.pdf') #with --page-size=Legal and --orientation=Landscape

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    print("os.path.join(BASE_DIR, 'media') = {}".format(os.path.join(BASE_DIR)))
    # context['customer_count'] = customers.count()
    # context['last_customers'] = customers.all()[:20]
    # context['last_employees'] = employees.all()[:20]
    # context['tours'] = Tour.objects.filter(active=True)
    return render(request, 'home/dashboard.html', context=context)
