from django.contrib.auth import get_user_model
from django.shortcuts import render

# Create your views here.
from django.utils.datetime_safe import datetime
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView

from api.permissions import *
from api.serializers import UserSerializer, EmployeeSerializer, SubcontractorSerializer
from api_administration.serializers import *
from work.functions import get_week, get_day
from work.models import Reminder, Employee, Subcontractor

User = get_user_model()


class ReminderListApiView(generics.GenericAPIView):
    # queryset = Complaint.objects.filter()
    app_pname  = 'reminder-list'
    serializer_class = ListReminderSerializer
    permission_classes = (IsAuthenticated,IsAdministration,)
    def get_queryset(self):
        user = self.request.user
        return Reminder.objects.filter(user=user)





class PlanListApiView(APIView):
    # queryset = Complaint.objects.filter()
    app_pname  = 'reminder-list'
    serializer_class = ListReminderSerializer
    permission_classes = (IsAuthenticated,IsAdministration,)
    def get(self, request, *args, **kwargs):
        confirm_date = datetime.strptime('Jun 1 2018  1:33PM', '%b %d %Y %I:%M%p')
        now = datetime.now()
        day = get_day('SEP 7 2018')
        week = get_week(now - confirm_date)
        user = self.request.user
        week_list = [1, 2, 3, 4]

        team_field = request.GET.get('team',0)

        return Reminder.objects.filter(user=user)






class UserListApiView(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAdminUser,)

    def list(self, request,**kwargs):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

class EmployeeListApiView(generics.ListAPIView):
    queryset = Employee.objects.filter(deleted=False)
    serializer_class = EmployeeSerializer
    permission_classes = (IsAdminUser,)

    def list(self, request,**kwargs):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = EmployeeSerializer(queryset, many=True)
        return Response(serializer.data)

class EmployeeCreateApiView(generics.CreateAPIView):
    queryset = Employee.objects.filter(deleted=False)
    serializer_class = EmployeeSerializer
    permission_classes = (IsAdminUser,)

    def create(self, request, *args, **kwargs):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = EmployeeSerializer(queryset, many=True)
        return Response(serializer.data)

class SubcontractorListApiView(generics.ListAPIView):
    queryset = Subcontractor.objects.all()
    serializer_class = SubcontractorSerializer
    permission_classes = (IsAdminUser,)

    def list(self, request,**kwargs):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = SubcontractorSerializer(queryset, many=True)
        return Response(serializer.data)

class SubcontractorRetrieveView(generics.RetrieveAPIView):
    queryset = Subcontractor.objects.filter(deleted=False)
    serializer_class = SubcontractorSerializer
    permission_classes = (IsAdminUser,)

class SubcontractorUpdateAPIView(generics.UpdateAPIView):
    queryset = Subcontractor.objects.filter(deleted=False)
    serializer_class = SubcontractorSerializer
    permission_classes = (IsAdminUser,)
    lookup_field = 'pk'
    # lookup_fields = ('account', 'username')


class SubcontractorCreateApiView(generics.CreateAPIView):
    queryset = Subcontractor.objects.filter(deleted=False)
    serializer_class = SubcontractorSerializer
    permission_classes = (IsAdminUser,)

    def create(self, request,**kwargs):
        # Note the use of `get_queryset()` instead of `self.queryset`
        queryset = self.get_queryset()
        serializer = SubcontractorSerializer(queryset, many=True)
        return Response(serializer.data)