
from django.urls import path
from django.conf.urls.i18n import i18n_patterns

from work.ajax_view import get_snippet_ajax
from work.calculate import task_calculate
from .views import *
from .plan_views import *

app_name = 'work'
urlpatterns = [
	path('change/password/', change_password, name='change-password'),

	path('admins/list/', admin_list, name='admin-list'),
    path('admins/add/', admin_add, name='admin-add'),
    path('admins/edit/<int:u_id>/', admin_edit, name='admin-edit'),
    path('admins/remove/<int:u_id>/', admin_remove, name='admin-remove'),
    path('user/profile/show/<int:ec_id>/', user_profile, name='user-profile'),
    # path('customers/remove/<int:c_id>/', customer_remove, name='customer-remove'),

	path('customers/list/<slug:op_slug>/', customers_list, name='customers-list'),
	path('customers/upload-from-excel/form/', customer_upload_from_excel, name='customer-upload-from-excel'),
	path('customers/payments/list/', customers_payment_list, name='customers-payment-list'),
	path('customers/payments/all/list/', all_payment_list, name='all-payments-list'),
    path('customers/add/', customer_add, name='customer-add'),
    path('customers/add-or-edit/card/', customer_add_card, name='customer-add-edit-card'),
    path('customers/edit/<int:c_id>/', customer_edit, name='customer-edit'),
    path('customers/remove/<int:c_id>/', customer_remove, name='customer-remove'),

    path('employees/list/<slug:op_slug>/', employees_list, name='employees-list'),
	path('employees/add/', employee_add, name='employee-add'),
	path('employees/edit/<int:e_id>/', employee_edit, name='employee-edit'),
	path('employees/remove/<int:e_id>/', employee_remove, name='employee-remove'),

	# path('employees/add/', employee_add, name='employee-add'),
    path('teams/list/', teams_list, name='teams-list'),
	path('teams/add/', team_add, name='team-add'),
	path('teams/edit/<int:t_id>/', team_edit, name='team-edit'),
	path('teams/remove/<int:t_id>/', team_remove, name='team-remove'),

    path('subcontractors/list/<slug:op_slug>', subcontractors_list, name='subcontractors-list'),
    path('subcontractors/add/', subcontractors_add, name='subcontractor-add'),
    path('subcontractors/edit/<int:s_id>/', subcontractor_edit, name='subcontractor-edit'),
    path('subcontractors/remove/<int:s_id>/', subcontractor_remove, name='subcontractor-remove'),

    path('complaints/list/', complaints_list, name='complaints-list'),
    path('complaints/write/', complaint_add, name='complaint-add'),
    path('complaints/edit/<int:c_id>/', complaint_edit, name='complaints-edit'),
    path('complaints/remove/<int:c_id>/', complaint_remove, name='complaint-remove'),

    # path('work-plans/list/', work_plan, name='work-plan'),

    path('reminders/list/', reminders_list, name='reminders-list'),
	path('reminders/add/', reminder_add, name='reminder-add'),
	path('reminders/edit/<int:r_id>/', reminder_edit, name='reminder-edit'),
	path('reminders/remove/<int:r_id>/', reminder_remove, name='reminder-remove'),

    path('tasks/list/', tasks_list, name='tasks-list'),
    path('tasks/calculate/', task_calculate, name='tasks-calculate'),
	path('tasks/add/', task_add, name='task-add'),
	path('tasks/edit/<int:t_id>/', task_edit, name='task-edit'),
	path('tasks/remove/<int:t_id>/', task_remove, name='task-remove'),

    path('services/list/', services_list, name='services-list'),
	path('services/add/', service_add, name='service-add'),
	path('services/edit/<int:s_id>/', service_edit, name='service-edit'),
	path('services/remove/<int:s_id>/', service_remove, name='service-remove'),

    path('cleaning-programs/list/', cleaning_programs_list, name='cleaning-programs-list'),
	path('cleaning-programs/add/', cleaning_program_add, name='cleaning-program-add'),
	path('cleaning-programs/edit/<int:c_id>/', cleaning_program_edit, name='cleaning-program-edit'),
	path('cleaning-programs/remove/<int:c_id>/', cleaning_program_remove, name='cleaning-program-remove'),

    path('snippets/list/', snippets_list, name='snippets-list'),
    path('get-snippet/ajax/<slug:f_slug>/', get_snippet_ajax, name='get-snippet-ajax'),
	path('snippets/add/', snippet_add, name='snippet-add'),
	path('snippets/edit/<int:s_id>/', snippet_edit, name='snippet-edit'),
	path('snippets/remove/<int:s_id>/', snippet_remove, name='snippet-remove'),

    path('notes/list/', notes_list, name='notes-list'),
	path('notes/add/', note_add, name='note-add'),
	path('notes/edit/<int:n_id>/', note_edit, name='note-edit'),
	path('notes/view/<int:n_id>/', note_view, name='note-view'),
	path('notes/remove/<int:n_id>/', note_remove, name='note-remove'),

	path('contract/template/', contract_template_list, name='contract-template'),

	path('contract-template/list/', contract_template_list, name='contract-template-list'),
    path('contract-template/add/', contract_template_add, name='contract-template-add'),
    path('contract-template/edit/<int:c_id>/', contract_template_edit, name='contract-template-edit'),
    path('contract-template/view/<int:c_id>/', customer_edit, name='contract-template-view'),
    path('contract-template/remove/<int:c_id>/', customer_remove, name='contract-template-remove'),

    path('contract/confirm/<uuid:tc_id>/', contract_confirm, name='contract-confirm'),
    path('contract/confirm-success/', contract_confirm_success, name='contract-confirm-success'),

	path('calendar/show/', calendar, name='calendar-show'),
	path('calendar/calendar-customer-task-remove/<int:td_id>/', calendar_remove, name='calendar-customer-task-remove'),
	path('economy/chart/', economy, name='economy-chart'),
	path('salary/info/', salary, name='salary-info'),



	path('teams/plans/team/<int:t_id>/', admin_team_plans, name='admin-team-plans'),
	path('teams/plans/list/', team_plans, name='team-plans'),
	path('teams/plans/show/ajax/', team_plan_show_ajax, name='team-plan-show-ajax'),
	path('teams/plans/show/plan-map/ajax/', team_plan_show_map_ajax, name='team-plan-map-ajax'),

	path('d/admin-team-plan-form-generate/ajax/', admin_team_plan_form_generate_ajax, name='admin-team-plan-form-generate-ajax'),
	path('d/admin-team-plan-form-change-customer-position-ajax/ajax/<int:cur_cus_ord_id>/',
		admin_team_plan_form_change_customer_position_ajax, name='admin-change-customer-position-ajax'),
	path('d/admin-team-plan-form-customer-ajax/ajax/<int:e_p_id>/<int:l_id>/', admin_team_plan_form_customer_ajax, name='admin-team-plan-form-customer-ajax'),
]
