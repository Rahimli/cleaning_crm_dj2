import datetime

from base_user.models import UserPermission
from general.models import *
from work.models import *


def check_permission_for_admins(req,page_key):
    user = req.user
    return_value = False
    # print(user)
    if user.usertype == 1 or user.usertype == 4:
        if user.usertype == 1:
            return_value = True
        else:
            try:
                UserPermission.objects.get(user=user,permission=page_key)
                return_value = True
            except:
                pass
    return return_value


import re



def generate_task_and_plan_date():
    task_list = []
    day_after = 0
    pl = PlanLog.objects.filter(complated=True,rejcected=False).exclude(planned_date=None).order_by('-planned_date').first()
    if pl and pl.planned_date:
        date_obj = pl.planned_date
        for pm_item in range(1,29):
            for w_item in range(1,5):
                for d_item in range(1,8):
                    day_after += 1
                    date_obj += datetime.timedelta(days=day_after)
                    ptws = PlanTeamWork.objects.filter(week=w_item,day__day=d_item,is_empty=False)
                    if ptws.exists():
                        for ptw_item in ptws:
                            ptwd_obj = PlanTeamWorkDate.objects.create(plan_team_work=ptw_item,date=date_obj)
                            for ptw_customer in ptw_item.get_customers():
                                if ptw_customer.customer.standart_task == True:
                                    custumer_task = Task.objects.filter(customer=ptw_customer.customer,task_type=2).exclude(date_type_fixed=4).first()
                                    if custumer_task:
                                        TaskDate.objects.create(task=custumer_task,
                                                                plan_teamWork_date=ptwd_obj,
                                                                date=date_obj,
                                                                )
                                else:
                                    task_custom_custumer = TaskCustomDay.objects.filter(
                                        task__customer = ptw_customer.customer,
                                        task__task_type = 2,
                                        task_day=d_item,
                                    ).exclude(task__date_type_fixed=4).first()
                                    if task_custom_custumer:
                                        TaskDate.objects.create(task=task_custom_custumer.task,
                                                                plan_teamWork_date=ptwd_obj,
                                                                task_custom_day=task_custom_custumer,
                                                                date=date_obj,
                                                                )


    return 1


def get_this_week_dates(date_str):
    datetime.datetime.now()
    date_time_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    week_day = date_time_obj.isoweekday()
    date_list = []
    print(week_day)
    while week_day != 1:
        date_time_obj = date_time_obj + datetime.timedelta(days=1)
    print(week_day)



def get_nearest_week_day_date(date_str,week_day):
    date_time_obj = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    add_day = 0
    while date_time_obj.isoweekday() != int(week_day):
        add_day += 1
        date_time_obj = date_time_obj + datetime.timedelta(days=1)
    return date_time_obj.date()






def stripTags(text,list):
    # scripts = re.compile(r'<script.*?/script>')
    scripts = re.compile(r'<(script).*?</\1>(?s)')
    css = re.compile(r'<style.*?/style>')
    tags = re.compile(r'<.*?>')

    if 'js' in list:
        text = scripts.sub('', text)
    if 'css' in list:
        text = css.sub('', text)
    if 'all' in list:
        text = tags.sub('', text)
    return text