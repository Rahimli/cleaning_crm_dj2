# Generated by Django 2.0 on 2018-10-08 14:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('work', '0017_taskdate'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='taskdate',
            unique_together={('plan_teamWork_date', 'date')},
        ),
    ]
