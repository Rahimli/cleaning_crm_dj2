from django.db import models
from django.utils.translation import ugettext as _
# Create your models here.
from payment.common import PAYMENT_TYPE


class CustomerStripe(models.Model):
    customer = models.OneToOneField('work.Customer',on_delete=models.CASCADE)
    stripe_id = models.CharField(max_length=255,blank=True,null=True)
    customer_code = models.CharField(max_length=255,blank=True,null=True)
    card_number_last = models.CharField(max_length=255,blank=True,null=True)
    # amount = models.DecimalField(max_digits=19,decimal_places=3,default=0)
    # paid = models.BooleanField(default=False)
    # currency = models.CharField(max_length=50,default='usd')
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}-{}".format(self.stripe_id,self.customer_code)


class CustomerPayment(models.Model):
    type = models.CharField(max_length=100,choices=PAYMENT_TYPE)
    customer = models.ForeignKey('work.Customer',blank=True,null=True,on_delete=models.SET_NULL)
    amount = models.DecimalField(max_digits=19,decimal_places=3)
    paid = models.BooleanField(default=False)
    currency = models.CharField(max_length=50)
    datetime = models.DateTimeField()
    pay_datetime = models.DateTimeField()
    date = models.DateField(auto_now_add=True)
