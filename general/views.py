import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import render

from django.utils.translation import ugettext_lazy as _
from geoposition import Geoposition
from django.utils.translation import ugettext as _
# Create your views here.
from general.models import PlanSettings, PlanLog
from home.views import base_auth


@login_required(login_url='base-user:login')
def plan_prepare_ajax(request,op_slug):
    from general.tasks import main_result_prepare_plan_new_plan , sub_main_result_prepare_plan_new_plan
    context = base_auth(req=request)
    user = request.user
    if user.usertype == 1:
        if request.method == 'GET' and request.is_ajax():
            message_code = 0
            message = ""
            plan_obj = PlanLog.objects
            settings_obj = PlanSettings.objects
            if plan_obj.filter(complated=False,rejcected=False):
                message = _("Please wait preparing plans........")
            else:
                if op_slug == 'main':
                    if settings_obj.filter(main_plan=True):
                        message_code = 1
                        main_result_prepare_plan_new_plan.delay()
                        message = _("Started plan preparing. Please wait........")
                    else:
                        message = _("Main plan is disable ........")
                if op_slug == 'sub-main':
                    if settings_obj.filter(sub_plan=True):
                        message_code = 1
                        sub_main_result_prepare_plan_new_plan.delay()
                        message = _("Started plan preparing. Please wait........")
                        settings_obj_f = settings_obj.first()
                        settings_obj_f.sub_plan = False
                        print("sub_plan = {}".format(settings_obj_f.sub_plan))
                        settings_obj_f.save()
                    else:
                        message = _("SubMain plan is disable ........")
            data = {'message_code': message_code, 'message': message}
            # data = {'message_code': message_code, 'message': message}
            return HttpResponse(json.dumps(data, ensure_ascii=False), content_type="application/json")
        else:
            raise Http404
    else:
        raise Http404

