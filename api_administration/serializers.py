from rest_framework import serializers

from work.models import Reminder


class ListReminderSerializer(serializers.ModelSerializer):
    edit_url         = serializers.SerializerMethodField(read_only=True)
    view_url         = serializers.SerializerMethodField(read_only=True)
    destroy_url         = serializers.SerializerMethodField(read_only=True)
    class Meta:
        model = Reminder
        fields = ('edit_url','view_url','destroy_url','user','name', 'date_time','date',)
    def get_edit_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[0]
    def get_view_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[1]
    def get_destroy_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_crud_url(request=request)[2]





