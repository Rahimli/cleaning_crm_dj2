from django.contrib import admin

# Register your models here.
from payment.models import *

admin.site.register(CustomerStripe)
admin.site.register(CustomerPayment)