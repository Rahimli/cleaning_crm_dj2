import uuid

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import signals
from tinymce.models import HTMLField
# Create your models here.fore
from api.serializers import EmployeeSerializer
from core.functions import files_path_and_rename
from django.utils.translation import ugettext_lazy as _
from geoposition.fields import GeopositionField

from payment.models import CustomerStripe
from work import fields as gen_field
from work.common import *
from general.tasks import create_customer_distance_customers
from rest_framework.reverse import reverse as api_reverse



class ExcelDocument(models.Model):
   excelfile = models.FileField(upload_to='documents/excel')
   date = models.DateTimeField(auto_now_add=True)


customer_type_choices = (
    (1,_('Private')),
    (2,_('Business')),
)
customer_type_search_choices = (
    ('',_('Customer type')),
    (1,_('Private')),
    (2,_('Business')),
)

class Customer(models.Model):
    our_company = models.BooleanField(default=False)
    user = models.OneToOneField(settings.AUTH_USER_MODEL,editable=False,on_delete=models.CASCADE)
    customer_type = models.PositiveIntegerField(choices=customer_type_choices)
    # ---------------------------------------------------------------------------------------------------
    full_name = models.CharField(max_length=255,blank=True,null=True)

    company_name = models.CharField(max_length=255,blank=True,null=True)
    contact_person = models.CharField(max_length=255,blank=True,null=True)
    cvr_number = models.PositiveIntegerField(blank=True,null=True)
    # ---------------------------------------------------------------------------------------------------
    stripe_id = models.CharField(max_length=255,blank=True,null=True)
    phonenumber = models.PositiveIntegerField()
    # email = models.EmailField()
    # address = models.CharField(max_length=255)
    street_name = models.CharField(max_length=255)
    zip_code = models.PositiveIntegerField()
    city = models.CharField(max_length=255)
    start_date = models.DateField()
    square_meters = models.DecimalField(max_digits=19,decimal_places=3)
    amount_of_bathrooms = models.DecimalField(max_digits=19,decimal_places=3)
    amount_of_kitchens = models.DecimalField(max_digits=19,decimal_places=3)
    amount_of_floors = models.DecimalField(max_digits=19,decimal_places=3)
    auto_generated_password = models.CharField(max_length=255,blank=True,null=True,editable=False)
    minute = models.DecimalField(max_digits=10,decimal_places=2,default=0.0)
    work_times = gen_field.IntegerRangeField(min_value=0, max_value=4,default=0)
    position = GeopositionField()
    address = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    plan_start_date = models.DateField(blank=True,null=True,editable=False)
    standart_task = models.BooleanField(default=True)
    deleted = models.BooleanField(default=False)
    def __str__(self):
        return_val = ''
        if int(self.customer_type) == 1:
            return_val = self.full_name
        elif int(self.customer_type) == 2:
            return_val = self.company_name
        return return_val
    def get_customer_name(self):
        return_val = ''
        if int(self.customer_type) == 1:
            return_val = self.full_name
        elif int(self.customer_type) == 2:
            return_val = self.company_name
        return return_val
    def get_customer_type(self):
        return_val = ''
        for customer_type_choices_item in customer_type_choices:
            if customer_type_choices_item[0] == self.customer_type:
                return_val = customer_type_choices_item[1]
                break
        return return_val
    def get_customer_day_minute(self,week_day):
        return_val = self.minute
        if self.standart_task is False and week_day > 0:
            task_custom_day_obj = TaskCustomDay.objects.filter(task__customer=self,task__date_type_fixed=4,task_day=week_day)
            if task_custom_day_obj.exists():
                return_val = task_custom_day_obj.first().time
        return return_val

    def get_my_card(self):
        return CustomerStripe.objects.filter(customer=self).first()
    # def get_customer_day_minute(self,date):
    #     return_val = self.minute
    #     if self.standart_task is False and week_day > 0:
    #         planTeamWorkDate = PlanTeamWorkDate.objects.filter(date=date).filter(plan_team_work=)
    #         task_custom_day_obj = TaskCustomDay.objects.filter(task__customer=self,task__date_type_fixed=4,task_day=week_day)
    #         if task_custom_day_obj.exists():
    #             return_val = task_custom_day_obj.first().time
    #     return return_val

def customer_post_save(sender, instance,created, signal, *args, **kwargs):
    # if created:
        # if instance.image:
        #     pass
            # image_thumb_resize_general(instance.image.url,41,41).delay()
            # image_thumb_resize_general.delay(instance.image.url,41,41)
    # if instance.status:
    if created:
        # print(signal)
        # Send verification email
        create_customer_distance_customers.delay(instance.id)
    # if created:



signals.post_save.connect(customer_post_save, sender=Customer)
#


employee_type_search_choices = (
    ('','Employee Type'),
    (1,'Employee'),
    (2,'Subcontractor'),
)
employment_type_search_choices = (
    ('','Employment type'),
    (1,'Permanent'),
    (2,'Hourly rate'),
)

employee_type_choices = (
    (1,'Employee'),
    (2,'Subcontractor'),
)
employment_type_choices = (
    (1,'Permanent'),
    (2,'Hourly rate'),
)

class Employee(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    employee_type = models.PositiveIntegerField(choices=employee_type_choices)
    # ---------------------------------------------------------------------------------------------------
    subcontractor = models.ForeignKey('Subcontractor',related_name='+',blank=True,null=True,on_delete=models.CASCADE)
    # ---------------------------------------------------------------------------------------------------
    full_name = models.CharField(max_length=255,blank=True,null=True)
    cpr_number = models.PositiveIntegerField()
    birth_date = models.DateField()
    phonenumber = models.PositiveIntegerField()
    auto_generated_password = models.CharField(max_length=255,blank=True,null=True,editable=False)
    # address = models.CharField(max_length=255)
    street_name = models.CharField(max_length=255)
    zip_code = models.PositiveIntegerField()
    city = models.CharField(max_length=255)

    start_date = models.DateField()
    employment_type = models.PositiveIntegerField(choices=employment_type_choices)
    amount_of_hours = models.DecimalField(max_digits=15,decimal_places=2,blank=True,null=True)
    hourly_rate = models.DecimalField(max_digits=15,decimal_places=2,blank=True,null=True)
    # Contract
    contract_file = models.FileField(blank=True,null=True,upload_to=files_path_and_rename)
    social_security_card = models.ImageField(blank=True,null=True,upload_to=files_path_and_rename)
    picture = models.ImageField(blank=True,null=True,upload_to=files_path_and_rename)
    date = models.DateTimeField(auto_now_add=True)

    deleted = models.BooleanField(default=False)
    def __str__(self):
        return self.full_name
    def get_employee_type(self):
        return_val = ''
        for employee_type_choices_item in employee_type_choices:
            if employee_type_choices_item[0] == self.employee_type:
                return_val = employee_type_choices_item[1]
                break
        return return_val
    def get_employment_type(self):
        return_val = ''
        for employment_type_choices_item in employment_type_choices:
            if employment_type_choices_item[0] == self.employee_type:
                return_val = employment_type_choices_item[1]
                break
        return return_val


class ConfirmedContract(models.Model):
    uid_key = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    employee = models.ForeignKey('Employee',blank=True,null=True,on_delete=models.CASCADE)
    customer = models.ForeignKey('Customer',blank=True,null=True,on_delete=models.CASCADE)
    contract_url = models.CharField(max_length=500)
    confirm = models.BooleanField(default=False)
    expiry_date = models.DateTimeField(blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True)



class Subcontractor(models.Model):
    company_name = models.CharField(max_length=255)
    cvr_number = models.IntegerField()
    # Address
    street_name = models.CharField(max_length=255)
    zip_code = models.PositiveIntegerField()
    city = models.CharField(max_length=255)

    # Contact person
    full_name = models.CharField(max_length=255)
    phonenumber = models.PositiveIntegerField()
    email = models.EmailField(max_length=255)

    date = models.DateTimeField(auto_now_add=True)

    deleted = models.BooleanField(default=False,editable=False)
    def __str__(self):
        return self.company_name
    def get_api_edit_url(self, request=None):
        return api_reverse("api:subcontractor-edit", kwargs={'pk': self.pk}, request=request)



class Complaint(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    content = HTMLField()
    is_read = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
    def get_owner(self):
        return self.user
    def get_api_crud_url(self, request=None):
        return [
            api_reverse("customer-api:complaint-edit", kwargs={'pk': self.pk}, request=request),
            api_reverse("customer-api:complaint-view", kwargs={'pk': self.pk}, request=request),
            api_reverse("customer-api:complaint-remove", kwargs={'pk': self.pk}, request=request),
                ]

class Reminder(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    date_time = models.DateTimeField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name

    def get_api_crud_url(self, request=None):
        return [
            api_reverse("administration-api:reminder-edit", kwargs={'pk': self.pk}, request=request),
            api_reverse("administration-api:reminder-view", kwargs={'pk': self.pk}, request=request),
            api_reverse("administration-api:reminder-remove", kwargs={'pk': self.pk}, request=request),
                ]

week_days_choices = (
         (1,'Monday'),
         (2,'Tuesday'),
         (3,'Wednesday'),
         (4,'Thursday'),
         (5,'Friday'),
         (6,'Saturday'),
         (7,'Sunday'),
     )

task_type_fixed_choices = (
     (1,'Every week'),
     (2,'Every other week'),
     (3,'Once every month'),
     (4,'Custom'),
)
task_type_choices = (
    (1,'Once'),
    (2,'Fixed'),
)


class TaskCustomDay(models.Model):
    task = models.ForeignKey('Task',on_delete=models.CASCADE,related_name='+')
    task_day = gen_field.IntegerRangeField(min_value=1,max_value=7,choices=week_days_choices)
    square_meter_cleaning_program = models.ForeignKey('CleaningProgram',on_delete=models.CASCADE,blank=True,null=True,related_name='+')
    fixed_cleaning_programs = models.ManyToManyField('CleaningProgram',blank=True,null=True)
    time = models.DecimalField(max_digits=10,decimal_places=2,default=0)
    price = models.DecimalField(max_digits=19,decimal_places=2,default=0)
    desc = models.TextField(blank=True,null=True)

    class Meta:
        unique_together = ("task", "task_day")


class Task(models.Model):
    task_type = models.PositiveIntegerField(choices=task_type_choices)

    name = models.CharField(max_length=255)
    date_type_fixed = models.PositiveIntegerField(choices=task_type_fixed_choices,blank=True,null=True)
    # date_type_custom = models.PositiveIntegerField(choices=week_days_choices,blank=True,null=True)
    square_meter_cleaning_program = models.ForeignKey('CleaningProgram',on_delete=models.CASCADE,blank=True,null=True,related_name='+')
    fixed_cleaning_programs = models.ManyToManyField('CleaningProgram',blank=True,null=True)

    customer = models.ForeignKey('Customer',on_delete=models.CASCADE)

    total_time = models.CharField(max_length=255)
    total_price = models.CharField(max_length=255)
    description = models.TextField()

    draft = models.BooleanField(default=False)
    date  = models.DateTimeField(auto_now_add=True)


    # class Meta:
    #     unique_together = (("task_type", "customer"),)
    def clean(self):
        # Don't allow draft entries to have a pub_date.
        if self.pk:
            if Task.objects.filter(customer=self.customer,task_type=self.task_type).exclude(id=self.pk):
                raise ValidationError(_('This customer\'s have fixed task' ))
        else:
            if Task.objects.filter(customer=self.customer,task_type=self.task_type):
                raise ValidationError(_('This customer\'s have fixed task'))

    def __str__(self):
        return self.name
    def get_custom_days(self):
        return TaskCustomDay.objects.filter(task=self)
    def get_task_type(self):
        return_val = ''
        for task_type_choices_item in task_type_choices:
            if task_type_choices_item[0] == self.task_type:
                return_val = task_type_choices_item[1]
                break
        return return_val
    def get_date_type_fixed(self):
        return_val = ''
        for task_type_fixed_choices_item in task_type_fixed_choices:
            if task_type_fixed_choices_item[0] == self.date_type_fixed:
                return_val = task_type_fixed_choices_item[1]
                break
        return return_val
    def get_date_type_custom(self):
        return_val = ''
        # for week_days_choices_item in week_days_choices:
        #     if week_days_choices_item[0] == self.task_type:
        #         return_val = week_days_choices_item[1]
        #         break
        return return_val

def task_save(sender, instance,created, signal, *args, **kwargs):
    if int(instance.task_type) == 2:
        print('instance.customer={}'.format(instance.customer))
        if int(instance.date_type_fixed) == 4:
            instance.customer.standart_task = False
        if int(instance.date_type_fixed) in [1,4]:
            instance.customer.work_times = 1
        elif int(instance.date_type_fixed) == 2:
            instance.customer.work_times = 2
        elif int(instance.date_type_fixed) == 3:
            instance.customer.work_times = 4
        instance.customer.save()
    if created:
        print(created)
    #     Send verification email
        # create_customer_distance_customers.delay(instance.id)
    # if created:



signals.post_save.connect(task_save, sender=Task)



class CleaningProgram(models.Model):
    name  = models.CharField(max_length=255)
    assign_service = models.ForeignKey('Service',on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name


service_type_choices = (
    (1,'Fixed qty. price'),
    (2,'Square meter price'),
)

service_time_type_choices = (
    (1,'Fixed time'),
    (2,'Time per square meter'),
)

service_type_search_choices = (
    ('','Type'),
    (1,'Fixed qty. price'),
    (2,'Square meter price'),
)

service_time_search_type_choices = (
    ('','Time time'),
    (1,'Fixed time'),
    (2,'Time per square meter'),
)
class Service(models.Model):
    name = models.CharField(max_length=255)
    type = models.PositiveIntegerField(choices=service_type_choices)
    price = models.DecimalField(max_digits=19,decimal_places=2)
    time_type = models.PositiveIntegerField(choices=service_time_type_choices)
    time = models.DecimalField(max_digits=12,decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
    def get_type(self):
        return_val = ''
        for service_type_choices_item in service_type_choices:
            if service_type_choices_item[0] == self.type:
                return_val = service_type_choices_item[1]
                break
        return return_val
    def get_time_type(self):
        return_val = ''
        for service_time_type_choices_item in service_time_type_choices:
            if service_time_type_choices_item[0] == self.time_type:
                return_val = service_time_type_choices_item[1]
                break
        return return_val


class Snippet(models.Model):
    snippet_user_type = models.IntegerField(choices=SNIPPET_CONRACT_USERTYPE_CHOICE)
    snippet = models.CharField(max_length=255,choices=SNIPPET_EMPLOYEE_CUSTOMER_CHOICE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = (("snippet_user_type", "snippet"),)

    def get_snippet_user_type(self):
        return_val = ''
        for SNIPPET_CONRACT_USERTYPE_CHOICE_item in SNIPPET_CONRACT_USERTYPE_CHOICE:
            if SNIPPET_CONRACT_USERTYPE_CHOICE_item[0] == self.snippet_user_type:
                return_val = SNIPPET_CONRACT_USERTYPE_CHOICE_item[1]
                break
        return return_val
    def get_snippet(self):
        return_val = ''
        for snippet_item in SNIPPET_EMPLOYEE_CUSTOMER_CHOICE:
            if snippet_item[0] == self.snippet:
                return_val = snippet_item[1]
                break
        return return_val

    def __str__(self):
        return self.snippet



class Note(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,editable=False,on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    content = HTMLField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_owner(self):
        return self.user
    def get_api_crud_url(self, request=None):
        return [
            api_reverse("customer-api:note-edit", kwargs={'pk': self.pk}, request=request),
            api_reverse("customer-api:note-view", kwargs={'pk': self.pk}, request=request),
            api_reverse("customer-api:note-remove", kwargs={'pk': self.pk}, request=request),
            ]

class ContractTemplate(models.Model):
    user_type = models.IntegerField(choices=SNIPPET_CONRACT_USERTYPE_CHOICE,blank=True,null=True)
    title = models.CharField(max_length=255)
    content = HTMLField()
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title








#***********************************************************************************************************************
def deserialize_team_employees(team):
    """Deserialize user instance to JSON."""
    list = []
    for team_employee_item in team.employees.all():
        list.append(EmployeeSerializer(team_employee_item))
    return list

class Team(models.Model):
    name = models.CharField(max_length=255)
    employees = models.ManyToManyField('Employee')
    date = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
    def get_team_work_days(self):
        tm_wds = TeamWorkDay.objects.filter(team=self)
        return tm_wds
    def get_team_work_day(self,day):
        tm_wd = TeamWorkDay.objects.filter(team=self,day=day).first()
        return tm_wd
    def to_json(self):
        # """deserialize message to JSON."""
        return {'name': self.name}
        # return {'name': self.name,'employees':deserialize_team_employees(self)}

class TeamWorkDay(models.Model):
    team = models.ForeignKey("Team",on_delete=models.CASCADE)
    day = models.ForeignKey("WorkDay",on_delete=models.CASCADE)
    minute = gen_field.IntegerRangeField(min_value=0, max_value=1440, default=0, choices=Hours_CHOICES)

    class Meta:
        unique_together = (("team", "day"),)

    def get_plans_w_d(self, w, d):
        plan_team_work = PlanTeamWork.objects.filter(team=self.team, week=w, day__id=d).first()
        return plan_team_work




class WorkDay(models.Model):
    day = gen_field.IntegerRangeField(min_value=1, max_value=7, choices=DayCHOICES, unique=True)

    def __str__(self):
        return_val = ''
        for dc in DayCHOICES:
            if dc[0] == self.day:
                return_val = dc[1]
        return str(return_val)

    def get_day_name(self):
        return_val = ''
        for dc in DayCHOICES:
            if dc[0] == self.day:
                return_val = dc[1]
        return str(return_val)


class PlanTeamWork(models.Model):
    team = models.ForeignKey("Team",on_delete=models.CASCADE)
    week = gen_field.IntegerRangeField(min_value=1, max_value=4)
    day = models.ForeignKey("WorkDay",on_delete=models.CASCADE)
    minute = gen_field.IntegerRangeField(min_value=0, max_value=1440, default=0)
    difference_minute = gen_field.IntegerRangeField(min_value=0, max_value=1440, default=0)
    is_empty = models.BooleanField(default=True)
    is_full = models.BooleanField(default=False)
    def __str__(self):
        return "{} - {} - {}".format(self.team.name,self.week,self.day)
    class Meta:
        unique_together = (("team", "week", "day"),)
    def get_customers(self):
        return CustomerOrder.objects.filter(plan_team_work=self).order_by('order_index')


    def get_last_customer(self):
        return CustomerOrder.objects.filter(plan_team_work=self).order_by('order_index').last()


    def get_customer(self, l_id):
        return CustomerOrder.objects.filter(plan_team_work=self, customer_id=l_id).first()





class CustomerDistance(models.Model):
    customer1 = models.ForeignKey('Customer',related_name='+',on_delete=models.CASCADE)
    customer2 = models.ForeignKey('Customer',related_name='+',on_delete=models.CASCADE)
    main_company = models.BooleanField(default=False)
    minute = models.IntegerField()
    distance = models.DecimalField(max_digits=19,decimal_places=2)
    def __str__(self):
        return "{} - {} - {} - {}".format(self.customer1.get_customer_name,self.customer2.get_customer_name,self.distance,self.minute)





class CustomerOrder(models.Model):
    plan_team_work = models.ForeignKey('PlanTeamWork',on_delete=models.CASCADE)
    order_index = gen_field.IntegerRangeField(min_value=1)
    customer = models.ForeignKey('Customer',on_delete=models.CASCADE)
    main_process = models.BooleanField(default=False)
    def __str__(self):
        return "{} ----- {}. {}".format(self.plan_team_work,self.order_index,self.customer.full_name)




class DistanceErrorLog(models.Model):
    customer1 = models.ForeignKey('Customer',related_name='+',on_delete=models.CASCADE)
    customer2 = models.ForeignKey('Customer',related_name='+',on_delete=models.CASCADE)
    # update_date = models.DateTimeField(auto_now=True,blank=True,null=True)
    date_time = models.DateTimeField(auto_now_add=True)
    date = models.DateField(auto_now_add=True)
    def __str__(self):
        return "{} - {} {} ".format(self.customer1.get_customer_name(),self.customer2.get_customer_name(),self.date.strftime("%d %b %Y %H:%M:%S"))



class PlanTeamWorkDate(models.Model):
    plan_team_work = models.ForeignKey('PlanTeamWork',on_delete=models.CASCADE)
    date = models.DateField()


class PlanTeamWorkDateEmployee(models.Model):
    plan_team_work_date = models.ForeignKey('PlanTeamWorkDate',on_delete=models.CASCADE)
    employee = models.ForeignKey('Employee',on_delete=models.CASCADE)

    class Meta:
        unique_together = ("plan_team_work_date", "employee")


class TaskDate(models.Model):
    task = models.ForeignKey('Task',on_delete=models.CASCADE)
    task_custom_day = models.ForeignKey('TaskCustomDay',on_delete=models.CASCADE,blank=True,null=True)
    plan_teamWork_date = models.ForeignKey('PlanTeamWorkDate',on_delete=models.CASCADE,blank=True,null=True)
    date = models.DateField()

    class Meta:
        unique_together = ("plan_teamWork_date", "date")

    # if task.date_type_fixed != 4:













