from rest_framework import serializers

# from work.models import Complaint


class UserSerializer(serializers.Serializer):
    email = serializers.EmailField()
    username = serializers.CharField(max_length=100)

class EmployeeSerializer(serializers.Serializer):
    employee_type = serializers.IntegerField()
    full_name = serializers.CharField(max_length=100)
    cpr_number = serializers.IntegerField()
    birth_date = serializers.DateField()
    phonenumber = serializers.IntegerField()
    auto_generated_password = serializers.CharField(max_length=255)
    street_name = serializers.CharField(max_length=255)
    zip_code = serializers.IntegerField()
    city = serializers.CharField(max_length=255)
    start_date = serializers.DateField()
    employment_type = serializers.IntegerField()
    amount_of_hours = serializers.DecimalField(max_digits=15,decimal_places=2)
    hourly_rate = serializers.DecimalField(max_digits=15,decimal_places=2)
    contract_file = serializers.FileField(allow_null=True)
    social_security_card = serializers.ImageField(allow_null=True)
    picture = serializers.ImageField(allow_null=True)
    date = serializers.DateTimeField(read_only=True)


class CustomerSerializer(serializers.Serializer):
    employee_type = serializers.IntegerField()
    full_name = serializers.CharField(max_length=100)
    cpr_number = serializers.IntegerField()
    birth_date = serializers.DateField()
    phonenumber = serializers.IntegerField()
    auto_generated_password = serializers.CharField(max_length=255)
    street_name = serializers.CharField(max_length=255)
    zip_code = serializers.IntegerField()
    city = serializers.CharField(max_length=255)
    start_date = serializers.DateField()
    employment_type = serializers.IntegerField()
    amount_of_hours = serializers.DecimalField(max_digits=15,decimal_places=2)
    hourly_rate = serializers.DecimalField(max_digits=15,decimal_places=2)
    contract_file = serializers.FileField(allow_null=True)
    social_security_card = serializers.ImageField(allow_null=True)
    picture = serializers.ImageField(allow_null=True)
    date = serializers.DateTimeField(read_only=True)


class SubcontractorSerializer(serializers.Serializer):
    edit_url         = serializers.SerializerMethodField(read_only=True)
    company_name = serializers.CharField(max_length=100)
    cvr_number = serializers.IntegerField()

    street_name = serializers.CharField(max_length=255)
    zip_code = serializers.IntegerField()
    city = serializers.CharField(max_length=255)


    full_name = serializers.CharField(max_length=100)
    phonenumber = serializers.IntegerField()
    email = serializers.EmailField()
    date = serializers.DateTimeField(read_only=True)
    def get_edit_url(self, obj):
        # request
        request = self.context.get("request")
        return obj.get_api_edit_url(request=request)


# class ComplaintSerializer(serializers.ModelSerializer):
#     url         = serializers.SerializerMethodField(read_only=True)
#     class Meta:
#         model = Complaint
#         fields = [
#             'url',
#             'id',
#             'name',
#             'is_read',
#             'content',
#             'date',
#         ]
#         read_only_fields = ['id',]
#     def get_url(self, obj):
#         # request
#         request = self.context.get("request")
#         return obj.get_api_url(request=request)


class ReminderSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    date_time = serializers.DateTimeField()
    is_read = serializers.BooleanField()
    date = serializers.DateTimeField(read_only=True)




class CleaningProgramSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    date = serializers.DateTimeField(read_only=True)
    # assign_service
    # date_time = serializers.DateTimeField()
    # is_read = serializers.BooleanField()




class TaskSerializer(serializers.Serializer):
    task_type = serializers.IntegerField()

    name = serializers.CharField(max_length=255)
    date_type_fixed = serializers.IntegerField()
    date_type_custom = serializers.IntegerField()
    # customer
    # cleaning_program\
    total_time = serializers.CharField(max_length=255)
    total_price = serializers.CharField(max_length=255)
    description = serializers.CharField(max_length=5000)
    draft = serializers.BooleanField(default=False)
    date = serializers.DateTimeField(read_only=True)






class ServiceSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)
    type = serializers.IntegerField()
    price = serializers.DecimalField(max_digits=19,decimal_places=2)
    time_type = serializers.IntegerField()
    time = serializers.DecimalField(max_digits=12,decimal_places=2)
    date = serializers.DateTimeField(read_only=True)


class SnippetSerializer(serializers.Serializer):
    snippet = serializers.CharField(max_length=255)
    content = serializers.CharField(max_length=5000)
    slug = serializers.CharField(max_length=255)
    date = serializers.DateTimeField(read_only=True)



class NoteSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255)
    content = serializers.CharField(max_length=5000)
    date = serializers.DateTimeField(read_only=True)



class ContractTemplateSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=255)
    content = serializers.CharField(max_length=5000)
    date = serializers.DateTimeField(read_only=True)
